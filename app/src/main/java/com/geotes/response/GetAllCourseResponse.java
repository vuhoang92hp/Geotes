package com.geotes.response;

import com.geotes.model.CourseModel;
import com.geotes.model.PlaylistModel;

import java.io.Serializable;
import java.util.ArrayList;

public class GetAllCourseResponse extends BaseResponse implements Serializable {
    public ArrayList<CourseModel> result;
}
