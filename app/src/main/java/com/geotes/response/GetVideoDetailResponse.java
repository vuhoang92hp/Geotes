package com.geotes.response;

import com.geotes.model.VideoDetailModel;
import com.geotes.model.VideoModel;

import java.io.Serializable;
import java.util.ArrayList;

public class GetVideoDetailResponse extends BaseResponse implements Serializable {
    public VideoDetailModel result;
}
