package com.geotes.response;

import com.geotes.model.NotificationModel;
import com.geotes.model.VersionCodeModel;

import java.io.Serializable;
import java.util.ArrayList;

public class GetVersionCodeResponse extends BaseResponse implements Serializable {
    public VersionCodeModel result;

}
