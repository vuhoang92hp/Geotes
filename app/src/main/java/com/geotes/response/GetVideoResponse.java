package com.geotes.response;

import com.geotes.model.PlaylistModel;
import com.geotes.model.VideoModel;

import java.io.Serializable;
import java.util.ArrayList;

public class GetVideoResponse extends BaseResponse implements Serializable {
    public ArrayList<VideoModel> result;
}
