package com.geotes.response;

import com.geotes.model.NewFeedModel;
import com.geotes.model.NotificationModel;

import java.io.Serializable;
import java.util.ArrayList;

public class GetNotificationResponse extends BaseResponse implements Serializable {
    public ArrayList<NotificationModel> result;
}
