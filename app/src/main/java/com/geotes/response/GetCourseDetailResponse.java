package com.geotes.response;

import com.geotes.model.CourseDetailModel;
import com.geotes.model.VideoDetailModel;

import java.io.Serializable;

public class GetCourseDetailResponse extends BaseResponse implements Serializable {
    public CourseDetailModel result;
}
