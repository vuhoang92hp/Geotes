package com.geotes.response;

import com.geotes.model.FollowModel;
import com.geotes.model.NewFeedModel;

import java.io.Serializable;
import java.util.ArrayList;

public class GetFollowResponse extends BaseResponse implements Serializable {
    public ArrayList<FollowModel> result;
}
