package com.geotes.response;

import com.geotes.model.BannerModel;
import com.geotes.model.NewFeedModel;

import java.io.Serializable;
import java.util.ArrayList;

public class GetNewFeedResponse extends BaseResponse implements Serializable {
    public ArrayList<NewFeedModel> result;
}
