package com.geotes.response;

import com.geotes.model.CourseDetailModel;
import com.geotes.model.FriendProfileModel;

import java.io.Serializable;

public class GetFriendProfileResponse extends BaseResponse implements Serializable {
    public FriendProfileModel result;
}
