package com.geotes.response;

import com.geotes.model.PlaylistModel;
import java.io.Serializable;
import java.util.ArrayList;

public class GetPlaylistResponse extends BaseResponse implements Serializable {
    public ArrayList<PlaylistModel> result;
}
