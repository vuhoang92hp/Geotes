package com.geotes.response;

import com.geotes.model.BannerModel;
import com.geotes.model.CourseModel;

import java.io.Serializable;
import java.util.ArrayList;

public class GetBannerResponse extends BaseResponse implements Serializable {
    public ArrayList<BannerModel> result;
}
