package com.geotes.common;

import android.app.Activity;
import android.app.ProgressDialog;
import android.view.KeyEvent;

import com.geotes.R;


public class DialogLoading {
	
	private static ProgressDialog dialog;
	private static Activity activity;
	private static String title = "";

    public static void showLoading(final Activity activity){
        //showLoading(activity, activity.getString(R.string.loading));
		showLoading(activity, "Loading...");
    }
	
	public static void showLoading(final Activity activity, String str){
		DialogLoading.activity = activity;
		if(str == null)
			//str = activity.getString(R.string.loading);
			str = "Loading...";
		DialogLoading.title = str;
		cancel();
		activity.runOnUiThread(new Runnable() {
			public void run() {
				dialog = new ProgressDialog(activity){
					@Override
			        public boolean onKeyDown(int keyCode, KeyEvent event) {
			            if(keyCode == KeyEvent.KEYCODE_BACK) {
			            	return true;
			            }
			            return super.onKeyDown(keyCode, event);
			        }
				};
				dialog.setMessage(DialogLoading.title);
				dialog.setCanceledOnTouchOutside(false);
				dialog.show();
			}
		});
	}
	
	public static boolean isShowing(){
		if(dialog == null){
			return false;
		}
		return dialog.isShowing();
	}
	
	public static void cancel(){
		try{
			if(activity != null)
				activity.runOnUiThread(new Runnable() {
					public void run() {
						if(dialog != null){
							dialog.dismiss();
						}
					}
				});
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static void setTitle(String title){
		DialogLoading.title = title;
		if(dialog != null)
			dialog.setMessage(DialogLoading.title);
	}
	
	public static String getTitle(){
		return DialogLoading.title;
	}
	
}
