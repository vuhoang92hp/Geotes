package com.geotes.common;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;

import com.geotes.R;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;


/**
 * Created by Thien Thien on 7/6/2017.
 */

public class PublicMethob {

    public static void animationShowUp(Context mContext, View view){
        view.setVisibility(View.VISIBLE);
        Animation animation = AnimationUtils.loadAnimation(mContext,
                R.anim.show_up);
        view.startAnimation(animation);
    }
    public static void animationShowDown(Context mContext, View view){
        view.setVisibility(View.VISIBLE);
        Animation animation = AnimationUtils.loadAnimation(mContext,
                R.anim.show_down);
        view.startAnimation(animation);
    }

    public static void animationHideDown(Context mContext, final View view){
        /*Animation slide_down = AnimationUtils.loadAnimation(mContext,
                R.anim.hide_down);
        view.startAnimation(slide_down);
        slide_down.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });*/
        view.setVisibility(View.GONE);
    }
    public static void animationHideUP(Context mContext, final View view){
        Animation slide_down = AnimationUtils.loadAnimation(mContext,
                R.anim.hide_up);
        view.startAnimation(slide_down);
        slide_down.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public static void animationZoomIn(Context mContext, final View view){
        Animation animation = AnimationUtils.loadAnimation(mContext,
                R.anim.zoom_in);
        view.startAnimation(animation);
    }

    public static void animationZoomOut(Context mContext, final View view){

        Animation animation = AnimationUtils.loadAnimation(mContext,
                R.anim.zoom_out);
        view.startAnimation(animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public static String formatPrice(int price){
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        DecimalFormat decimalFormat = new DecimalFormat("###,###,###,###,###", symbols);
        return decimalFormat.format( price);
    }

    public static void animationRotate(final Context mContext, final View view){
        Animation animation = AnimationUtils.loadAnimation(mContext,
                R.anim.animation_rotate);
        view.startAnimation(animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
    public static String converTime(int time_seconds)
    {
        String result = "";
        int day = (int) TimeUnit.SECONDS.toDays(time_seconds);
        long hours = TimeUnit.SECONDS.toHours(time_seconds) - (day *24);
        long minute = TimeUnit.SECONDS.toMinutes(time_seconds) - (TimeUnit.SECONDS.toHours(time_seconds)* 60);


        if (day != 0)
        {
            result = result + day + " NGÀY ";
        }
        if (hours !=0)
        {
            result = result + hours + " GIỜ ";
        }
        if (minute !=0)
        {
            result = result + minute + " PHÚT";
        }
        else if (result.equals(""))
        {
            result = 0 + " PHÚT";
        }

        return result;
    }
    public static String converTimeToMinute(int time_seconds)
    {
        String result = "";

        long minute = TimeUnit.SECONDS.toMinutes(time_seconds) - (TimeUnit.SECONDS.toHours(time_seconds)* 60);



        if (minute !=0)
        {
            result = result + minute + "\nPHÚT";
        }
        else if (result.equals(""))
        {
            result = 0 + "\nPHÚT";
        }

        return result;
    }

    public static String parseDateToddMMyyyy(String time) {
        String inputPattern = "yyyy-MM-dd hh:mm:ss";
        String outputPattern = "dd/MM/yyyy";

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = "Lúc "+time.substring(10, time.length()-3) + "  Ngày "+ outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static void  animationShowLeft(Context mContext, View view){
        view.setVisibility(View.VISIBLE);
        Animation animation = AnimationUtils.loadAnimation(mContext,
                R.anim.show_left);
        view.startAnimation(animation);
    }

    public static void animationHideLeft(Context mContext, final View view){
        Animation animation = AnimationUtils.loadAnimation(mContext,
                R.anim.hide_left);
        view.startAnimation(animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(View.INVISIBLE);
                return;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                return;
            }
        });
    }

    public static void animationHide(Context mContext, final View view){
        view.setVisibility(View.INVISIBLE);
    }

    public static void anamationAlphaDark(Context mContext, View view){
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.alpha_dark);
        view.startAnimation(animation);
    }

    public static void anamationAlphaDarkSlow(Context mContext, View view){
        Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.alpha_dark_slow);
        view.startAnimation(animation);
    }

    public static Typeface setTypeFace(Context mContext){
        Typeface typeface = Typeface.createFromAsset(mContext.getAssets(), "UTM_Helve.ttf");
        return typeface;
    }

    public static void showImageNomal(final Context mContext, final String urlPhoto, final View iv) {
        try {
            Picasso.with(mContext)
                    .load(urlPhoto)
                    .into((ImageView)iv, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            // Log something here eg "true"
                        }

                        @Override
                        public void onError() {
                            // Log something here eg "false"
                            ((ImageView) iv).setImageResource(R.mipmap.ic_no_image);
                        }
                    });
        } catch (Exception e) {
            ((ImageView) iv).setImageResource(R.mipmap.ic_no_image);

        }
        if (urlPhoto==null)
        {
            ((ImageView) iv).setImageResource(R.mipmap.ic_no_image);
        }
    }
    public static void showImageAvarta(final Context mContext, final String urlPhoto, final View iv) {
        try {
            Picasso.with(mContext)
                    .load(urlPhoto)
                    .into((ImageView)iv, new com.squareup.picasso.Callback() {
                        @Override
                        public void onSuccess() {
                            // Log something here eg "true"
                        }

                        @Override
                        public void onError() {
                            // Log something here eg "false"
                            ((ImageView) iv).setImageResource(R.mipmap.ic_no_avarta);
                        }
                    });
        } catch (Exception e) {
            ((ImageView) iv).setImageResource(R.mipmap.ic_no_avarta);

        }
        if (urlPhoto==null)
        {
            ((ImageView) iv).setImageResource(R.mipmap.ic_no_avarta);
        }
    }


    public static  int calCulate_Price(int typeTrip, int distance, int carType)
    {
        // typeTrip = 0:
        //typeTrip = 1: duong may bay
        //carType = 0: tat ca cac xe
        //carType = 1: xe 5 cho
        //carType = 2: xe 8 cho
        //distance unit: met


        int priceTrip = 0;

        if (typeTrip == 0)
        {
            if (distance < 3000 && carType == 1)
            {
                priceTrip = distance * 9; // 9k/1km
            }
            else if (distance >= 3000 && carType == 1)
            {
                priceTrip = 3000 * 9 + (distance - 3000) * 8; //9k voi 3km dau, 8k voi cac km tiep theo
            }
            else if (distance < 3000 && carType == 2)
            {
                priceTrip = distance * 12 ;
            }
            else if (distance >= 3000 && carType == 2)
            {
                priceTrip = 3000 * 12 + (distance - 3000) * 11; //12k voi 3km dau, 11k voi cac km tiep theo
            }
        }
        else // duong may bay
        {
            if (carType == 1 && distance < 16000)
            {
                priceTrip = distance * 9;
            }
            else if (carType == 1 && distance >= 26000)
            {
                priceTrip = distance * 8;
            }
            else if (carType == 1 && distance >= 16000 && distance < 26000 )
            {
                priceTrip = 145000;
            }
            else if (carType == 2 && distance < 16000)
            {
                priceTrip = distance * 12;
            }
            else  if (carType == 2 && distance >= 26000)
            {
                priceTrip = distance * 11;
            }
            else if (carType == 2 && distance >= 16000 && distance < 26000)
            {
                priceTrip = 190000;
            }
        }

        return priceTrip;
    }

    public static  String getCurrentDetailTime()
    {
        String time = "";

        Calendar cal = Calendar.getInstance();
        int minute = cal.get(Calendar.MINUTE);
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        int dayofmonth = cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);

        time = hour + ":"+ minute + " NGÀY " + dayofmonth + " THÁNG " + month + " NĂM " + year;


        return time;
    }
    public static String convertDecimalToDegree(double data)
    {
        int d,m;
        double s;
         d = (int) data;
         m =  (int)((data - d)*60);
         s  = (( data  - d  - (double)m/60) * 3600);

        return d + "\u00B0" + m+"'"+Math.round(s*100.0)/100.0+"''";

    }
    public static double convertDegreeToDecimal(int degree, int minutes, int seconds)
    {

        double decimal = (double)((minutes * 60)+seconds) / 3600;
        return degree + decimal;

    }

    public static double roundNumber(double number)
    {
        return  Math.round(number*1000.0)/1000.0;
    }
    public static double roundNumber2(double number)
    {
        return  Math.round(number*10.0)/10.0;
    }
    public static double convertDegreeToRadian(double number)
    {
        return  number*Math.PI/180;
    }

    public static double phuongvi(double xa, double ya, double xb, double yb)
    {
        //phuong vi canh AB
        double denta_Y = yb - ya;
        double denta_X = xb - xa;
        double phuong_vi_ab;
        double phuong_vi_degree;

        double R = Math.atan(Math.abs(denta_Y/denta_X));

        if (denta_X >= 0 && denta_Y >= 0)
        {
            phuong_vi_ab = R;
        }
        else if (denta_X <= 0 && denta_Y >=0)
        {
            phuong_vi_ab = Math.PI - R;
        }
        else if (denta_X <= 0 && denta_Y <=0)
        {
            phuong_vi_ab = Math.PI + R;
        }
        else if (denta_X >= 0 && denta_Y <=0)
        {
            phuong_vi_ab = 2 * Math.PI - R;
        }
        else
        {
            phuong_vi_ab = 0;
        }

         phuong_vi_degree = phuong_vi_ab * 180 / Math.PI;

        //txt_Sab.setText(Math.round(Sab*1000.0)/1000.0+"");
        //txt_phuong_vi.setText(PublicMethob.convertDecimalToDegree(phuong_vi_degree));
        return phuong_vi_degree;
    }

    public static  double khoangCachAB(double xa, double ya, double xb, double yb)
    {
        double khoang_cach;
        khoang_cach = Math.sqrt(Math.pow((xb-xa),2) +Math.pow((yb-ya),2));
        return khoang_cach;
    }
    public static void hideKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

}
