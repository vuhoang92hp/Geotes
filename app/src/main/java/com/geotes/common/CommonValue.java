package com.geotes.common;

/**
 * Created by Hoang on 1/1/2018.
 */

public class CommonValue {
    //Key activity Client include fragment
    public static  String INCLUDE_FRAGMENT = "";
    public static final String FRAGMENT_CALCULATE = "calculate";
    public static final String FRAGMENT_LIBRARY = "library";
    public static final String FRAGMENT_PLAYLIST = "playlist";
    public static final String FRAGMENT_EBOOK = "FRAGMENT_EBOOK";
    public static final String FRAGMENT_MENU = "menu";
    public static final String FRAGMENT_CTV = "ctv";
    public static final String FRAGMENT_CTV_REGISTER = "ctv_register";
    public static final String FRAGMENT_COURSE = "course";
    public static final String FRAGMENT_COURSE_DETAIL = "course_detail";
    public static final String FRAGMENT_DOCUMENT = "document";


    //Key activity Main include fragment
    public static  String TAB_SELECTED = "";
    public static final String TAB_HOME = "TAB_HOME";
    public static final String TAB_FAVOURITE = "TAB_FAVOURITE";
    public static final String TAB_CALCULATE = "TAB_CALCULATE";
    public static final String TAB_LIBRARY = "TAB_LIBRARY";

    //Key activity include fragment
    public static  String CURRENT_FRAGMENT = "";
    public static final String FRAGMENT_PROFILE_INFO = "FRAGMENT_PROFILE_INFO";
    public static final String FRAGMENT_CONTACT = "FRAGMENT_CONTACT";
    public static final String FRAGMENT_FEEDBACK = "FRAGMENT_FEEDBACK";
    public static final String FRAGMENT_EBOOK_DETAIL = "FRAGMENT_EBOOK_DETAIL";
    public static final String FRAGMENT_DOCUMENT_DETAIL = "FRAGMENT_DOCUMENT_DETAIL";
    public static final String FRAGMENT_PLAYLIST_DETAIL = "FRAGMENT_PLAYLIST_DETAIL";


    //Key activity Calculate activity
    public static  String CALCULATE_FRAGMENT = "";
    public static final String FRAGMENT_TRAC_DIA_THUAN = "FRAGMENT_TRAC_DIA_THUAN";
    public static final String FRAGMENT_TRAC_DIA_NGHICH = "FRAGMENT_TRAC_DIA_NGHICH";
    public static final String FRAGMENT_GIAO_HOI_THUAN = "FRAGMENT_GIAO_HOI_THUAN";
    public static final String FRAGMENT_GIAO_HOI_NGHICH_3_DIEM = "FRAGMENT_GIAO_HOI_NGHICH_3_DIEM";
    public static final String FRAGMENT_GIAO_HOI_GOC = "FRAGMENT_GIAO_HOI_GOC";
    public static final String FRAGMENT_DIEN_TICH_DA_GIAC = "FRAGMENT_DIEN_TICH_DA_GIAC";
    public static final String FRAGMENT_DIEN_TICH_TAM_GIAC_3_CANH = "FRAGMENT_DIEN_TICH_TAM_GIAC_3_CANH";
    public static final String FRAGMENT_CHON_KHOANG_CACH_DIEM = "FRAGMENT_CHON_KHOANG_CACH_DIEM";

    //Key document
    public static  String FRAGMENT_DOCUMENT_TYPE = "";
    public static final String  FDOCUMENT_TYPE_0= "FDOCUMENT_TYPE_0";
    public static final String  FDOCUMENT_TYPE_1= "FDOCUMENT_TYPE_1";
    public static final String  FDOCUMENT_TYPE_2= "FDOCUMENT_TYPE_2";
    public static final String  FDOCUMENT_TYPE_3= "FDOCUMENT_TYPE_3";
    public static final String  FDOCUMENT_TYPE_4= "FDOCUMENT_TYPE_4";
    public static final String  FDOCUMENT_TYPE_5= "FDOCUMENT_TYPE_5";
    public static final String  FDOCUMENT_TYPE_6= "FDOCUMENT_TYPE_6";
    public static final String  FDOCUMENT_TYPE_7= "FDOCUMENT_TYPE_7";
    public static final String  FDOCUMENT_TYPE_8= "FDOCUMENT_TYPE_8";
    public static final String  FDOCUMENT_TYPE_EBOOK_1= "FDOCUMENT_TYPE_EBOOK_1";
    public static final String  FDOCUMENT_TYPE_EBOOK_2= "FDOCUMENT_TYPE_EBOOK_2";
    public static final String  FDOCUMENT_TYPE_EBOOK_3= "FDOCUMENT_TYPE_EBOOK_3";
    //Youtube
    public static int PLAYLIST_ID = 0;
    public static int VIDEO_DETAIL_ID = 0;
    public static String VIDEO_LINK = "";
    public static int COURSE_ID = 0;

    //Course register
    public static int COURSE_REGISTER_ID = 0;

    //Key logout
    public static String KEY_LOGOUT = "";

    //camera garally

    public static String IMAGE = "";
    public static final String PICTURE = "picture";
    public static final String AVATAR = "avatar";
    public static final int REQUEST_CAMERA = 5001;
    public static final int SELECT_FILE = 6001;

    //New home
    public static final String KEY_NEWFEED_ID = "newfeedid";
    public static final int KEY_RELOAD = 1111;
    public static final int KEY_RELOAD_PROFILE = 1112;
    public static final int KEY_RELOAD_FOLLOW = 1113;
    //Friend Profile
    public static  int FRIEND_PROFILE_ID = 0;

    //Notification
    public static  String MAIN_SELECTED_TAB = "Home";

}
