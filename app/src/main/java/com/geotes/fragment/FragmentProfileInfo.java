package com.geotes.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.geotes.R;
import com.geotes.activity.IncludeActivity;
import com.geotes.api.APICommon;
import com.geotes.api.ServiceGenerator;
import com.geotes.common.CommonValue;
import com.geotes.common.Config;
import com.geotes.common.PublicMethob;
import com.geotes.controller.UserController;
import com.geotes.model.UserModel;
import com.geotes.request.CourseRegisterRequest;
import com.geotes.request.UpdateProfileRequest;
import com.geotes.response.CourseRegisterResponse;
import com.geotes.response.UpdateProfileResponse;
import com.geotes.response.UploadImageResponse;
import com.geotes.utils.ImageUtils;
import com.geotes.utils.Utility;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * Created by a Hoang on 6/28/2017.
 */

public class FragmentProfileInfo extends Fragment implements View.OnClickListener{
    private ImageView img_back,img_avarta;
    private TextView txt_title,txt_done,txt_user_name;
    private IncludeActivity main;
    private EditText edt_name, edt_phone_number, edt_email, edt_address;
    private UserModel userModel;
    private Bitmap bitmap;
    private String path_image="";
    public static final int REQUEST_CAMERA = 5001;
    public static final int SELECT_FILE = 6001;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private String photo = "";
    private boolean is_change_avarta = false;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_info, container, false);
        main = (IncludeActivity) getActivity();

        initUI(view);
        initEvent();
        bindData();
        return view;
    }

    private void initUI(View view) {

        img_back = (ImageView) view.findViewById(R.id.img_back);
        txt_title = (TextView) view.findViewById(R.id.txt_title);
        txt_done = (TextView) view.findViewById(R.id.txt_done);
        edt_name = (EditText) view.findViewById(R.id.edt_name);
        edt_phone_number = (EditText) view.findViewById(R.id.edt_phone_number);
        edt_email = (EditText) view.findViewById(R.id.edt_email);
        edt_address = (EditText) view.findViewById(R.id.edt_address);
        img_avarta = (ImageView) view.findViewById(R.id.img_avarta);
        txt_user_name = (TextView) view.findViewById(R.id.txt_user_name);
    }

    private void initEvent() {
        img_back.setOnClickListener(this);
        txt_done.setOnClickListener(this);
        img_avarta.setOnClickListener(this);
    }

    private void bindData() {
         sharedPreferences = getActivity().getSharedPreferences(Config.Pref, getActivity().MODE_PRIVATE);
        editor = sharedPreferences.edit();
        String profile = sharedPreferences.getString(Config.KEY_USER, "");
        Gson gson = new Gson();
         userModel = gson.fromJson(profile, UserModel.class);

        txt_title.setText(getResources().getString(R.string.profile_info));
        txt_done.setText(getResources().getString(R.string.done));

        if (userModel!=null)
        {
            PublicMethob.showImageAvarta(main,userModel.photo,img_avarta);
            txt_user_name.setText(userModel.name);
            edt_name.setText(userModel.name);
            edt_phone_number.setText(userModel.phone_number);
            edt_email.setText(userModel.email);
            edt_address.setText(userModel.address);
            if (userModel.photo!=null)
            {
                photo = userModel.photo.substring(userModel.photo.lastIndexOf("/")+1);
            }

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.img_back:
                main.finish();
                break;
            case R.id.txt_done:
                updateProfile();
                //main.showFrm(main.getFragmentTemp(), main.getFragmentMenu());
                break;
            case R.id.img_avarta:
                selectImage();
                break;

        }
    }
    private void selectImage() {
        final CharSequence[] items = {"Chụp ảnh", "Chọn từ thư viện",
                "Hủy bỏ"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Chọn ảnh!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(getActivity());

                if (items[item].equals("Chụp ảnh")) {
                    if (result)
                        is_change_avarta = true;
                        cameraIntent();

                } else if (items[item].equals("Chọn từ thư viện")) {
                    if (result)
                        is_change_avarta = true;
                        galleryIntent();

                } else if (items[item].equals("Hủy bỏ")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
    public File f;
    Uri imageToUploadUri;
    private void cameraIntent() {
        Intent chooserIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        f = new File(Environment.getExternalStorageDirectory(), "POST_IMAGE.jpg");
        String s = getActivity().getApplicationContext().getPackageName() + ".provider";
        imageToUploadUri = FileProvider.getUriForFile(main, getActivity().getApplicationContext().getPackageName() + ".provider", f);;
        chooserIntent.putExtra(MediaStore.EXTRA_OUTPUT,imageToUploadUri);
        startActivityForResult(chooserIntent, REQUEST_CAMERA);


    }

    private void galleryIntent() {

        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, SELECT_FILE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            /*If result for REQUEST_CAMERA*/
            if (requestCode == CommonValue.REQUEST_CAMERA) {
                String pathPhotoCapture = f.getPath();
                ImageCompressionAsyncTask imageCompressionAsyncTask = new ImageCompressionAsyncTask(pathPhotoCapture) {
                    @Override
                    protected void onPostExecute(byte[] imageBytes) {
                        File destination = new File(Environment.getExternalStorageDirectory(),
                                System.currentTimeMillis() + ".png");
                        FileOutputStream fo;
                        try {
                            destination.createNewFile();
                            fo = new FileOutputStream(destination);
                            fo.write(imageBytes);
                            fo.close();

                            path_image = destination.getPath();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                };
                imageCompressionAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);;
                bitmap = BitmapFactory.decodeFile(pathPhotoCapture);
                img_avarta.setImageBitmap(bitmap);

            } else if (requestCode == CommonValue.SELECT_FILE) { /*If result for Gallery*/

                Uri uri = data.getData();
                String[] projection = {MediaStore.Images.Media.DATA};
                Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(projection[0]);
                String selectedImagePath = cursor.getString(columnIndex);
                cursor.close();
                bitmap = BitmapFactory.decodeFile(selectedImagePath);

                ImageCompressionAsyncTask imageCompressionAsyncTask = new ImageCompressionAsyncTask(selectedImagePath) {
                    @Override
                    protected void onPostExecute(byte[] imageBytes) {
                        File destination = new File(Environment.getExternalStorageDirectory(),
                                System.currentTimeMillis() + ".png");
                        FileOutputStream fo;
                        try {
                            destination.createNewFile();
                            fo = new FileOutputStream(destination);
                            fo.write(imageBytes);
                            fo.close();

                            path_image = destination.getPath();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                };
                imageCompressionAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                Drawable drawable = new BitmapDrawable(bitmap);
                img_avarta.setImageDrawable(drawable);

            }

        }


    }
    public abstract class ImageCompressionAsyncTask extends AsyncTask<String, Void, byte[]> {
        String path = "";
        Bitmap bmp;

        public ImageCompressionAsyncTask(String path) {
            this.path = path;
        }

        @Override
        protected byte[] doInBackground(String... strings) {
            if (path == null || path.equals("")) {
                return null;
            } else {
                return ImageUtils.compressImage(path);
            }

        }

        protected abstract void onPostExecute(byte[] imageBytes);
    }


    public class UpdateProfileAsyncTask extends AsyncTask<UpdateProfileRequest, Void, UpdateProfileResponse> {
        ProgressDialog dialog;
        UpdateProfileRequest updateProfileRequest;
        Context ct;


        public UpdateProfileAsyncTask(Context ct, UpdateProfileRequest updateProfileRequest) {
            this.ct = ct;
            this.updateProfileRequest = updateProfileRequest;
        }

        @Override
        protected UpdateProfileResponse doInBackground(UpdateProfileRequest... params) {
            try {
                Thread.sleep(1000);
                UserController controller = new UserController();
                return controller.updateProfile(updateProfileRequest);
            } catch (Exception ex) {
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(ct);
            dialog.setMessage("Đang xử lý ...");
            dialog.show();
            dialog.setCancelable(false);
        }

        @Override
        protected void onPostExecute(UpdateProfileResponse code) {
            try {

                if (code != null) {
                    if (code.code == 200) {
                        userModel = code.result;
                        Gson gson = new Gson();
                        String profile = gson.toJson(userModel);
                        editor.putString(Config.KEY_USER, profile).commit();
                        Intent returnIntent = new Intent();

                        //reload profile
                        main.setResult(main.RESULT_OK, returnIntent);
                        main.finish();


                    }
                }
            } catch (Exception e) {
            }
            dialog.dismiss();
        }
    }

    private void uploadImage(final String path1) {
        try {

            final ProgressDialog dialog;
            dialog = new ProgressDialog(getContext());
            dialog.setMessage("Upload hình ảnh...");
            dialog.show();
            dialog.setCancelable(false);

            final File file1 = new File(path1);
            MultipartBody.Part body1 = null;
            MultipartBody.Part body2 = null;
            MultipartBody.Part body3 = null;
            MultipartBody.Part body4 = null;
            MultipartBody.Part body5 = null;
            if (file1.exists()) {
                body1 = prepareFilePart("FileNo1", path1);
            }


            final okhttp3.RequestBody userId = createPartFromString(userModel.id + "");
            okhttp3.RequestBody token = createPartFromString(userModel.ApiToken);

            APICommon.Geotes service = ServiceGenerator.GetInstance();
            Call<UploadImageResponse> call = service.uploadImage(body1, body2,body3,body4,body5, userId, token);
            call.enqueue(new Callback<UploadImageResponse>() {

                @Override
                public void onResponse(Call<UploadImageResponse> call, Response<UploadImageResponse> response) {

                    if (response.body().code == 200) {

                        dialog.dismiss();
                        Toast.makeText(getActivity(), "Upload ảnh thành công !", Toast.LENGTH_LONG).show();
                        photo = response.body().result;
                        UpdateProfileAsyncTask profileAsyncTask = new UpdateProfileAsyncTask(main,
                                new UpdateProfileRequest(userModel.id, edt_name.getText().toString().trim(),photo, edt_email.getText().toString().trim(), edt_address.getText().toString().trim()));

                        profileAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


                    } else {
                        dialog.dismiss();
                        Toast.makeText(getActivity(), "Upload ảnh thất bại !", Toast.LENGTH_LONG).show();
                    }


                }

                @Override
                public void onFailure(Call<UploadImageResponse> call, Throwable t) {
                    dialog.dismiss();
                    Toast.makeText(getActivity(), "Upload ảnh thất bại !", Toast.LENGTH_LONG).show();
                }
            });

        } catch (Exception err) {
            err.printStackTrace();
        }
    }
    @NonNull
    private MultipartBody.Part prepareFilePart(String partName, String path) {
        try {
            File file = new File(path);
            okhttp3.RequestBody requestFile =
                    okhttp3.RequestBody.create(okhttp3.MediaType.parse(path), file);

            // MultipartBody.Part is used to send also the actual file name
            return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
        } catch (Exception err) {
            err.printStackTrace();
            return null;
        }
    }
    @NonNull
    private okhttp3.RequestBody createPartFromString(String descriptionString) {
        return okhttp3.RequestBody.create(
                MultipartBody.FORM, descriptionString);
    }

    private void updateProfile() {

            PublicMethob.hideKeyboard(main);
        if (checkOk()) {

            if (!is_change_avarta)
            {
                UpdateProfileAsyncTask profileAsyncTask = new UpdateProfileAsyncTask(main,
                        new UpdateProfileRequest(userModel.id, edt_name.getText().toString().trim(),photo, edt_email.getText().toString().trim(), edt_address.getText().toString().trim()));


                profileAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else
            {
                uploadImage(path_image);
            }


        } else
        {
            Toast.makeText(main, "Vui lòng nhập đầy đủ thông tin!", Toast.LENGTH_LONG).show();
        }
    }

    private boolean checkOk()
    {
        boolean result = true;

        if (edt_name.getText().toString().trim().equals(""))
        {
            result = false;
        }
        else if (!com.geotes.utils.Utils.isEmailValid(edt_email.getText().toString().trim())) {
            Toast.makeText(getActivity(), "Email không hợp lệ!", Toast.LENGTH_SHORT).show();
            result=false;
        }

        return result;
    }
}
