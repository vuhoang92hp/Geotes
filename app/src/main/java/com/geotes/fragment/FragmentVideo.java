package com.geotes.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.geotes.R;
import com.geotes.activity.IncludeActivity;
import com.geotes.adapter.ListViewPlaylistAdapter;
import com.geotes.adapter.ListViewVideoAdapter;
import com.geotes.api.APICommon;
import com.geotes.api.ServiceGenerator;
import com.geotes.common.CRecyclerView;
import com.geotes.model.PlaylistModel;
import com.geotes.model.VideoModel;
import com.geotes.response.GetPlaylistResponse;
import com.geotes.response.GetVideoResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.geotes.common.CommonValue.PLAYLIST_ID;

/**
 * Created by a Hoang on 6/28/2017.
 */

public class FragmentVideo extends Fragment implements View.OnClickListener{
    private ImageView img_back, img_menu;
    private TextView txt_title;
    private IncludeActivity main;
    private RelativeLayout lay_title;
    private CRecyclerView list_video;
    private ArrayList<VideoModel> arr_video;
    private ListViewVideoAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video, container, false);
        main = (IncludeActivity) getActivity();

        initUI(view);
        initEvent();
        bindData();
        return view;
    }

    private void initUI(View view) {
        list_video = (CRecyclerView) view.findViewById(R.id.list_video);
        img_back = (ImageView) view.findViewById(R.id.img_back);
        img_menu = (ImageView) view.findViewById(R.id.img_menu);
        txt_title = (TextView) view.findViewById(R.id.txt_title);
        lay_title = (RelativeLayout) view.findViewById(R.id.lay_title);
    }

    private void initEvent() {

        img_back.setOnClickListener(this);


    }

    private void bindData() {
        img_menu.setVisibility(View.INVISIBLE);
        txt_title.setText(getResources().getString(R.string.playlist));
        lay_title.setBackgroundResource(R.color.colorRed);
        arr_video = new ArrayList<>();
        adapter = new ListViewVideoAdapter(getActivity(), arr_video);
        list_video.setAdapter(adapter);
        getVideoList();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.img_back:
                //getFragmentManager().popBackStack();
                main.replaceFrm( main.getFragmentPlaylist());
                break;


        }
    }

    private void getVideoList() {
        APICommon.Geotes service = ServiceGenerator.GetInstance();
        Call<GetVideoResponse> getVideoList;
        getVideoList = service.getVideoList(PLAYLIST_ID);
        getVideoList.enqueue(new Callback<GetVideoResponse>() {
            @Override
            public void onResponse(Call<GetVideoResponse> call, Response<GetVideoResponse> response) {
                try {
                    if (response.body() != null && response.body().code == 200) {
                        if (response.body().result != null && response.body().result.size() > 0) {
                            arr_video.addAll(response.body().result);
                            adapter.notifyDataSetChanged();
                        } else {

                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<GetVideoResponse> call, Throwable t) {
                Toast.makeText(getActivity(), "No Results Found", Toast.LENGTH_SHORT).show();
            }


        });
    }
}
