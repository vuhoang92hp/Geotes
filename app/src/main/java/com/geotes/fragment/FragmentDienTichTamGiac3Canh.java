package com.geotes.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.geotes.R;
import com.geotes.activity.CalculateActivity;
import com.geotes.common.PublicMethob;

/**
 * Created by a Hoang on 6/28/2017.
 */

public class FragmentDienTichTamGiac3Canh extends Fragment implements View.OnClickListener{

    private ImageView img_back, img_reset;
    private TextView txt_title;
    private CalculateActivity main;
    private EditText edt_a, edt_b, edt_c;
    private TextView txt_chu_vi, txt_dien_tich,txt_calculate;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dien_tich_tam_giac_theo_3_canh, container, false);
        main = (CalculateActivity) getActivity();

        initUI(view);
        initEvent();
        bindData();
        return view;
    }

    private void initUI(View view) {
        img_back = (ImageView) view.findViewById(R.id.img_back);
        img_reset = (ImageView) view.findViewById(R.id.img_reset);
        txt_title = (TextView) view.findViewById(R.id.txt_title);
        edt_a = (EditText) view.findViewById(R.id.edt_a);
        edt_b = (EditText) view.findViewById(R.id.edt_b);
        edt_c = (EditText) view.findViewById(R.id.edt_c);
        txt_chu_vi = (TextView) view.findViewById(R.id.txt_chu_vi);
        txt_dien_tich = (TextView) view.findViewById(R.id.txt_dien_tich);
        txt_calculate = (TextView) view.findViewById(R.id.txt_calculate);
    }

    private void initEvent() {
        img_reset.setOnClickListener(this);
        img_back.setOnClickListener(this);
        txt_calculate.setOnClickListener(this);

    }

    private void bindData() {

        txt_title.setText(getResources().getString(R.string.dien_tich_tam_giac_3_canh));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.img_back:
                main.finish();
                break;

            case R.id.img_reset:
                edt_a.setText("");
                edt_b.setText("");
                edt_c.setText("");
                txt_chu_vi.setText("");
                txt_dien_tich.setText("");
                break;
            case R.id.txt_calculate:
                PublicMethob.hideKeyboard(main);
                if (checkOk())
                {
                    double a,b,c, chu_vi, dien_tich;
                    try {
                         a = Double.parseDouble(edt_a.getText().toString().trim());
                         b = Double.parseDouble(edt_b.getText().toString().trim());
                         c = Double.parseDouble(edt_c.getText().toString().trim());
                         if (checkTriangle(a,b,c))
                         {
                             chu_vi = (a+b+c)/2;
                             dien_tich = Math.sqrt(chu_vi * (chu_vi - a)* (chu_vi - b)* (chu_vi - c));
                             txt_chu_vi.setText(PublicMethob.roundNumber(chu_vi)+"");
                             txt_dien_tich.setText(PublicMethob.roundNumber(dien_tich)+"");
                         }

                    }
                    catch (Exception e)
                    {
                        Toast.makeText(main, "Dữ liệu nhập lỗi!", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }



                }
                else
                {
                    Toast.makeText(main, "Vui lòng nhập đầy đủ thông tin!", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }
    private boolean checkOk()
    {
        boolean result = true;

        if (edt_a.getText().toString().trim().equals(""))
        {
            result = false;
        }
        if (edt_b.getText().toString().trim().equals(""))
        {
            result = false;
        }
        if (edt_c.getText().toString().trim().equals(""))
        {
            result = false;
        }


        return result;
    }

    private boolean checkTriangle(double a, double b, double c)
    {
        boolean result;

        if ((a+b)>c && (a+c)>b && (b+c)>a && a>0 && b>0 && c>0)
        {
            result = true;
        }
        else
        {
            result = false;
            Toast.makeText(main, "Tam giác không hợp lệ!", Toast.LENGTH_LONG).show();
        }


        return result;
    }

}
