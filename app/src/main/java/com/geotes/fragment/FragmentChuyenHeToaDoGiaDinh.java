package com.geotes.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.geotes.R;
import com.geotes.activity.CalculateActivity;
import com.geotes.common.PublicMethob;

/**
 * Created by a Hoang on 6/28/2017.
 */

public class FragmentChuyenHeToaDoGiaDinh extends Fragment implements View.OnClickListener{
    private RelativeLayout lay_title;
    private ImageView img_back, img_reset;
    private TextView txt_title;
    private CalculateActivity main;

    private EditText edt_xa, edt_ya, edt_xb, edt_yb, edt_xd, edt_yd,edt_xc,edt_yc;

    private TextView txt_calculate, txt_toa_do_xc, txt_toa_do_yc;

    private double xa, ya, xb,yb,xd,yd, xc,yc, x_ket_qua, y_ket_qua;



    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chuyen_he_toa_do_gia_dinh, container, false);
        main = (CalculateActivity) getActivity();

        initUI(view);
        initEvent();
        bindData();
        return view;
    }

    private void initUI(View view) {

        lay_title = (RelativeLayout) view.findViewById(R.id.lay_title);
        img_back = (ImageView) view.findViewById(R.id.img_back);
        img_reset = (ImageView) view.findViewById(R.id.img_reset);
        txt_title = (TextView) view.findViewById(R.id.txt_title);

        txt_calculate = (TextView) view.findViewById(R.id.txt_calculate);
        txt_toa_do_xc = (TextView) view.findViewById(R.id.txt_toa_do_xc);
        txt_toa_do_yc = (TextView) view.findViewById(R.id.txt_toa_do_yc);

        edt_xa = (EditText) view.findViewById(R.id.edt_xa);
        edt_ya = (EditText) view.findViewById(R.id.edt_ya);
        edt_xb = (EditText) view.findViewById(R.id.edt_xb);
        edt_yb = (EditText) view.findViewById(R.id.edt_yb);
        edt_xd = (EditText) view.findViewById(R.id.edt_xd);
        edt_yd = (EditText) view.findViewById(R.id.edt_yd);
        edt_xc = (EditText) view.findViewById(R.id.edt_xc);
        edt_yc = (EditText) view.findViewById(R.id.edt_yc);

    }

    private void initEvent() {
        img_reset.setOnClickListener(this);
        img_back.setOnClickListener(this);
        txt_calculate.setOnClickListener(this);

    }

    private void bindData() {

        txt_title.setText(getResources().getString(R.string.calcu_chuyen_he_toa_do_gia_dinh));
        lay_title.setBackgroundResource(R.color.colorOrange);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.img_back:
                main.finish();
                break;

            case R.id.img_reset:
                    reset();
                break;
            case R.id.txt_calculate:
                PublicMethob.hideKeyboard(main);
                if (checkOk())
                {

                    try {
                        xa = Double.parseDouble(edt_xa.getText().toString().trim());
                        ya = Double.parseDouble(edt_ya.getText().toString().trim());
                        xb = Double.parseDouble(edt_xb.getText().toString().trim());
                        yb = Double.parseDouble(edt_yb.getText().toString().trim());
                        xd = Double.parseDouble(edt_xd.getText().toString().trim());
                        yd = Double.parseDouble(edt_yd.getText().toString().trim());
                        xc = Double.parseDouble(edt_xc.getText().toString().trim());
                        yc = Double.parseDouble(edt_yc.getText().toString().trim());

                        //khoang cach AC
                        double Sac = PublicMethob.khoangCachAB(xa,ya,xc,yc);

                        //phuong vi AB
                        double anpha_AB = PublicMethob.phuongvi(xa,ya,xb,yb);

                        //phuong vi AC
                        double anpha_AC = PublicMethob.phuongvi(xa,ya,xc,yc);

                        //phuong vi AD
                        double anpha_AD = PublicMethob.phuongvi(xa,ya,xd,yd);

                        x_ket_qua = xd + Sac * Math.sin(90 - anpha_AC + anpha_AB);
                        y_ket_qua = yd + Sac * Math.cos(90 - anpha_AC + anpha_AB);

                        txt_toa_do_xc.setText(PublicMethob.roundNumber(x_ket_qua)+"");
                        txt_toa_do_yc.setText(PublicMethob.roundNumber(y_ket_qua)+"");

                    }
                     catch (Exception e)
                     {
                         Toast.makeText(main, "Dữ liệu nhập lỗi!", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                     }

               // xc = xd +

                }
                else
                {
                    Toast.makeText(main, "Vui lòng nhập đầy đủ thông tin!", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private boolean checkOk()
    {
        boolean result = true;

        if (edt_xa.getText().toString().trim().equals(""))
        {
            result = false;
        }
        if (edt_ya.getText().toString().trim().equals(""))
        {
            result = false;
        }
        if (edt_xb.getText().toString().trim().equals(""))
        {
            result = false;
        }
        if (edt_yb.getText().toString().trim().equals(""))
        {
            result = false;
        }
        if (edt_xd.getText().toString().trim().equals(""))
        {
            result = false;
        }
        if (edt_yd.getText().toString().trim().equals(""))
        {
            result = false;
        }
        if (edt_xc.getText().toString().trim().equals(""))
        {
            result = false;
        }
        if (edt_yc.getText().toString().trim().equals(""))
        {
            result = false;
        }
        return result;
    }

    public void reset()
    {

        txt_toa_do_xc.setText("");
        txt_toa_do_yc.setText("");

        edt_xa.setText("");
        edt_ya.setText("");
        edt_xb.setText("");
        edt_yb.setText("");
        edt_xd.setText("");
        edt_yd.setText("");
        edt_xc.setText("");
        edt_yc.setText("");
    }
}
