package com.geotes.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.geotes.R;
import com.geotes.activity.IncludeActivity;

/**
 * Created by a Hoang on 6/28/2017.
 */

public class FragmentIntroduction extends Fragment implements View.OnClickListener{
    private ImageView img_back, img_menu;
    private TextView txt_title,txt_course_register;
    private IncludeActivity main;
    private TextView txt_content, txt_website, txt_facebook, txt_phone;



    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_introduction, container, false);
        main = (IncludeActivity) getActivity();

        initUI(view);
        initEvent();
        bindData();
        return view;
    }

    private void initUI(View view) {

        img_back = (ImageView) view.findViewById(R.id.img_back);
        img_menu = (ImageView) view.findViewById(R.id.img_menu);
        txt_title = (TextView) view.findViewById(R.id.txt_title);

        txt_content = (TextView) view.findViewById(R.id.txt_content);
        txt_website = (TextView) view.findViewById(R.id.txt_website);
        txt_facebook = (TextView) view.findViewById(R.id.txt_facebook);
        txt_phone = (TextView) view.findViewById(R.id.txt_phone);

    }

    private void initEvent() {

        img_back.setOnClickListener(this);
        txt_website.setOnClickListener(this);
        txt_facebook.setOnClickListener(this);
        txt_phone.setOnClickListener(this);

    }

    private void bindData() {

        txt_title.setText(getResources().getString(R.string.contact));
        img_menu.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.img_back:
                main.finish();
               /* getFragmentManager().popBackStack();
                main.showFrm(main.getFragmentTemp(), main.getFragmentMenu());*/
                break;

            case R.id.txt_website:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://tracdiapro.com/"));
                startActivity(browserIntent);
                break;
            case R.id.txt_facebook:
                Intent browserIntent1 = new Intent(Intent.ACTION_VIEW, Uri.parse(txt_facebook.getText().toString().trim()));
                startActivity(browserIntent1);
                break;
            case R.id.txt_phone:

                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", txt_phone.getText().toString(), null));
                startActivity(intent);
                break;

        }
    }

}
