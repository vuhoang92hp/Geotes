package com.geotes.fragment.fragment_login;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.geotes.BuildConfig;
import com.geotes.R;
import com.geotes.activity.FriendProfileActivity;
import com.geotes.activity.StatusDetailsActivity;
import com.geotes.api.APICommon;
import com.geotes.api.ServiceGenerator;
import com.geotes.common.Config;
import com.geotes.request.DeletePostRequest;
import com.geotes.response.GetFriendProfileResponse;
import com.geotes.response.GetVersionCodeResponse;
import com.google.gson.Gson;

import java.security.MessageDigest;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by a Hoang on 6/28/2017.
 */

public class SplashFragment extends Fragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_splash, container, false);

        initUI(view);
        initEvent();
        bindData();

        return view;
    }

    private void initUI(View view) {

    }

    private void initEvent() {

    }

    private void bindData() {

    }


}
