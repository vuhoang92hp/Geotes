package com.geotes.fragment.fragment_login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;

import com.geotes.activity.MainActivity;
import com.geotes.activity.StartActivity;
import com.geotes.common.Config;
import com.geotes.common.DialogLoading;
import com.geotes.controller.UserController;
import com.geotes.model.UserModel;
import com.geotes.request.ActiveAccountRequest;
import com.geotes.request.RegisterRequest;
import com.geotes.response.ActiveAccountResponse;
import com.geotes.response.RegisterResponse;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.geotes.R;
/**
 * Created by a Hoang on 6/28/2017.
 */

public class RegisterFragment extends Fragment implements View.OnClickListener{
    private StartActivity main;
    private EditText edt_phone_number, edt_password, edt_re_password, edt_name;
    private TextView txt_register, txt_login;
    private String phone, pass, re_pas, name;
    private final static int APP_REQUEST_CODE = 37985;
    private UserModel userModel;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        main = (StartActivity) getActivity();

        initUI(view);
        initEvent();
        bindData();
        return view;
    }

    private void initUI(View view) {
        edt_phone_number = (EditText) view.findViewById(R.id.edt_phone_number);
        edt_password = (EditText) view.findViewById(R.id.edt_password);
        edt_re_password = (EditText) view.findViewById(R.id.edt_re_password);
        edt_name = (EditText) view.findViewById(R.id.edt_name);
        txt_register = (TextView) view.findViewById(R.id.txt_register);
        txt_login = (TextView) view.findViewById(R.id.txt_login);
    }

    private void initEvent() {
        txt_register.setOnClickListener(this);
        txt_login.setOnClickListener(this);
    }

    private void bindData() {
        sharedPreferences = getActivity().getSharedPreferences(Config.Pref, getActivity().MODE_PRIVATE);
        editor = sharedPreferences.edit();


    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.txt_register:
                phone = edt_phone_number.getText().toString().trim();
                pass = edt_password.getText().toString().trim();
                re_pas = edt_re_password.getText().toString().trim();
                name = edt_name.getText().toString().trim();
                if (checkOk())
                {
                    RegisterAsynctask registerAsynctask = new RegisterAsynctask(main, new RegisterRequest(phone, name, "", pass));
                    registerAsynctask.execute();
                }
                else
                {

                }

                break;
            case R.id.txt_login:
                main.showFragment(main.getFragmentTemp(), main.getLoginFragment());
                break;
        }
    }

    private boolean checkOk()
    {
        boolean result = true;

        if (phone.equals(""))
        {
            result = false;
        }
        if (pass.length()<6)
        {
            Toast.makeText(main, "Mật khẩu ít nhất 6 kí tự!", Toast.LENGTH_LONG).show();
            result = false;

        }
        if (re_pas.equals(""))
        {
            result = false;
        }
        if (name.equals(""))
        {
            result = false;
        }
        if(result == false)
        {
            Toast.makeText(main, "Vui lòng nhập đầy đủ thông tin!", Toast.LENGTH_LONG).show();
        }

        if(!pass.equals(re_pas))
        {
            Toast.makeText(main, "Mật khẩu nhập lại không trùng khớp!", Toast.LENGTH_LONG).show();
            result = false;
        }



        return result;
    }

    /*register*/
    public class RegisterAsynctask extends AsyncTask<RegisterRequest, Void, RegisterResponse> {
        ProgressDialog dialog_facebook;
        RegisterRequest registerRequest;
        Context ct;

        public RegisterAsynctask(Context ct, RegisterRequest registerRequest) {
            this.ct = ct;
            this.registerRequest = registerRequest;
        }

        @Override
        protected RegisterResponse doInBackground(RegisterRequest... params) {
            try {
                Thread.sleep(3000);
                String session_id = FirebaseInstanceId.getInstance().getToken().toString();
                registerRequest.token_firebase = session_id;
                UserController controller = new UserController();
                return controller.registerAccount(registerRequest);
            } catch (Exception ex) {
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
            dialog_facebook = new ProgressDialog(ct);
            dialog_facebook.setMessage("Đang khởi tạo...");
            dialog_facebook.show();
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(RegisterResponse code) {
            try {

                Toast.makeText(main, code.message, Toast.LENGTH_LONG).show();
                if (code != null) {
                    dialog_facebook.dismiss();
                    if (code.code == 200) {
                        loginByPhone();

                    } else if (code.code == 401)
                    {
                        loginByPhone();
                    }

                    else {
                        dialog_facebook.dismiss();
                        Toast.makeText(main, code.message, Toast.LENGTH_LONG).show();;
                    }
                } else {
                    Toast.makeText(main, code.message, Toast.LENGTH_LONG).show();
                    dialog_facebook.dismiss();
                }
            } catch (Exception e) {
                Toast.makeText(main, code.message, Toast.LENGTH_LONG).show();
                dialog_facebook.dismiss();
            }

        }
    }

    public void loginByPhone() {

        com.facebook.accountkit.AccessToken accessToken = AccountKit.getCurrentAccessToken();

        final Intent intent = new Intent(getActivity(), AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(
                        LoginType.PHONE,
                        AccountKitActivity.ResponseType.TOKEN);
        PhoneNumber phoneNum = new PhoneNumber("", phone);
        configurationBuilder.setInitialPhoneNumber(phoneNum);
        configurationBuilder.setSMSWhitelist(new String[]{"VN"});
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        startActivityForResult(intent, APP_REQUEST_CODE);

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode ==APP_REQUEST_CODE && resultCode == -1)
        {
            loginAccountKitPhone();
        }
        else
        {
            Toast.makeText(main, "Xác minh số điện thoại thất bại!", Toast.LENGTH_LONG).show();
        }
    }

    private void loginAccountKitPhone() {
        DialogLoading.showLoading(getActivity());
        AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
            @Override
            public void onSuccess(final Account account) {
                DialogLoading.cancel();
                phone = account.getPhoneNumber() + "";
                phone = phone.replace("+840", "0");
                phone = phone.replace("+84", "0");

                //call active account
                ActiveAccountAsystask activeAccountAsystask = new ActiveAccountAsystask(getActivity(), new ActiveAccountRequest(phone, pass, ""));
                activeAccountAsystask.execute();
            }

            @Override
            public void onError(final AccountKitError error) {
                DialogLoading.cancel();
            }
        });

    }


    public class ActiveAccountAsystask extends AsyncTask<ActiveAccountRequest, Void, ActiveAccountResponse> {
        ProgressDialog dialog;
        ActiveAccountRequest activeAccountRequest;
        Context ct;

        public ActiveAccountAsystask(Context ct, ActiveAccountRequest activeAccountRequest) {
            this.ct = ct;
            this.activeAccountRequest = activeAccountRequest;
        }

        @Override
        protected ActiveAccountResponse doInBackground(ActiveAccountRequest... params) {
            try {
                Thread.sleep(3000);
                //String session_id = FirebaseInstanceId.getInstance().getToken().toString();
                //activeAccountRequest.token_firebase = session_id;
                UserController controller = new UserController();
                return controller.activeAccount(activeAccountRequest);
            } catch (Exception ex) {
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(ct);
            dialog.setMessage("");
            dialog.show();
        }

        @Override
        protected void onPostExecute(ActiveAccountResponse code) {
            try {

                if (code != null) {
                    if (code.code == 200) {
                        Toast.makeText(getActivity(), code.message, Toast.LENGTH_LONG).show();
                        userModel = code.result;
                        Gson gson = new Gson();
                        String profile = gson.toJson(userModel);
                        editor.putString(Config.KEY_USER, profile).commit();
                        //============
                        Intent i = new Intent(main, MainActivity.class);
                        startActivity(i);
                        main.finish();

                    }
                    else
                    {
                        Toast.makeText(getActivity(), code.message, Toast.LENGTH_LONG).show();
                    }
                }
            } catch (Exception e) {
                dialog.dismiss();
            }
            dialog.dismiss();
        }
    }
}
