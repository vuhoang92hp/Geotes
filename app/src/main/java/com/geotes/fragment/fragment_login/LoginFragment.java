package com.geotes.fragment.fragment_login;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookAuthorizationException;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.geotes.activity.MainActivity;
import com.geotes.activity.StartActivity;
import com.geotes.common.Config;
import com.geotes.common.DialogLoading;
import com.geotes.controller.UserController;
import com.geotes.model.UserModel;
import com.geotes.request.ActiveAccountRequest;
import com.geotes.request.LoginFaceRequest;
import com.geotes.request.LoginRequest;
import com.geotes.response.ActiveAccountResponse;
import com.geotes.response.LoginResponse;
import com.geotes.utils.Utils;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.geotes.R;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.google.android.gms.internal.zzagz.runOnUiThread;


public class
LoginFragment extends Fragment implements View.OnClickListener {
    private EditText edt_phone_number, edt_password;
    private TextView txt_login, txt_foget_pass, txt_skip, txt_register;
    private ImageView img_login_facebook;
    private StartActivity main;
    private String phone, pass;
    private UserModel userModel;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private final static int APP_REQUEST_CODE = 37985;
    //ProgressDialog dialog_facebook;
    private CallbackManager callbackManager;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_log_in, container, false);
        main = (StartActivity) getActivity();
        initUI(view);
        initEvent();
        bindData();
        return view;
    }


    private void initUI(View view) {
        edt_phone_number = (EditText) view.findViewById(R.id.edt_phone_number);
        edt_password = (EditText) view.findViewById(R.id.edt_password);
        txt_login = (TextView) view.findViewById(R.id.txt_login);
        txt_foget_pass = (TextView) view.findViewById(R.id.txt_foget_pass);
        txt_skip = (TextView) view.findViewById(R.id.txt_skip);
        txt_register = (TextView) view.findViewById(R.id.txt_register);
        img_login_facebook = (ImageView) view.findViewById(R.id.img_login_facebook);
    }

    private void initEvent() {
        txt_login.setOnClickListener(this);
        txt_foget_pass.setOnClickListener(this);
        txt_skip.setOnClickListener(this);
        txt_register.setOnClickListener(this);
        img_login_facebook.setOnClickListener(this);

    }
    private void bindData() {


        sharedPreferences = getActivity().getSharedPreferences(Config.Pref, getActivity().MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.txt_login:
               /* Intent i = new Intent(main, MainActivity.class);
                startActivity(i);
                main.finish();*/

               if (checkOk())
               {
                   phone = edt_phone_number.getText().toString().trim();
                   pass = edt_password.getText().toString().trim();

                   LoginAsyntask loginAsyntask = new LoginAsyntask(main, new LoginRequest("","",""));
                   loginAsyntask.execute();

               }
               else
               {
                   Toast.makeText(main, "Vui lòng nhập đầy đủ thông tin!", Toast.LENGTH_LONG).show();
               }

                break;
            case R.id.txt_foget_pass:
                main.showFragment(main.getFragmentTemp(), main.getForgetPassFragment());
                break;
            case R.id.txt_skip:
                Intent i1 = new Intent(main, MainActivity.class);
                startActivity(i1);
                main.finish();
                break;
            case R.id.txt_register:
                main.showFragment(main.getFragmentTemp(), main.getRegisterFragment());
                break;
            case R.id.img_login_facebook:
                //login facebook
                //dialog_facebook = new ProgressDialog(getActivity());
                //dialog_facebook.setMessage("Loading...");
                //dialog_facebook.show();
                initData();
                loginFacebook();
                break;
        }
    }
    private boolean checkOk()
    {
        boolean result = true;

        if (edt_phone_number.getText().toString().trim().equals(""))
        {
            result = false;
        }
        if (edt_password.getText().toString().trim().equals(""))
        {
            result = false;
        }


        return result;
    }

    /* Login Asyntask App*/
    class LoginAsyntask extends AsyncTask<Void, Void, LoginResponse> {
        ProgressDialog dialog;
        String sessionId = "";
        Context ct;
        LoginRequest request;
        public LoginAsyntask(Context ct, LoginRequest request) {
            this.ct = ct;
            this.request=request;
        }

        @Override
        protected LoginResponse doInBackground(Void... params) {
            try {
                Thread.sleep(3000);
                String session_id = FirebaseInstanceId.getInstance().getToken().toString();
                request.token_firebase= session_id;
                request.phone_number = phone;
                request.password = pass;
                request.device_type = 1;
                UserController controller = new UserController();
                return controller.login(request);
            } catch (Exception ex) {
                ex.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(ct);
            dialog.setMessage("Đang tải thông tin đăng nhập...");
            //dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected void onPostExecute(LoginResponse response) {
            try {
                if(response!=null) {
//
                    int code = response.code;
                    if(code==200) {
                        userModel = response.result;
                        Gson gson = new Gson();
                        String profile = gson.toJson(userModel);
                        editor.putString(Config.KEY_USER, profile).commit();
                        //============
                        Intent i = new Intent(main, MainActivity.class);
                        startActivity(i);
                        main.finish();


                    } else if (code==401)
                    {

                        Toast.makeText(getActivity(), "Tài khoản chưa được kích hoạt, vui lòng kích hoạt!", Toast.LENGTH_LONG).show();
                        loginByPhone();

                    } else {
                        Toast.makeText(getActivity(), "Số điện thoại hoặc mật khẩu không đúng!", Toast.LENGTH_LONG).show();
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            dialog.dismiss();
        }
    }
    public void loginByPhone() {

        com.facebook.accountkit.AccessToken accessToken = AccountKit.getCurrentAccessToken();

        final Intent intent = new Intent(getActivity(), AccountKitActivity.class);
        AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder =
                new AccountKitConfiguration.AccountKitConfigurationBuilder(
                        LoginType.PHONE,
                        AccountKitActivity.ResponseType.TOKEN);
        PhoneNumber phoneNum = new PhoneNumber("", phone);
        configurationBuilder.setInitialPhoneNumber(phoneNum);
        configurationBuilder.setSMSWhitelist(new String[]{"VN"});
        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configurationBuilder.build());
        startActivityForResult(intent, APP_REQUEST_CODE);

    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (callbackManager != null) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }

        if (requestCode ==APP_REQUEST_CODE && resultCode == -1)
        {
            loginAccountKitPhone();
        }
        else
        {
           // Toast.makeText(main, "Xác minh số điện thoại thất bại!", Toast.LENGTH_LONG).show();
        }
    }

    private void loginAccountKitPhone() {
        DialogLoading.showLoading(getActivity());
        AccountKit.getCurrentAccount(new AccountKitCallback<Account>() {
            @Override
            public void onSuccess(final Account account) {
                DialogLoading.cancel();
                phone = account.getPhoneNumber() + "";
                phone = phone.replace("+840", "0");
                phone = phone.replace("+84", "0");

                //call active account
                ActiveAccountAsystask activeAccountAsystask = new ActiveAccountAsystask(getActivity(), new ActiveAccountRequest(phone, pass, ""));
                activeAccountAsystask.execute();
            }

            @Override
            public void onError(final AccountKitError error) {
                DialogLoading.cancel();
            }
        });

    }


    public class ActiveAccountAsystask extends AsyncTask<ActiveAccountRequest, Void, ActiveAccountResponse> {
        ProgressDialog dialog;
        ActiveAccountRequest activeAccountRequest;
        Context ct;

        public ActiveAccountAsystask(Context ct, ActiveAccountRequest activeAccountRequest) {
            this.ct = ct;
            this.activeAccountRequest = activeAccountRequest;
        }

        @Override
        protected ActiveAccountResponse doInBackground(ActiveAccountRequest... params) {
            try {
                Thread.sleep(3000);
                //String session_id = FirebaseInstanceId.getInstance().getToken().toString();
                //activeAccountRequest.token_firebase = session_id;
                UserController controller = new UserController();
                return controller.activeAccount(activeAccountRequest);
            } catch (Exception ex) {
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(ct);
            dialog.setMessage("");
            dialog.show();
        }

        @Override
        protected void onPostExecute(ActiveAccountResponse code) {
            try {

                if (code != null) {
                    if (code.code == 200) {
                        Toast.makeText(getActivity(), code.message, Toast.LENGTH_LONG).show();
                        userModel = code.result;
                        Gson gson = new Gson();
                        String profile = gson.toJson(userModel);
                        editor.putString(Config.KEY_USER, profile).commit();
                        //============
                        Intent i = new Intent(main, MainActivity.class);
                        startActivity(i);
                        main.finish();

                    }
                    else
                    {
                        Toast.makeText(getActivity(), code.message, Toast.LENGTH_LONG).show();
                    }
                }
            } catch (Exception e) {
                dialog.dismiss();
            }
            dialog.dismiss();
        }
    }

    private void initData() {

        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(final LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(final JSONObject object,
                                                    GraphResponse response) {
                                if (object != null) {
                                    //String email = object.optString("email") == null || object.optString("email").equals("") ? object.optString("id") : object.optString("email");
                                    String email = (object.optString("email") == null || object.optString("email").equals("")) ? object.optString("id") : object.optString("email");
                                    String phone_number = "";
                                    String name = object.optString("name");
                                    String fbid = object.optString("id");
                                    String avatar = Utils.addressAvatarFB(fbid);

                                    LoginFaceAsystask loginFaceAsystask = new LoginFaceAsystask(main, new LoginFaceRequest(fbid,name, email, avatar,"",phone_number));
                                    loginFaceAsystask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                                } else {

                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,link,birthday,picture,email,gender");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(main, "Login Cancel", Toast.LENGTH_LONG).show();
                       // dialog_facebook.dismiss();
                    }
                });

            }

            @Override
            public void onError(final FacebookException error) {
                if (error instanceof FacebookAuthorizationException) {
                    if (AccessToken.getCurrentAccessToken() != null) {
                        LoginManager.getInstance().logOut();
                        loginFacebook();
                    }
                }

            }
        });
    }

    /*Login with facebook*/
    public class LoginFaceAsystask extends AsyncTask<LoginFaceRequest, Void, LoginResponse> {

        LoginFaceRequest loginRequest;
        Context ct;

        public LoginFaceAsystask(Context ct, LoginFaceRequest loginRequest) {
            this.ct = ct;
            this.loginRequest = loginRequest;
        }

        @Override
        protected LoginResponse doInBackground(LoginFaceRequest... params) {
            try {
                Thread.sleep(3000);
                String session_id = FirebaseInstanceId.getInstance().getToken().toString();
                loginRequest.token_firebase = session_id;
                UserController controller = new UserController();
                return controller.loginFacebook(loginRequest);
            } catch (Exception ex) {
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(LoginResponse code) {
            try {

                Toast.makeText(main, code.message, Toast.LENGTH_LONG).show();
                if (code != null) {
                    if (code.code == 200) {
                        userModel = code.result;
                        Gson gson = new Gson();
                        String profile = gson.toJson(userModel);
                        editor.putString(Config.KEY_USER, profile).commit();
                        //============


                            Intent i = new Intent(main, MainActivity.class);
                            startActivity(i);
                            main.finish();


                    } else {

                        Toast.makeText(main, getResources().getString(R.string.login_facebook_failed), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(main, getResources().getString(R.string.login_facebook_failed), Toast.LENGTH_LONG).show();

                }
            } catch (Exception e) {
                Toast.makeText(main, getResources().getString(R.string.login_facebook_failed), Toast.LENGTH_LONG).show();

            }
           // dialog_facebook.dismiss();
        }
    }
    private void loginFacebook() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends", "email"));

    }
}
