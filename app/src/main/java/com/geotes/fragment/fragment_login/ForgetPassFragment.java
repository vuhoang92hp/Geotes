package com.geotes.fragment.fragment_login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.geotes.R;
import com.geotes.activity.StartActivity;


public class ForgetPassFragment extends Fragment implements View.OnClickListener{
    private StartActivity main;
    private EditText edt_phone, edt_new_password, edt_re_new_password;
    private TextView txt_update, txt_login, txt_register;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_forget_pass, container, false);
        main = (StartActivity) getActivity();

        initUI(view);
        initEvent();
        bindData();
        return view;

    }

    private void initUI(View view) {
        edt_phone = (EditText) view.findViewById(R.id.edt_phone);
        edt_new_password = (EditText) view.findViewById(R.id.edt_new_password);
        edt_re_new_password = (EditText) view.findViewById(R.id.edt_re_new_password);
        txt_update = (TextView) view.findViewById(R.id.txt_update);
        txt_login = (TextView) view.findViewById(R.id.txt_login);
        txt_register = (TextView) view.findViewById(R.id.txt_register);
    }

    private void initEvent() {
        txt_update.setOnClickListener(this);
        txt_login.setOnClickListener(this);
        txt_register.setOnClickListener(this);
    }
    private void bindData() {


    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.txt_update:

                break;
            case R.id.txt_login:
                main.showFragment(main.getFragmentTemp(), main.getLoginFragment());
                break;
            case R.id.txt_register:
                main.showFragment(main.getFragmentTemp(), main.getRegisterFragment());
                break;
        }
    }
}
