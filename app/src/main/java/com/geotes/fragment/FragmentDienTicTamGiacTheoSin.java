package com.geotes.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.geotes.R;
import com.geotes.activity.CalculateActivity;

/**
 * Created by a Hoang on 6/28/2017.
 */

public class FragmentDienTicTamGiacTheoSin extends Fragment implements View.OnClickListener{
    private RelativeLayout lay_title;
    private ImageView img_back, img_reset;
    private TextView txt_title;
    private CalculateActivity main;




    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_trac_dia_thuan, container, false);
        main = (CalculateActivity) getActivity();

        initUI(view);
        initEvent();
        bindData();
        return view;
    }

    private void initUI(View view) {

        lay_title = (RelativeLayout) view.findViewById(R.id.lay_title);
        img_back = (ImageView) view.findViewById(R.id.img_back);
        img_reset = (ImageView) view.findViewById(R.id.img_reset);
        txt_title = (TextView) view.findViewById(R.id.txt_title);


    }

    private void initEvent() {
        img_reset.setOnClickListener(this);
        img_back.setOnClickListener(this);

    }

    private void bindData() {

        txt_title.setText(getResources().getString(R.string.dien_tich_tam_giac_sin));
        lay_title.setBackgroundResource(R.color.colorBlueDark);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.img_back:
                main.finish();
                break;

            case R.id.img_reset:

                break;

        }
    }
}
