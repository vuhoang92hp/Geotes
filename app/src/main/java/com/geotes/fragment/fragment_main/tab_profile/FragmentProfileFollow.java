package com.geotes.fragment.fragment_main.tab_profile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.geotes.R;
import com.geotes.activity.IncludeActivity;
import com.geotes.activity.MainActivity;
import com.geotes.adapter.ListViewFollowAdapter;
import com.geotes.adapter.ListViewNewFeedAdapter;
import com.geotes.api.APICommon;
import com.geotes.api.ServiceGenerator;
import com.geotes.common.CRecyclerView;
import com.geotes.common.Config;
import com.geotes.model.FollowModel;
import com.geotes.model.NewFeedModel;
import com.geotes.model.UserModel;
import com.geotes.response.GetFollowResponse;
import com.geotes.response.GetNewFeedResponse;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.geotes.common.CommonValue.CURRENT_FRAGMENT;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_EBOOK_1;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_EBOOK_2;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_EBOOK_3;
import static com.geotes.common.CommonValue.FRAGMENT_DOCUMENT_TYPE;
import static com.geotes.common.CommonValue.FRAGMENT_EBOOK_DETAIL;
import static com.geotes.common.CommonValue.FRAGMENT_MENU;
import static com.geotes.common.CommonValue.KEY_RELOAD;
import static com.geotes.common.CommonValue.KEY_RELOAD_FOLLOW;


/**
 * Created by a Hoang on 6/28/2017.
 */

public class FragmentProfileFollow extends Fragment implements View.OnClickListener{

    private MainActivity main;
    private CRecyclerView list_profile_follow;
    private UserModel userModel;
    public static ListViewFollowAdapter adapter_list_follow;
    private boolean isCanNext = true;
    private boolean isProgessingLoadMore = true;
    public int page_number = 1;
    public int page_size = 10;
    private SwipeRefreshLayout swipeRefresh;
    public static ArrayList<FollowModel> arr_follow;



    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_follow, container, false);
        main = (MainActivity) getActivity();

        initUI(view);
        initEvent();
        bindData();
        return view;
    }

    private void initUI(View view) {

        list_profile_follow = (CRecyclerView) view.findViewById(R.id.list_profile_follow);
        swipeRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefresh);


    }

    private void initEvent() {

        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefresh.setRefreshing(false);
                page_number = 1;
                arr_follow.clear();
                adapter_list_follow = new ListViewFollowAdapter(main, arr_follow);
                list_profile_follow.setAdapter(adapter_list_follow);
                loadData();
            }
        });

    }

    public void bindData() {


        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Config.Pref, getActivity().MODE_PRIVATE);
        String profile = sharedPreferences.getString(Config.KEY_USER, "");
        Gson gson = new Gson();
        userModel = gson.fromJson(profile, UserModel.class);

        if (userModel!=null)
        {
            arr_follow = new ArrayList<>();
            swipeRefresh.setColorSchemeResources(R.color.colorGreen);
            adapter_list_follow = new ListViewFollowAdapter(main, arr_follow);
            loadData();

            list_profile_follow.setAdapter(adapter_list_follow);
            //list_favourite.setNestedScrollingEnabled(false);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {

        }
    }
    private void loadData(){

        getListFollow();

    }
    private void getListFollow() {
        APICommon.Geotes service = ServiceGenerator.GetInstance();
        Call<GetFollowResponse> getFollow;
        getFollow = service.getListFollow(userModel.id,userModel.id);
        getFollow.enqueue(new Callback<GetFollowResponse>() {
            @Override
            public void onResponse(Call<GetFollowResponse> call, Response<GetFollowResponse> response) {
                try {
                    isProgessingLoadMore = false;
                    if (response.body() != null && response.body().code == 200) {
                        arr_follow.addAll(response.body().result);

                        adapter_list_follow.notifyDataSetChanged();


                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<GetFollowResponse> call, Throwable t) {
                Toast.makeText(main, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
            }


        });
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == KEY_RELOAD_FOLLOW) {
            page_number = 1;
            arr_follow.clear();
            adapter_list_follow = new ListViewFollowAdapter(main, arr_follow);
            list_profile_follow.setAdapter(adapter_list_follow);
            loadData();
        }

    }
}
