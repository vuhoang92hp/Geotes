package com.geotes.fragment.fragment_main;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.Image;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.geotes.R;
import com.geotes.activity.IncludeActivity;
import com.geotes.activity.MainActivity;
import com.geotes.activity.MenuActivity;
import com.geotes.activity.StartActivity;
import com.geotes.adapter.CustomImageHomeViewPager;
import com.geotes.adapter.CustomImageViewPager;
import com.geotes.adapter.ListViewNewFeedAdapter;
import com.geotes.api.APICommon;
import com.geotes.api.ServiceGenerator;
import com.geotes.common.CRecyclerView;
import com.geotes.common.Config;
import com.geotes.common.PublicMethob;
import com.geotes.common.autoviewpager.AutoScrollViewPager;
import com.geotes.model.BannerModel;
import com.geotes.model.CommentModel;
import com.geotes.model.CourseModel;
import com.geotes.model.NewFeedModel;
import com.geotes.model.UserModel;
import com.geotes.response.GetAllCourseResponse;
import com.geotes.response.GetBannerResponse;
import com.geotes.response.GetCourseDetailResponse;
import com.geotes.response.GetNewFeedResponse;
import com.geotes.utils.Utils;
import com.google.gson.Gson;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.geotes.common.CommonValue.COURSE_ID;
import static com.geotes.common.CommonValue.COURSE_REGISTER_ID;
import static com.geotes.common.CommonValue.CURRENT_FRAGMENT;
import static com.geotes.common.CommonValue.FRAGMENT_MENU;
import static com.geotes.common.CommonValue.INCLUDE_FRAGMENT;
import static com.geotes.common.CommonValue.KEY_RELOAD;
import static com.geotes.common.CommonValue.KEY_RELOAD_PROFILE;

/**
 * Created by a Hoang on 6/28/2017.
 */

public class FragmentHome extends Fragment implements View.OnClickListener, ViewPager.OnPageChangeListener{
    private TextView txt_title;
    private ImageView img_menu,img_search;
    private AutoScrollViewPager viewpager_home;
    private CircleIndicator mIndicator;
    private CRecyclerView listview_new_feed;
    private MainActivity main;
    private RelativeLayout relativeLayout2;
    private ArrayList<BannerModel> arr_banner;
    private ArrayList<String> arrImage = new ArrayList<>();
    public static ListViewNewFeedAdapter adapter_list_new_feed;
    private boolean isCanNext = true;
    private boolean isProgessingLoadMore = true;
    public int page_number = 1;
    public int page_size =5;
    private SwipeRefreshLayout swipeRefresh;
    public static ArrayList<NewFeedModel> arr_new_feed;
    private UserModel userModel;
    private LinearLayout lay_title, lay_search;
    private EditText edt_search;
    private ImageView img_clear;
    private ScrollView scrollview;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        main = (MainActivity) getActivity();

        initUI(view);
        initEvent();
        bindData();
        return view;
    }

    private void initUI(View view) {
        txt_title = (TextView) view.findViewById(R.id.txt_title);
        img_search = (ImageView) view.findViewById(R.id.img_search);
        img_menu = (ImageView) view.findViewById(R.id.img_menu);
        viewpager_home = (AutoScrollViewPager) view.findViewById(R.id.viewpager_home);
        mIndicator = (CircleIndicator) view.findViewById(R.id.indicator);
        listview_new_feed = (CRecyclerView) view.findViewById(R.id.listview_new_feed);
        relativeLayout2 = (RelativeLayout) view.findViewById(R.id.relativeLayout2);
        swipeRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefresh);
        lay_title = (LinearLayout) view.findViewById(R.id.lay_title);
        lay_search = (LinearLayout) view.findViewById(R.id.lay_search);
        edt_search = (EditText) view.findViewById(R.id.edt_search);
        img_clear = (ImageView) view.findViewById(R.id.img_clear);
        scrollview = (ScrollView) view.findViewById(R.id.scrollview);
    }

    private void initEvent() {
        img_search.setOnClickListener(this);
        img_menu.setOnClickListener(this);
        img_clear.setOnClickListener(this);
        viewpager_home.setOnPageChangeListener(this);
        /*listview_new_feed.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (Utils.isReadyForPullEnd(recyclerView) && isCanNext && !isProgessingLoadMore) {
                    page_number++;
                    loadData();
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });*/

        scrollview.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                if (scrollview != null) {
                    if (scrollview.getChildAt(0).getBottom() <= (scrollview.getHeight() + scrollview.getScrollY())) {
                        //scroll view is at bottom
                        if (isCanNext && !isProgessingLoadMore) {
                            page_number++;
                            loadData();

                            isCanNext = false;

                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    // Do something after 3s = 3000ms

                                    isCanNext = true;



                                }
                            }, 3000);
                        }
                    } else {

                        //scroll view is not at bottom
                    }
                }
            }
        });
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isCanNext = true;
                swipeRefresh.setRefreshing(false);

                page_number = 1;
                adapter_list_new_feed = new ListViewNewFeedAdapter(main, arr_new_feed);
                arr_new_feed.clear();
                loadData();
                listview_new_feed.setAdapter(adapter_list_new_feed);
                //listview_new_feed.setNestedScrollingEnabled(false);
            }
        });
        edt_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    //perform search
                    lay_search.setVisibility(View.GONE);
                    lay_title.setVisibility(View.VISIBLE);
                    isCanNext = false;

                    page_number = 1;
                    adapter_list_new_feed = new ListViewNewFeedAdapter(main, arr_new_feed);
                    arr_new_feed.clear();
                    PublicMethob.hideKeyboard(main);

                    String text_search = edt_search.getText().toString().trim();
                    getNewFeed(0,text_search);
                    edt_search.setText("");
                    listview_new_feed.setAdapter(adapter_list_new_feed);
                    listview_new_feed.setNestedScrollingEnabled(false);


                    return true;
                }
                return false;
            }
        });
    }

    private void bindData() {
        txt_title.setText(getResources().getString(R.string.app_name));


        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Config.Pref, getActivity().MODE_PRIVATE);
        String profile = sharedPreferences.getString(Config.KEY_USER, "");
        Gson gson = new Gson();
        userModel = gson.fromJson(profile, UserModel.class);
        arr_new_feed = new ArrayList<>();
        arr_banner = new ArrayList<>();

        swipeRefresh.setColorSchemeResources(R.color.colorGreen);
        Display display = main.getWindowManager().getDefaultDisplay();
        int width = display.getWidth();  // deprecated
        int height = display.getHeight();

        ViewGroup.LayoutParams params = relativeLayout2.getLayoutParams();
// Changes the height and width to the specified *pixels*
        params.height = height/3;
        relativeLayout2.setLayoutParams(params);
        //relativeLayout2.setMinimumHeight(height/3);

        adapter_list_new_feed = new ListViewNewFeedAdapter(main, arr_new_feed);
        loadData();
        getBanner();
        listview_new_feed.setAdapter(adapter_list_new_feed);
        listview_new_feed.setNestedScrollingEnabled(false);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.img_menu:

                Intent i = new Intent(main, MenuActivity.class);
                startActivity(i);
                break;

            case R.id.img_clear:
                edt_search.setText("");
                lay_search.setVisibility(View.GONE);
                lay_title.setVisibility(View.VISIBLE);
                PublicMethob.hideKeyboard(main);
                break;

            case R.id.img_search:
                lay_search.setVisibility(View.VISIBLE);
                lay_title.setVisibility(View.GONE);
                break;

        }
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private void getBanner() {
        APICommon.Geotes service = ServiceGenerator.GetInstance();
        Call<GetBannerResponse> getBanner;
        getBanner = service.getBanner();
        getBanner.enqueue(new Callback<GetBannerResponse>() {
            @Override
            public void onResponse(Call<GetBannerResponse> call, Response<GetBannerResponse> response) {
                try {
                    if (response.body() != null && response.body().code == 200) {
                        if (response.body().result != null ) {
                            arr_banner.addAll(response.body().result);

                            for (int i = 0;i<arr_banner.size();i++)
                            {
                                arrImage.add(i, arr_banner.get(i).photo);
                            }

                            viewpager_home.setAdapter(new CustomImageHomeViewPager(main, arrImage));
                            mIndicator.setViewPager(viewpager_home);
                            viewpager_home.startAutoScroll(5000);

                        } else {
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<GetBannerResponse> call, Throwable t) {
                Toast.makeText(main, "Không có kết nối!", Toast.LENGTH_SHORT).show();
                arrImage.add(0,"");
                arrImage.add(1,"");
                arrImage.add(2,"");
                arrImage.add(3,"");
                arrImage.add(4,"");

                viewpager_home.setAdapter(new CustomImageHomeViewPager(main, arrImage));
                mIndicator.setViewPager(viewpager_home);
                viewpager_home.startAutoScroll(5000);
            }


        });
    }

    private void loadData(){
       // adapter_list_new_feed.clear();
        getNewFeed(0,"");

    }

    private void getNewFeed(int is_like, String text_search) {
        APICommon.Geotes service = ServiceGenerator.GetInstance();
        Call<GetNewFeedResponse> getNewFeed;
        if (userModel!=null)
        {
            getNewFeed = service.getNewFeed(userModel.id,is_like,text_search, page_size, page_number, 1);
        }
        else
        {
            getNewFeed = service.getNewFeed(0,is_like,text_search, page_size, page_number,1);
        }

        getNewFeed.enqueue(new Callback<GetNewFeedResponse>() {
            @Override
            public void onResponse(Call<GetNewFeedResponse> call, Response<GetNewFeedResponse> response) {
                try {
                    isProgessingLoadMore = false;
                    if (response.body() != null && response.body().code == 200) {
                        arr_new_feed.addAll(response.body().result);

                        //adapter_list_new_feed.addAll(arr_new_feed);
                        adapter_list_new_feed.notifyDataSetChanged();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<GetNewFeedResponse> call, Throwable t) {
                Toast.makeText(main, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
            }


        });
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == KEY_RELOAD || resultCode  == RESULT_OK) {
                isCanNext = true;
                swipeRefresh.setRefreshing(false);

                page_number = 1;
                adapter_list_new_feed = new ListViewNewFeedAdapter(main, arr_new_feed);
                arr_new_feed.clear();
                loadData();
                listview_new_feed.setAdapter(adapter_list_new_feed);
            }



    }


}
