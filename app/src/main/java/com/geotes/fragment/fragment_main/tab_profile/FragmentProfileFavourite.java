package com.geotes.fragment.fragment_main.tab_profile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.geotes.R;
import com.geotes.activity.IncludeActivity;
import com.geotes.activity.MainActivity;
import com.geotes.adapter.ListViewNewFeedAdapter;
import com.geotes.api.APICommon;
import com.geotes.api.ServiceGenerator;
import com.geotes.common.CRecyclerView;
import com.geotes.common.Config;
import com.geotes.model.CourseModel;
import com.geotes.model.NewFeedModel;
import com.geotes.model.UserModel;
import com.geotes.response.GetAllCourseResponse;
import com.geotes.response.GetNewFeedResponse;
import com.geotes.utils.Utils;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.geotes.common.CommonValue.KEY_RELOAD;
import static com.geotes.common.CommonValue.KEY_RELOAD_PROFILE;

/**
 * Created by a Hoang on 6/28/2017.
 */

public class FragmentProfileFavourite extends Fragment implements View.OnClickListener{

    private TextView txt_non_favourite;
    private MainActivity main;
    private CRecyclerView list_favourite;
    private UserModel userModel;
    public static ListViewNewFeedAdapter adapter_list_new_feed;
    private boolean isCanNext = true;
    private boolean isProgessingLoadMore = true;
    public int page_number = 1;
    public int page_size = 5;
    private SwipeRefreshLayout swipeRefresh;
    public static ArrayList<NewFeedModel> arr_favourite;



    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favourite, container, false);
        main = (MainActivity) getActivity();

        initUI(view);
        initEvent();
        bindData();
        return view;
    }

    private void initUI(View view) {

        list_favourite = (CRecyclerView) view.findViewById(R.id.list_favourite);
        swipeRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefresh);
        txt_non_favourite = (TextView) view.findViewById(R.id.txt_non_favourite);

    }

    private void initEvent() {


        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isCanNext = true;
                swipeRefresh.setRefreshing(false);

                page_number = 1;
                adapter_list_new_feed = new ListViewNewFeedAdapter(main, arr_favourite);
                arr_favourite.clear();
                loadData();
                list_favourite.setAdapter(adapter_list_new_feed);
            }
        });

    }

    public void bindData() {


        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Config.Pref, getActivity().MODE_PRIVATE);
        String profile = sharedPreferences.getString(Config.KEY_USER, "");
        Gson gson = new Gson();
        userModel = gson.fromJson(profile, UserModel.class);

        if (userModel!=null)
        {
            arr_favourite = new ArrayList<>();
            swipeRefresh.setColorSchemeResources(R.color.colorGreen);
            adapter_list_new_feed = new ListViewNewFeedAdapter(main, arr_favourite);
            loadData();

            list_favourite.setAdapter(adapter_list_new_feed);
            //list_favourite.setNestedScrollingEnabled(false);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {



        }
    }
    private void loadData(){
        getFavourite(1,"");

    }
    private void getFavourite(int is_like, String text_search) {
        APICommon.Geotes service = ServiceGenerator.GetInstance();
        Call<GetNewFeedResponse> getNewFeed;
        getNewFeed = service.getNewFeed(userModel.id,is_like,text_search, page_size, page_number,1);
        getNewFeed.enqueue(new Callback<GetNewFeedResponse>() {
            @Override
            public void onResponse(Call<GetNewFeedResponse> call, Response<GetNewFeedResponse> response) {
                try {
                    isProgessingLoadMore = false;
                    if (response.body() != null && response.body().code == 200) {
                        arr_favourite.addAll(response.body().result);

                        adapter_list_new_feed.notifyDataSetChanged();

                        if (arr_favourite.size()==0)
                        {
                            txt_non_favourite.setVisibility(View.VISIBLE);
                            //list_favourite.setVisibility(View.GONE);
                        } else
                        {
                            txt_non_favourite.setVisibility(View.GONE);
                            //list_favourite.setVisibility(View.VISIBLE);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<GetNewFeedResponse> call, Throwable t) {
                Toast.makeText(main, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
            }


        });
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == KEY_RELOAD || resultCode  == RESULT_OK) {
                page_number = 1;
                arr_favourite.clear();
                adapter_list_new_feed = new ListViewNewFeedAdapter(main, arr_favourite);
                list_favourite.setAdapter(adapter_list_new_feed);
                loadData();
            }


    }


}
