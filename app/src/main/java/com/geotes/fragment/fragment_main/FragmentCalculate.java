package com.geotes.fragment.fragment_main;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.geotes.R;
import com.geotes.activity.CalculateActivity;
import com.geotes.activity.DistanceCalculateActivity;
import com.geotes.activity.IncludeActivity;
import com.geotes.activity.MainActivity;
import com.geotes.activity.PolygonActivity;
import com.geotes.activity.StartActivity;


import static com.geotes.common.CommonValue.CALCULATE_FRAGMENT;
import static com.geotes.common.CommonValue.FRAGMENT_CHON_KHOANG_CACH_DIEM;
import static com.geotes.common.CommonValue.FRAGMENT_DIEN_TICH_DA_GIAC;
import static com.geotes.common.CommonValue.FRAGMENT_DIEN_TICH_TAM_GIAC_3_CANH;
import static com.geotes.common.CommonValue.FRAGMENT_GIAO_HOI_GOC;
import static com.geotes.common.CommonValue.FRAGMENT_GIAO_HOI_NGHICH_3_DIEM;
import static com.geotes.common.CommonValue.FRAGMENT_GIAO_HOI_THUAN;
import static com.geotes.common.CommonValue.FRAGMENT_TRAC_DIA_NGHICH;
import static com.geotes.common.CommonValue.FRAGMENT_TRAC_DIA_THUAN;

/**
 * Created by a Hoang on 6/28/2017.
 */

public class FragmentCalculate extends Fragment implements View.OnClickListener{
    private ImageView img_back, img_menu;
    private TextView txt_title;
    private MainActivity main;
    private LinearLayout lay_trac_dia_thuan, lay_trac_dia_nghich, lay_giao_hoi_thuan, lay_giao_hoi_nghich_3_diem, lay_giao_hoi_goc, lay_dien_tich_da_giac, lay_dien_tich_tam_giac_3_canh, lay_chon_khoang_cach_diem,lay_dien_tich_tren_ban_do;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_calculator, container, false);
        main = (MainActivity) getActivity();

        initUI(view);
        initEvent();
        bindData();
        return view;
    }

    private void initUI(View view) {
        img_back = (ImageView) view.findViewById(R.id.img_back);
        img_menu = (ImageView) view.findViewById(R.id.img_menu);
        txt_title = (TextView) view.findViewById(R.id.txt_title);
        lay_trac_dia_thuan = (LinearLayout) view.findViewById(R.id.lay_trac_dia_thuan);
        lay_trac_dia_nghich = (LinearLayout) view.findViewById(R.id.lay_trac_dia_nghich);
        lay_giao_hoi_thuan = (LinearLayout) view.findViewById(R.id.lay_giao_hoi_thuan);
        lay_giao_hoi_nghich_3_diem = (LinearLayout) view.findViewById(R.id.lay_giao_hoi_nghich_3_diem);
        lay_giao_hoi_goc = (LinearLayout) view.findViewById(R.id.lay_giao_hoi_goc);
        lay_dien_tich_da_giac = (LinearLayout) view.findViewById(R.id.lay_dien_tich_da_giac);
        lay_dien_tich_tam_giac_3_canh = (LinearLayout) view.findViewById(R.id.lay_dien_tich_tam_giac_3_canh);
        lay_chon_khoang_cach_diem = (LinearLayout) view.findViewById(R.id.lay_chon_khoang_cach_diem);
        lay_dien_tich_tren_ban_do = (LinearLayout) view.findViewById(R.id.lay_dien_tich_tren_ban_do);

    }

    private void initEvent() {
        img_menu.setOnClickListener(this);
        img_back.setOnClickListener(this);
        lay_trac_dia_thuan.setOnClickListener(this);
        lay_trac_dia_nghich.setOnClickListener(this);
        lay_giao_hoi_thuan.setOnClickListener(this);
        lay_giao_hoi_nghich_3_diem.setOnClickListener(this);
        lay_giao_hoi_goc.setOnClickListener(this);
        lay_dien_tich_da_giac.setOnClickListener(this);
        lay_dien_tich_tam_giac_3_canh.setOnClickListener(this);
        lay_chon_khoang_cach_diem.setOnClickListener(this);
        lay_dien_tich_tren_ban_do.setOnClickListener(this);
    }

    private void bindData() {

        txt_title.setText(getResources().getString(R.string.calculate));
        img_menu.setVisibility(View.INVISIBLE);
        img_back.setVisibility(View.INVISIBLE);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.img_back:
                main.finish();
                break;

            case R.id.img_menu:

                break;

            case R.id.lay_trac_dia_thuan:
                CALCULATE_FRAGMENT = FRAGMENT_TRAC_DIA_THUAN;
                Intent i = new Intent(main, CalculateActivity.class);
                startActivity(i);
                break;
            case R.id.lay_trac_dia_nghich:
                CALCULATE_FRAGMENT = FRAGMENT_TRAC_DIA_NGHICH;
                Intent i1 = new Intent(main, CalculateActivity.class);
                startActivity(i1);
                break;
            case R.id.lay_giao_hoi_thuan:
                CALCULATE_FRAGMENT = FRAGMENT_GIAO_HOI_THUAN;
                Intent i2 = new Intent(main, CalculateActivity.class);
                startActivity(i2);
                break;
            case R.id.lay_giao_hoi_nghich_3_diem:
                CALCULATE_FRAGMENT = FRAGMENT_GIAO_HOI_NGHICH_3_DIEM;
                Intent i3 = new Intent(main, CalculateActivity.class);
                startActivity(i3);
                break;
            case R.id.lay_giao_hoi_goc:
                CALCULATE_FRAGMENT = FRAGMENT_GIAO_HOI_GOC;
                Intent i4 = new Intent(main, CalculateActivity.class);
                startActivity(i4);
                break;
            case R.id.lay_dien_tich_da_giac:
                CALCULATE_FRAGMENT = FRAGMENT_DIEN_TICH_DA_GIAC;
                Intent i5 = new Intent(main, CalculateActivity.class);
                startActivity(i5);
                break;
            case R.id.lay_dien_tich_tam_giac_3_canh:
                CALCULATE_FRAGMENT = FRAGMENT_DIEN_TICH_TAM_GIAC_3_CANH;
                Intent i6 = new Intent(main, CalculateActivity.class);
                startActivity(i6);
                break;
            case R.id.lay_chon_khoang_cach_diem:
                CALCULATE_FRAGMENT = FRAGMENT_CHON_KHOANG_CACH_DIEM;
                ActivityCompat.requestPermissions(main,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                if (ActivityCompat.checkSelfPermission(main, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(main, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {


                    return;

                }
                else
                {
                    Intent i7 = new Intent(main, DistanceCalculateActivity.class);
                    //Intent i7 = new Intent(main, TestMapActivity.class);
                    startActivity(i7);
                }

                break;

            case R.id.lay_dien_tich_tren_ban_do:
                CALCULATE_FRAGMENT = FRAGMENT_CHON_KHOANG_CACH_DIEM;
                ActivityCompat.requestPermissions(main,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                if (ActivityCompat.checkSelfPermission(main, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(main, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                    return;

                }
                else
                {
                    Intent i8 = new Intent(main, PolygonActivity.class);
                    startActivity(i8);
                }

                break;
        }
    }
}
