package com.geotes.fragment.fragment_main;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.geotes.R;
import com.geotes.activity.IncludeActivity;
import com.geotes.activity.MainActivity;
import com.geotes.activity.StatusDetailsActivity;
import com.geotes.adapter.ListViewNotificationAdapter;
import com.geotes.api.APICommon;
import com.geotes.api.ServiceGenerator;
import com.geotes.common.CRecyclerView;
import com.geotes.common.CommonValue;
import com.geotes.common.Config;
import com.geotes.common.PublicMethob;
import com.geotes.controller.UserController;
import com.geotes.fragment.FragmentProfileInfo;
import com.geotes.model.CommentModel;
import com.geotes.model.NotificationModel;
import com.geotes.model.UserModel;
import com.geotes.request.AddCommentRequest;
import com.geotes.request.AddStatusRequest;
import com.geotes.request.UpdateProfileRequest;
import com.geotes.response.AddCommentResponse;
import com.geotes.response.AddStatusResponse;
import com.geotes.response.UploadImageResponse;
import com.geotes.utils.ImageUtils;
import com.geotes.utils.Utility;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.geotes.common.CommonValue.CURRENT_FRAGMENT;
import static com.geotes.common.CommonValue.FRAGMENT_MENU;
import static com.geotes.common.CommonValue.INCLUDE_FRAGMENT;

/**
 * Created by a Hoang on 6/28/2017.
 */

public class FragmentPostStatus extends Fragment implements View.OnClickListener{
    private MainActivity main;
    private ImageView img_menu;
    private TextView txt_title;
    private LinearLayout lay_done;
    private Bitmap bitmap;
    private String path_image="";
    private EditText edt_content;
    private RelativeLayout lay_photo_1, lay_photo_2, lay_photo_3, lay_photo_4, lay_photo_5;
    private ImageView img_photo_1, img_photo_2, img_photo_3,img_photo_4, img_photo_5;
    private ImageView img_close_1, img_close_2, img_close_3, img_close_4, img_close_5;
    private static final int REQUEST_CAMERA = 10000, SELECT_FILE = 1;
    private boolean chooseImage1, chooseImage2, chooseImage3, chooseImage4, chooseImage5;
    private String path1 = "", path2 = "", path3 = "", path4 = "", path5 = "";
    private UserModel userModel;
    private String content, image_path = "";

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_post_status, container, false);
        main = (MainActivity) getActivity();

        initUI(view);
        initEvent();
        bindData();
        return view;
    }

    private boolean checkOk()
    {
        boolean result = true;
        content = edt_content.getText().toString().trim();
        content = content.replace("\n","<br>");
        if (content.equals(""))
        {
            result = false;
            Toast.makeText(main,"Vui lòng nhập nội dung!", Toast.LENGTH_LONG).show();
        }

        return result;

    }
    private void initUI(View view) {
        img_menu = (ImageView) view.findViewById(R.id.img_menu);
        txt_title = (TextView) view.findViewById(R.id.txt_title);
        lay_done = (LinearLayout) view.findViewById(R.id.lay_done);
        edt_content = (EditText) view.findViewById(R.id.edt_content);
        lay_photo_1 = (RelativeLayout) view.findViewById(R.id.lay_photo_1);
        lay_photo_2 = (RelativeLayout) view.findViewById(R.id.lay_photo_2);
        lay_photo_3 = (RelativeLayout) view.findViewById(R.id.lay_photo_3);
        lay_photo_4 = (RelativeLayout) view.findViewById(R.id.lay_photo_4);
        lay_photo_5 = (RelativeLayout) view.findViewById(R.id.lay_photo_5);
        img_photo_1 = (ImageView) view.findViewById(R.id.img_photo_1);
        img_photo_2 = (ImageView) view.findViewById(R.id.img_photo_2);
        img_photo_3 = (ImageView) view.findViewById(R.id.img_photo_3);
        img_photo_4 = (ImageView) view.findViewById(R.id.img_photo_4);
        img_photo_5 = (ImageView) view.findViewById(R.id.img_photo_5);
        img_close_1 = (ImageView) view.findViewById(R.id.img_close_1);
        img_close_2 = (ImageView) view.findViewById(R.id.img_close_2);
        img_close_3 = (ImageView) view.findViewById(R.id.img_close_3);
        img_close_4 = (ImageView) view.findViewById(R.id.img_close_4);
        img_close_5 = (ImageView) view.findViewById(R.id.img_close_5);
    }

    private void initEvent() {
        lay_done.setOnClickListener(this);
        lay_photo_1.setOnClickListener(this);
        lay_photo_2.setOnClickListener(this);
        lay_photo_3.setOnClickListener(this);
        lay_photo_4.setOnClickListener(this);
        lay_photo_5.setOnClickListener(this);
        img_close_1.setOnClickListener(this);
        img_close_2.setOnClickListener(this);
        img_close_3.setOnClickListener(this);
        img_close_4.setOnClickListener(this);
        img_close_5.setOnClickListener(this);
    }

    private void bindData() {
        txt_title.setText("Đăng bài");
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Config.Pref, getActivity().MODE_PRIVATE);
        String profile = sharedPreferences.getString(Config.KEY_USER, "");
        Gson gson = new Gson();
        userModel = gson.fromJson(profile, UserModel.class);
        img_menu.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.lay_done:
                if (checkOk())
                {

                    if (path1.equals("") && path2.equals("") && path3.equals("") && path4.equals("") && path5.equals(""))
                    {
                        AddStatusAsyncTask addStatusAsyncTask = new AddStatusAsyncTask(main, new AddStatusRequest(userModel.id, userModel.ApiToken, content, image_path));
                        addStatusAsyncTask.execute();
                    }
                    else
                    {
                        uploadImage(path1, path2, path3, path4, path5);
                    }
                }

                break;
            case R.id.lay_photo_1:
                selectImage();
                chooseImage1 = true;
                break;
            case R.id.lay_photo_2:
                selectImage();
                chooseImage2 = true;
                break;
            case R.id.lay_photo_3:
                selectImage();
                chooseImage3 = true;
                break;
            case R.id.lay_photo_4:
                selectImage();
                chooseImage4 = true;
                break;
            case R.id.lay_photo_5:
                selectImage();
                chooseImage5 = true;
                break;
            case R.id.img_close_1:
                lay_photo_1.setBackgroundResource(R.drawable.bg_stoke_green);
                img_close_1.setVisibility(View.INVISIBLE);
                img_photo_1.setVisibility(View.INVISIBLE);
                chooseImage1 = false;
                path1 = "";
                break;
            case R.id.img_close_2:
                lay_photo_2.setBackgroundResource(R.drawable.bg_stoke_green);
                img_close_2.setVisibility(View.INVISIBLE);
                img_photo_2.setVisibility(View.INVISIBLE);
                chooseImage2 = false;
                path2 = "";
                break;
            case R.id.img_close_3:
                lay_photo_3.setBackgroundResource(R.drawable.bg_stoke_green);
                img_close_3.setVisibility(View.INVISIBLE);
                img_photo_3.setVisibility(View.INVISIBLE);
                chooseImage3 = false;
                path3 = "";
                break;
            case R.id.img_close_4:
                lay_photo_4.setBackgroundResource(R.drawable.bg_stoke_green);
                img_close_4.setVisibility(View.INVISIBLE);
                img_photo_4.setVisibility(View.INVISIBLE);
                chooseImage4 = false;
                path4 = "";
                break;
            case R.id.img_close_5:
                lay_photo_5.setBackgroundResource(R.drawable.bg_stoke_green);
                img_close_5.setVisibility(View.INVISIBLE);
                img_photo_5.setVisibility(View.INVISIBLE);
                chooseImage5 = false;
                path5 = "";
                break;
        }
    }

    private void reset()
    {
        edt_content.setText("");

        // lay 1
        lay_photo_1.setBackgroundResource(R.drawable.bg_stoke_green);
        img_close_1.setVisibility(View.INVISIBLE);
        img_photo_1.setVisibility(View.INVISIBLE);
        chooseImage1 = false;
        path1 = "";
        //lay 2
        lay_photo_2.setBackgroundResource(R.drawable.bg_stoke_green);
        img_close_2.setVisibility(View.INVISIBLE);
        img_photo_2.setVisibility(View.INVISIBLE);
        chooseImage2 = false;
        path2 = "";

        //lay 3

        lay_photo_3.setBackgroundResource(R.drawable.bg_stoke_green);
        img_close_3.setVisibility(View.INVISIBLE);
        img_photo_3.setVisibility(View.INVISIBLE);
        chooseImage3 = false;
        path3 = "";
        //lay 4
        lay_photo_4.setBackgroundResource(R.drawable.bg_stoke_green);
        img_close_4.setVisibility(View.INVISIBLE);
        img_photo_4.setVisibility(View.INVISIBLE);
        chooseImage4 = false;
        path4 = "";

        //lay 5
        lay_photo_5.setBackgroundResource(R.drawable.bg_stoke_green);
        img_close_5.setVisibility(View.INVISIBLE);
        img_photo_5.setVisibility(View.INVISIBLE);
        chooseImage5 = false;
        path5 = "";
    }

    private void selectImage() {
        final CharSequence[] items = {"Chụp ảnh", "Chọn từ thư viện",
                "Hủy bỏ"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Thêm ảnh!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = Utility.checkPermission(getActivity());

                if (items[item].equals("Chụp ảnh")) {
                    if (result)
                        cameraIntent();

                } else if (items[item].equals("Chọn từ thư viện")) {
                    if (result)
                        galleryIntent();

                } else if (items[item].equals("Hủy bỏ")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void galleryIntent() {

        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, SELECT_FILE);
    }
    public File f;
    Uri imageToUploadUri;
    private void cameraIntent() {
        Intent chooserIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        f = new File(Environment.getExternalStorageDirectory(), "POST_IMAGE.jpg");
        String s = getActivity().getApplicationContext().getPackageName() + ".provider";
        imageToUploadUri = FileProvider.getUriForFile(main, getActivity().getApplicationContext().getPackageName() + ".provider", f);;
        chooserIntent.putExtra(MediaStore.EXTRA_OUTPUT,imageToUploadUri);
        startActivityForResult(chooserIntent, REQUEST_CAMERA);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {

            /*If result for REQUEST_CAMERA*/
            if (requestCode == REQUEST_CAMERA) {
                String pathPhotoCapture = f.getPath();
                ImageCompressionAsyncTask imageCompressionAsyncTask = new ImageCompressionAsyncTask(pathPhotoCapture) {
                    @Override
                    protected void onPostExecute(byte[] imageBytes) {
                        File destination = new File(Environment.getExternalStorageDirectory(),
                                System.currentTimeMillis() + ".png");
                        FileOutputStream fo;
                        try {
                            destination.createNewFile();
                            fo = new FileOutputStream(destination);
                            fo.write(imageBytes);
                            fo.close();

                            path_image = destination.getPath();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                };
                imageCompressionAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);;
                bitmap = BitmapFactory.decodeFile(pathPhotoCapture);

                if (chooseImage1) {
                    lay_photo_1.setBackgroundResource(R.drawable.border_edittext_transparent);
                    img_photo_1.setImageBitmap(bitmap);
                    img_close_1.setVisibility(View.VISIBLE);
                    img_photo_1.setVisibility(View.VISIBLE);
                    chooseImage1 = false;
                    path1 = path_image;




                }
                else if (chooseImage2) {
                    lay_photo_2.setBackgroundResource(R.drawable.border_edittext_transparent);
                    img_photo_2.setImageBitmap(bitmap);
                    img_close_2.setVisibility(View.VISIBLE);
                    img_photo_2.setVisibility(View.VISIBLE);
                    chooseImage2 = false;
                    path2 = path_image;




                }
                else if (chooseImage3) {
                    lay_photo_3.setBackgroundResource(R.drawable.border_edittext_transparent);
                    img_photo_3.setImageBitmap(bitmap);
                    img_close_3.setVisibility(View.VISIBLE);
                    img_photo_3.setVisibility(View.VISIBLE);
                    chooseImage3 = false;
                    path3 = path_image;



                }
                else if (chooseImage4) {
                    lay_photo_4.setBackgroundResource(R.drawable.border_edittext_transparent);
                    img_photo_4.setImageBitmap(bitmap);
                    img_close_4.setVisibility(View.VISIBLE);
                    img_photo_4.setVisibility(View.VISIBLE);
                    chooseImage4 = false;
                    path4 = path_image;



                }
                else if (chooseImage5) {
                    lay_photo_5.setBackgroundResource(R.drawable.border_edittext_transparent);
                    img_photo_5.setImageBitmap(bitmap);
                    img_close_5.setVisibility(View.VISIBLE);
                    img_photo_5.setVisibility(View.VISIBLE);
                    chooseImage5 = false;
                    path5 = path_image;

                }



            }else if (requestCode == SELECT_FILE)

                if (resultCode == RESULT_OK) {
                    Uri uri = data.getData();
                    String[] projection = {MediaStore.Images.Media.DATA};

                    Cursor cursor = getActivity().getContentResolver().query(uri, projection, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(projection[0]);
                    String selectedImagePath = cursor.getString(columnIndex);
                    cursor.close();
                    Bitmap bitmap = BitmapFactory.decodeFile(selectedImagePath);
                    Drawable drawable = new BitmapDrawable(bitmap);


                    if (chooseImage1) {
                        lay_photo_1.setBackgroundResource(R.drawable.border_edittext_transparent);
                        img_photo_1.setImageDrawable(drawable);
                        img_close_1.setVisibility(View.VISIBLE);
                        img_photo_1.setVisibility(View.VISIBLE);
                        chooseImage1 = false;
                        path1 = selectedImagePath;

                    }
                    else  if (chooseImage2) {
                        lay_photo_2.setBackgroundResource(R.drawable.border_edittext_transparent);
                        img_photo_2.setImageDrawable(drawable);
                        img_close_2.setVisibility(View.VISIBLE);
                        img_photo_2.setVisibility(View.VISIBLE);
                        chooseImage2 = false;
                        path2 = selectedImagePath;


                    }
                    else  if (chooseImage3) {
                        lay_photo_3.setBackgroundResource(R.drawable.border_edittext_transparent);
                        img_photo_3.setImageDrawable(drawable);
                        img_close_3.setVisibility(View.VISIBLE);
                        img_photo_3.setVisibility(View.VISIBLE);
                        chooseImage3 = false;
                        path3 = selectedImagePath;



                    }
                    else  if (chooseImage4) {
                        lay_photo_4.setBackgroundResource(R.drawable.border_edittext_transparent);
                        img_photo_4.setImageDrawable(drawable);
                        img_close_4.setVisibility(View.VISIBLE);
                        img_photo_4.setVisibility(View.VISIBLE);
                        chooseImage4 = false;
                        path4 = selectedImagePath;


                    }
                    else  if (chooseImage5) {
                        lay_photo_5.setBackgroundResource(R.drawable.border_edittext_transparent);
                        img_photo_5.setImageDrawable(drawable);
                        img_close_5.setVisibility(View.VISIBLE);
                        img_photo_5.setVisibility(View.VISIBLE);
                        chooseImage5 = false;
                        path5 = selectedImagePath;

                    }

                }
            }
        }
    public abstract class ImageCompressionAsyncTask extends AsyncTask<String, Void, byte[]> {
        String path = "";
        Bitmap bmp;

        public ImageCompressionAsyncTask(String path) {
            this.path = path;
        }

        @Override
        protected byte[] doInBackground(String... strings) {
            if (path == null || path.equals("")) {
                return null;
            } else {
                return ImageUtils.compressImage(path);
            }

        }

        protected abstract void onPostExecute(byte[] imageBytes);
    }

    public class AddStatusAsyncTask extends AsyncTask<AddStatusRequest, Void, AddStatusResponse> {
        ProgressDialog dialog;
        AddStatusRequest addStatusRequest;
        Context ct;


        public AddStatusAsyncTask(Context ct, AddStatusRequest addStatusRequest) {
            this.ct = ct;
            this.addStatusRequest = addStatusRequest;
        }

        @Override
        protected AddStatusResponse doInBackground(AddStatusRequest... params) {
            try {
                Thread.sleep(1000);
                UserController controller = new UserController();
                return controller.addStatus(addStatusRequest);
            } catch (Exception ex) {
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(ct);
            dialog.setMessage("Đang xử lý ...");
            dialog.show();
            dialog.setCancelable(false);
        }

        @Override
        protected void onPostExecute(AddStatusResponse code) {
            try {

                if (code != null) {
                    if (code.code == 200) {
                        reset();
                        Toast.makeText(main, "Đăng tin thành công, bài đăng đang được kiểm duyệt!", Toast.LENGTH_LONG).show();
                        PublicMethob.hideKeyboard(main);
                    }
                    else
                    {
                        reset();
                        Toast.makeText(main, code.message, Toast.LENGTH_LONG).show();}
                }
            } catch (Exception e) {
                Toast.makeText(main, "Không có kết nối mạng!", Toast.LENGTH_LONG).show();
            }
            dialog.dismiss();
        }
    }

    private void uploadImage(final String path1, final String path2, final String path3, final String path4, final String path5) {
        try {

            final ProgressDialog dialog;
            dialog = new ProgressDialog(getContext());
            dialog.setMessage("Upload hình ảnh...");
            dialog.show();
            dialog.setCancelable(false);

            final File file1 = new File(path1);
            final File file2 = new File(path2);
            final File file3 = new File(path3);
            final File file4 = new File(path4);
            final File file5 = new File(path5);
            MultipartBody.Part body1 = null;
            MultipartBody.Part body2 = null;
            MultipartBody.Part body3 = null;
            MultipartBody.Part body4 = null;
            MultipartBody.Part body5 = null;
            if (file1.exists()) {
                body1 = prepareFilePart("FileNo1", path1);
            }
            if (file2.exists()) {
                body2 = prepareFilePart("FileNo2", path2);
            }
            if (file3.exists()) {
                body3 = prepareFilePart("FileNo3", path3);
            }
            if (file4.exists()) {
                body4 = prepareFilePart("FileNo4", path4);
            }
            if (file5.exists()) {
                body5 = prepareFilePart("FileNo5", path5);
            }


            final okhttp3.RequestBody userId = createPartFromString(userModel.id + "");
            okhttp3.RequestBody token = createPartFromString(userModel.ApiToken);

            APICommon.Geotes service = ServiceGenerator.GetInstance();
            Call<UploadImageResponse> call = service.uploadImage(body1, body2,body3,body4,body5, userId, token);
            call.enqueue(new Callback<UploadImageResponse>() {

                @Override
                public void onResponse(Call<UploadImageResponse> call, Response<UploadImageResponse> response) {

                    if (response.body().code == 200) {

                        dialog.dismiss();
                        Toast.makeText(getActivity(), "Upload ảnh thành công !", Toast.LENGTH_SHORT).show();

                        image_path = response.body().result;
                        AddStatusAsyncTask addStatusAsyncTask = new AddStatusAsyncTask(main, new AddStatusRequest(userModel.id, userModel.ApiToken, content, image_path));
                        addStatusAsyncTask.execute();

                    } else {
                        dialog.dismiss();
                        Toast.makeText(getActivity(), "Upload ảnh thất bại !", Toast.LENGTH_LONG).show();
                    }


                }

                @Override
                public void onFailure(Call<UploadImageResponse> call, Throwable t) {
                    dialog.dismiss();
                    Toast.makeText(getActivity(), "Upload ảnh thất bại !", Toast.LENGTH_LONG).show();
                }
            });

        } catch (Exception err) {
            err.printStackTrace();
        }
    }

    @NonNull
    private MultipartBody.Part prepareFilePart(String partName, String path) {
        try {
            File file = new File(path);
            okhttp3.RequestBody requestFile =
                    okhttp3.RequestBody.create(okhttp3.MediaType.parse(path), file);

            // MultipartBody.Part is used to send also the actual file name
            return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
        } catch (Exception err) {
            err.printStackTrace();
            return null;
        }
    }
    @NonNull
    private okhttp3.RequestBody createPartFromString(String descriptionString) {
        return okhttp3.RequestBody.create(
                MultipartBody.FORM, descriptionString);
    }


}



