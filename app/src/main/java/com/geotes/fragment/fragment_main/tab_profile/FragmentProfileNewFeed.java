package com.geotes.fragment.fragment_main.tab_profile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.geotes.R;
import com.geotes.activity.IncludeActivity;
import com.geotes.activity.MainActivity;
import com.geotes.adapter.ListViewNewFeedAdapter;
import com.geotes.api.APICommon;
import com.geotes.api.ServiceGenerator;
import com.geotes.common.CRecyclerView;
import com.geotes.common.Config;
import com.geotes.model.NewFeedModel;
import com.geotes.model.UserModel;
import com.geotes.response.GetNewFeedResponse;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.geotes.common.CommonValue.CURRENT_FRAGMENT;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_EBOOK_1;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_EBOOK_2;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_EBOOK_3;
import static com.geotes.common.CommonValue.FRAGMENT_DOCUMENT_TYPE;
import static com.geotes.common.CommonValue.FRAGMENT_EBOOK_DETAIL;
import static com.geotes.common.CommonValue.FRAGMENT_MENU;
import static com.geotes.common.CommonValue.KEY_RELOAD_PROFILE;


/**
 * Created by a Hoang on 6/28/2017.
 */

public class FragmentProfileNewFeed extends Fragment implements View.OnClickListener{
    private boolean isProgessingLoadMore = true;
    public int page_number = 1;
    public int page_size =5;
    private SwipeRefreshLayout swipeRefresh;
    public static ArrayList<NewFeedModel> arr_new_feed_profile;
    private UserModel userModel;
    private CRecyclerView list_profile_newfeed;
    private boolean isCanNext = true;
    public static ListViewNewFeedAdapter adapter_list_new_feed_profile;
    public MainActivity main;
    private TextView txt_non_post;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_newfeed, container, false);
        main = (MainActivity) getActivity();

        initUI(view);
        initEvent();
        bindData();
        return view;
    }

    private void initUI(View view) {
        swipeRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefresh);
        list_profile_newfeed = (CRecyclerView) view.findViewById(R.id.list_profile_newfeed);
        txt_non_post = (TextView) view.findViewById(R.id.txt_non_post);
    }

    private void initEvent() {
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isCanNext = true;
                swipeRefresh.setRefreshing(false);

                page_number = 1;
                adapter_list_new_feed_profile = new ListViewNewFeedAdapter(main, arr_new_feed_profile);
                arr_new_feed_profile.clear();
                loadData();
                list_profile_newfeed.setAdapter(adapter_list_new_feed_profile);
                //listview_new_feed.setNestedScrollingEnabled(false);
            }
        });

    }

    private void bindData() {

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Config.Pref, getActivity().MODE_PRIVATE);
        String profile = sharedPreferences.getString(Config.KEY_USER, "");
        Gson gson = new Gson();
        userModel = gson.fromJson(profile, UserModel.class);
        arr_new_feed_profile = new ArrayList<>();

        swipeRefresh.setColorSchemeResources(R.color.colorGreen);
        Display display = main.getWindowManager().getDefaultDisplay();
        int width = display.getWidth();  // deprecated
        int height = display.getHeight();
        adapter_list_new_feed_profile = new ListViewNewFeedAdapter(main, arr_new_feed_profile);
        list_profile_newfeed.setAdapter(adapter_list_new_feed_profile);

        loadData();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {



        }
    }
    public void loadData(){

        if (userModel!=null)
        {
            getNewFeed(0,"");
        }

    }

    private void getNewFeed(int is_like, String text_search) {
        APICommon.Geotes service = ServiceGenerator.GetInstance();
        Call<GetNewFeedResponse> getNewFeed;

            getNewFeed = service.getNewFeed(userModel.id,is_like,text_search, page_size, page_number, 2);


        getNewFeed.enqueue(new Callback<GetNewFeedResponse>() {
            @Override
            public void onResponse(Call<GetNewFeedResponse> call, Response<GetNewFeedResponse> response) {
                try {
                    isProgessingLoadMore = false;
                    if (response.body() != null && response.body().code == 200) {
                        arr_new_feed_profile.addAll(response.body().result);

                        //adapter_list_new_feed.addAll(arr_new_feed);
                        adapter_list_new_feed_profile.notifyDataSetChanged();
                        if (arr_new_feed_profile.size()==0)
                        {
                            txt_non_post.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            txt_non_post.setVisibility(View.GONE);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<GetNewFeedResponse> call, Throwable t) {
                Toast.makeText(main, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
            }


        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode  == RESULT_OK) {
            if (requestCode == KEY_RELOAD_PROFILE) {
                bindData();
            }
        }
    }
}
