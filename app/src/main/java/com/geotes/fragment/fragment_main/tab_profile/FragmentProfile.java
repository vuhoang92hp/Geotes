package com.geotes.fragment.fragment_main.tab_profile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.geotes.R;
import com.geotes.activity.IncludeActivity;
import com.geotes.activity.MainActivity;
import com.geotes.adapter.CustomFragmentPagerAdapter;
import com.geotes.api.APICommon;
import com.geotes.api.ServiceGenerator;
import com.geotes.common.Config;
import com.geotes.common.MaterialTabs;
import com.geotes.common.PublicMethob;
import com.geotes.model.UserModel;
import com.google.gson.Gson;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.geotes.common.CommonValue.CURRENT_FRAGMENT;
import static com.geotes.common.CommonValue.FRAGMENT_MENU;
import static com.geotes.common.CommonValue.FRAGMENT_PROFILE_INFO;
import static com.geotes.common.CommonValue.INCLUDE_FRAGMENT;
import static com.geotes.common.CommonValue.KEY_RELOAD_PROFILE;

/**
 * Created by meobu on 11/21/2017.
 */

public class FragmentProfile extends Fragment implements View.OnClickListener {
    private MainActivity main;
    private ImageView ivAvatar;

    private ImageView img_search;
    private ImageView ivMenu;
    private TextView tvNameUser;
    private TextView tvTitle;
    private UserModel userModel;
    private MaterialTabs tabs;
    private ViewPager viewPager;
    private LinearLayout llNewFeed, llClass, llRate;
    private TextView tvNewFeed, tvClass, tvRate;
    private CustomFragmentPagerAdapter adapter;
    private SharedPreferences sharedPreferences;

    private Gson gson;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        main = (MainActivity) getActivity();
        sharedPreferences = getActivity().getSharedPreferences(Config.Pref, getActivity().MODE_PRIVATE);
        String profile = sharedPreferences.getString(Config.KEY_USER, "");
        gson = new Gson();
        userModel = gson.fromJson(profile, UserModel.class);
        initUI(view);
        bindData();
        initEven();
        return view;
    }

    private void initEven() {
        img_search.setOnClickListener(this);
        llNewFeed.setOnClickListener(this);
        llClass.setOnClickListener(this);
        llRate.setOnClickListener(this);
        ivMenu.setOnClickListener(this);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setTabs(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void bindData() {

        img_search.setImageResource(R.mipmap.ic_edit);
        tvTitle.setText(getResources().getString(R.string.profile_page));
        if (userModel != null) {
            tvNameUser.setText(userModel.name);
            PublicMethob.showImageAvarta(main,userModel.photo,ivAvatar);

        }
        ivMenu.setVisibility(View.INVISIBLE);

        adapter = new CustomFragmentPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new FragmentProfileNewFeed(), getResources().getString(R.string.new_feed));
        adapter.addFragment(new FragmentProfileFavourite(), getResources().getString(R.string.favourite));
        adapter.addFragment(new FragmentProfileFollow(), getResources().getString(R.string.following));
        viewPager.setAdapter(adapter);
        tabs.setViewPager(viewPager);
        viewPager.setOffscreenPageLimit(3);
        setTabs(0);
    }

    private void initUI(View view) {
        ivAvatar = (ImageView) view.findViewById(R.id.iv_avatar);
        img_search = (ImageView) view.findViewById(R.id.img_search);
        tvNameUser = (TextView) view.findViewById(R.id.tv_name_user);
        tabs = (MaterialTabs) view.findViewById(R.id.tabHost);
        viewPager = (ViewPager) view.findViewById(R.id.view_pager);
        llNewFeed = (LinearLayout) view.findViewById(R.id.ll_new_feed);
        llClass = (LinearLayout) view.findViewById(R.id.ll_class);
        llRate = (LinearLayout) view.findViewById(R.id.ll_rate);
        tvNewFeed = (TextView) view.findViewById(R.id.tv_new_feed);
        tvClass = (TextView) view.findViewById(R.id.tv_class);
        tvRate = (TextView) view.findViewById(R.id.tv_rate);
        tvTitle = (TextView) view.findViewById(R.id.txt_title);
        ivMenu = (ImageView) view.findViewById(R.id.img_menu);

    }

    private void setTabs(int isTabs) {
        llNewFeed.setBackgroundResource(R.color.colorTransparent);
        llClass.setBackgroundResource(R.color.colorTransparent);
        llRate.setBackgroundResource(R.color.colorTransparent);
        tvRate.setTextColor(getResources().getColor(R.color.colorTextGrey));
        tvClass.setTextColor(getResources().getColor(R.color.colorTextGrey));
        tvNewFeed.setTextColor(getResources().getColor(R.color.colorTextGrey));

        switch (isTabs) {
            case 0:
                llNewFeed.setBackgroundResource(R.drawable.border_button_green);
                tvNewFeed.setTextColor(getResources().getColor(R.color.colorWhite));

                break;
            case 1:
                llClass.setBackgroundResource(R.drawable.border_button_green);
                tvClass.setTextColor(getResources().getColor(R.color.colorWhite));

                break;
            case 2:
                llRate.setBackgroundResource(R.drawable.border_button_green);
                tvRate.setTextColor(getResources().getColor(R.color.colorWhite));

                break;
            default:
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_new_feed:
                setTabs(0);
                viewPager.setCurrentItem(0);
                break;
            case R.id.ll_class:
                setTabs(1);
                viewPager.setCurrentItem(1);
                break;
            case R.id.ll_rate:
                setTabs(2);
                viewPager.setCurrentItem(2);
                break;
            case R.id.img_menu:

                break;
            case R.id.img_search:
                INCLUDE_FRAGMENT =  FRAGMENT_PROFILE_INFO;;
                CURRENT_FRAGMENT = FRAGMENT_PROFILE_INFO;
                Intent i = new Intent(main, IncludeActivity.class);
                startActivityForResult(i,KEY_RELOAD_PROFILE);
                break;
            default:
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode  == RESULT_OK) {
            if (requestCode == KEY_RELOAD_PROFILE) {
                String profile = sharedPreferences.getString(Config.KEY_USER, "");
                userModel = gson.fromJson(profile, UserModel.class);
                bindData();
            }
        }
    }

    /*@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_OK) {
            String profile = sharedPreferences.getString(Config.KEY_USER, "");
            userModel = gson.fromJson(profile, UserModel.class);
            bindData();
        }
    }*/
}
