package com.geotes.fragment.fragment_main;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.geotes.R;
import com.geotes.activity.IncludeActivity;
import com.geotes.activity.MainActivity;
import com.geotes.adapter.CustomImageHomeViewPager;
import com.geotes.adapter.ListViewNewFeedAdapter;
import com.geotes.adapter.ListViewNotificationAdapter;
import com.geotes.api.APICommon;
import com.geotes.api.ServiceGenerator;
import com.geotes.common.CRecyclerView;
import com.geotes.common.Config;
import com.geotes.common.PublicMethob;
import com.geotes.common.autoviewpager.AutoScrollViewPager;
import com.geotes.model.BannerModel;
import com.geotes.model.NewFeedModel;
import com.geotes.model.NotificationModel;
import com.geotes.model.UserModel;
import com.geotes.response.GetBannerResponse;
import com.geotes.response.GetNewFeedResponse;
import com.geotes.response.GetNotificationResponse;
import com.google.gson.Gson;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.geotes.common.CommonValue.CURRENT_FRAGMENT;
import static com.geotes.common.CommonValue.FRAGMENT_MENU;
import static com.geotes.common.CommonValue.INCLUDE_FRAGMENT;
import static com.geotes.common.CommonValue.KEY_RELOAD;

/**
 * Created by a Hoang on 6/28/2017.
 */

public class FragmentNotification extends Fragment implements View.OnClickListener, ViewPager.OnPageChangeListener{
    private TextView txt_title;
    private ImageView img_menu,img_search;
    private CRecyclerView listview_notification;
    private MainActivity main;
    public static  ListViewNotificationAdapter adapter_list_notification;
    private static boolean isCanNext = true;
    private static boolean isProgessingLoadMore = true;
    public static int page_number = 1;
    public static int page_size =10;
    private SwipeRefreshLayout swipeRefresh;
    public static  ArrayList<NotificationModel> arr_notification;
    private static UserModel userModel;
    private ScrollView scrollview;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        main = (MainActivity) getActivity();

        initUI(view);
        initEvent();
        bindData();
        return view;
    }

    private void initUI(View view) {
        txt_title = (TextView) view.findViewById(R.id.txt_title);
        img_menu = (ImageView) view.findViewById(R.id.img_menu);
        img_search = (ImageView) view.findViewById(R.id.img_search);
        listview_notification = (CRecyclerView) view.findViewById(R.id.listview_notification);
        swipeRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefresh);
        scrollview = (ScrollView) view.findViewById(R.id.scrollview);
    }

    private void initEvent() {
        img_menu.setOnClickListener(this);
       scrollview.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                if (scrollview != null) {
                    if (scrollview.getChildAt(0).getBottom() <= (scrollview.getHeight() + scrollview.getScrollY())) {
                        //scroll view is at bottom
                        if (isCanNext && !isProgessingLoadMore) {
                            page_number++;
                            loadData();

                            isCanNext = false;

                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    // Do something after 3s = 3000ms

                                    isCanNext = true;



                                }
                            }, 3000);
                        }
                    } else {

                        //scroll view is not at bottom
                    }
                }
            }
        });
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isCanNext = true;
                swipeRefresh.setRefreshing(false);

                page_number = 1;
                adapter_list_notification = new ListViewNotificationAdapter(main, arr_notification);
                arr_notification.clear();
                loadData();
                listview_notification.setAdapter(adapter_list_notification);
                //listview_new_feed.setNestedScrollingEnabled(false);
            }
        });

    }

    private void bindData() {
        img_search.setVisibility(View.INVISIBLE);
        img_menu.setVisibility(View.INVISIBLE);
        txt_title.setText(getResources().getString(R.string.notification));
        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Config.Pref, getActivity().MODE_PRIVATE);
        String profile = sharedPreferences.getString(Config.KEY_USER, "");
        Gson gson = new Gson();
        userModel = gson.fromJson(profile, UserModel.class);

        arr_notification = new ArrayList<>();


        swipeRefresh.setColorSchemeResources(R.color.colorGreen);
        Display display = main.getWindowManager().getDefaultDisplay();
        int width = display.getWidth();  // deprecated
        int height = display.getHeight();

        adapter_list_notification = new ListViewNotificationAdapter(main, arr_notification);
        listview_notification.setAdapter(adapter_list_notification);
        listview_notification.setNestedScrollingEnabled(false);

        loadData();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.img_menu:
                INCLUDE_FRAGMENT = FRAGMENT_MENU;
                CURRENT_FRAGMENT = FRAGMENT_MENU;
                Intent i = new Intent(main, IncludeActivity.class);
                startActivity(i);
                break;


        }
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }



    public static  void loadData(){

       if (userModel!=null)
       {
           getNotification();
       }

    }

    public static void getNotification() {
        APICommon.Geotes service = ServiceGenerator.GetInstance();
        Call<GetNotificationResponse> getNotification;

        getNotification = service.getNotification(userModel.id,userModel.ApiToken, page_size, page_number);


        getNotification.enqueue(new Callback<GetNotificationResponse>() {
            @Override
            public void onResponse(Call<GetNotificationResponse> call, Response<GetNotificationResponse> response) {
                try {
                    isProgessingLoadMore = false;
                    if (response.body() != null && response.body().code == 200) {
                        arr_notification.addAll(response.body().result);
                       adapter_list_notification.notifyDataSetChanged();

                       setNotification(arr_notification);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<GetNotificationResponse> call, Throwable t) {
               // Toast.makeText(, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
            }


        });
    }

    public static  void setNotification(ArrayList<NotificationModel> arr)
    {
        int number = 0;
        if (arr.size()>0)
        {
            for (int i = 0; i < arr.size();i++)
            {
                if (arr.get(i).is_read==0)
                {
                    number = number+1;
                }
            }

            MainActivity.setNumberNotification(number);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == KEY_RELOAD || resultCode  == RESULT_OK) {
            isCanNext = true;

            page_number = 1;
            adapter_list_notification = new ListViewNotificationAdapter(main, arr_notification);
            arr_notification.clear();
            loadData();
            listview_notification.setAdapter(adapter_list_notification);
        }
    }

    public static void reLoadNotification()
    {
        if (userModel!=null)
        {
            page_number = 1;
            arr_notification.clear();
            getNotification();
        }
    }

}
