package com.geotes.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.geotes.R;
import com.geotes.activity.CalculateActivity;
import com.geotes.activity.MainActivity;
import com.geotes.common.PublicMethob;

/**
 * Created by a Hoang on 6/28/2017.
 */

public class FragmentTracDiaNghich extends Fragment implements View.OnClickListener{
    private RelativeLayout lay_title;
    private ImageView img_back, img_reset;
    private TextView txt_title;
    private CalculateActivity main;
    private EditText edt_dd, edt_mm, edt_ss,edt_Sab,edt_xa,edt_ya;
    private TextView txt_calculate;
    private TextView txt_toa_do_xb, txt_toa_do_yb;
    private int dd, mm, ss ;
    private double  Xb, Yb, Xa, Ya,Sab;



    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_trac_dia_nghich, container, false);
        main = (CalculateActivity) getActivity();

        initUI(view);
        initEvent();
        bindData();
        return view;
    }

    private void initUI(View view) {

        lay_title = (RelativeLayout) view.findViewById(R.id.lay_title);
        img_back = (ImageView) view.findViewById(R.id.img_back);
        img_reset = (ImageView) view.findViewById(R.id.img_reset);
        txt_title = (TextView) view.findViewById(R.id.txt_title);

        edt_dd = (EditText) view.findViewById(R.id.edt_dd);
        edt_mm = (EditText) view.findViewById(R.id.edt_mm);
        edt_ss = (EditText) view.findViewById(R.id.edt_ss);
        edt_Sab = (EditText) view.findViewById(R.id.edt_Sab);
        edt_xa = (EditText) view.findViewById(R.id.edt_xa);
        edt_ya = (EditText) view.findViewById(R.id.edt_ya);

        txt_calculate = (TextView) view.findViewById(R.id.txt_calculate);
        txt_toa_do_xb = (TextView) view.findViewById(R.id.txt_toa_do_xb);
        txt_toa_do_yb = (TextView) view.findViewById(R.id.txt_toa_do_yb);

    }

    private void initEvent() {
        img_reset.setOnClickListener(this);
        img_back.setOnClickListener(this);
        txt_calculate.setOnClickListener(this);

    }

    private void bindData() {

        txt_title.setText(getResources().getString(R.string.trac_dia_nghich));
        lay_title.setBackgroundResource(R.color.colorPink);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.img_back:
                main.finish();
                break;

            case R.id.img_reset:
                reset();
                break;
            case R.id.txt_calculate:
                PublicMethob.hideKeyboard(main);
                if (checkOk())
                {
                    try {
                        dd = Integer.parseInt(edt_dd.getText().toString().trim());
                        mm = Integer.parseInt(edt_mm.getText().toString().trim());
                        ss = Integer.parseInt(edt_ss.getText().toString().trim());
                        Sab = Double.parseDouble(edt_Sab.getText().toString().trim());
                        Xa = Double.parseDouble(edt_xa.getText().toString().trim());
                        Ya = Double.parseDouble(edt_ya.getText().toString().trim());

                    } catch (Exception e)
                    {
                        Toast.makeText(main, "Dữ liệu nhập lỗi!", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                    double anpha = PublicMethob.convertDegreeToDecimal(dd,mm,ss);
                    double denta_x = Sab * Math.cos(PublicMethob.convertDegreeToRadian(anpha));
                    double denta_y = Sab * Math.sin(PublicMethob.convertDegreeToRadian(anpha));

                    Xb = PublicMethob.roundNumber(Xa + denta_x);
                    Yb = PublicMethob.roundNumber(Ya +denta_y);

                    txt_toa_do_xb.setText(Xb+"");
                    txt_toa_do_yb.setText(Yb+"");


                }
                else
                {
                    Toast.makeText(main, "Vui lòng nhập đầy đủ thông tin!", Toast.LENGTH_LONG).show();
                }
                break;

        }
    }
    public void reset()
    {
        edt_dd.setText("");
        edt_mm.setText("");
        edt_ss.setText("");
        edt_Sab.setText("");
        edt_xa.setText("");
        edt_ya.setText("");
        txt_toa_do_xb.setText("");
        txt_toa_do_yb.setText("");
    }

    private boolean checkOk()
    {
        boolean result = true;

        if (edt_dd.getText().toString().trim().equals(""))
        {
            result = false;
        }
        if (edt_mm.getText().toString().trim().equals(""))
        {
            result = false;
        }
        if (edt_ss.getText().toString().trim().equals(""))
        {
            result = false;
        }
        if (edt_Sab.getText().toString().trim().equals(""))
        {
            result = false;
        }
        if (edt_xa.getText().toString().trim().equals(""))
        {
            result = false;
        }
        if (edt_ya.getText().toString().trim().equals(""))
        {
            result = false;
        }


        return result;
    }
}
