package com.geotes.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.geotes.R;
import com.geotes.activity.IncludeActivity;
import com.geotes.adapter.ListViewDocumentAdapter;
import com.geotes.adapter.ListViewVideoAdapter;
import com.geotes.common.CRecyclerView;
import com.geotes.model.DocumentModel;
import com.geotes.model.VideoModel;

import java.util.ArrayList;

import static com.geotes.common.CommonValue.CURRENT_FRAGMENT;
import static com.geotes.common.CommonValue.FRAGMENT_MENU;

/**
 * Created by a Hoang on 6/28/2017.
 */

public class FragmentDocument extends Fragment implements View.OnClickListener{
    private ImageView img_back,img_menu;
    private TextView txt_title;
    private IncludeActivity main;

    private CRecyclerView list_document;
    private ArrayList<DocumentModel> arr_document;
    private ListViewDocumentAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_document, container, false);
        main = (IncludeActivity) getActivity();

        initUI(view);
        initEvent();
        bindData();
        return view;
    }

    private void initUI(View view) {
        img_menu = (ImageView) view.findViewById(R.id.img_menu);
        list_document = (CRecyclerView) view.findViewById(R.id.list_document);
        img_back = (ImageView) view.findViewById(R.id.img_back);
        txt_title = (TextView) view.findViewById(R.id.txt_title);

    }

    private void initEvent() {
        img_back.setOnClickListener(this);

    }

    private void bindData() {
        img_menu.setVisibility(View.INVISIBLE);
        txt_title.setText(getResources().getString(R.string.document_tutorial));

        arr_document = new ArrayList<>();
        arr_document.add(new DocumentModel(0, getResources().getString(R.string.do_chieu_dai_may_toan_dac_dien_tu)));
        arr_document.add(new DocumentModel(1, getResources().getString(R.string.do_goc_toan_may_toan_dac)));
        arr_document.add(new DocumentModel(2, getResources().getString(R.string.su_dung_may_toan_dac_nikon)));
        arr_document.add(new DocumentModel(3, getResources().getString(R.string.bo_tri_diem_ra_ngoai_thuc_dia_bang_may_toan_dac_nikon)));
        arr_document.add(new DocumentModel(4, getResources().getString(R.string.huong_dan_su_dung_may_toan_dac_topcon_gts_105n)));
        arr_document.add(new DocumentModel(5, getResources().getString(R.string.Do_cao_bang_may_thuy_binh)));
        arr_document.add(new DocumentModel(6, getResources().getString(R.string.Do_cao_luong_giac)));
        arr_document.add(new DocumentModel(7, getResources().getString(R.string.Huong_dan_do_cao_bang_may_thuy_binh)));
        arr_document.add(new DocumentModel(8, getResources().getString(R.string.Nguyen_ly_do_cao_bang_may_thuy_binh)));

        adapter = new ListViewDocumentAdapter(getActivity(), arr_document);
        list_document.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.img_back:
                main.finish();
               /* getFragmentManager().popBackStack();
                main.hideFrm(main.getFragmentDocument());
                main.showFrm(main.getFragmentTemp(),main.getFragmentMenu());*/
                CURRENT_FRAGMENT = FRAGMENT_MENU;
                break;
            
        }
    }
}
