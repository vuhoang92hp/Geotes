package com.geotes.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.geotes.R;
import com.geotes.activity.CalculateActivity;
import com.geotes.activity.IncludeActivity;
import com.geotes.common.PublicMethob;

/**
 * Created by a Hoang on 6/28/2017.
 */

public class FragmentTracDiaThuan extends Fragment implements View.OnClickListener{
    private RelativeLayout lay_title;
    private ImageView img_back, img_reset;
    private TextView txt_title;
    private CalculateActivity main;
    private EditText edt_xa, edt_ya, edt_xb,edt_yb;
    private TextView txt_calculate, txt_phuong_vi, txt_Sab;
    private double xa, ya, xb, yb;
    private double Sab,phuong_vi_ab;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_trac_dia_thuan, container, false);
        main = (CalculateActivity) getActivity();

        initUI(view);
        initEvent();
        bindData();
        return view;
    }

    private void initUI(View view) {

        lay_title = (RelativeLayout) view.findViewById(R.id.lay_title);
        img_back = (ImageView) view.findViewById(R.id.img_back);
        img_reset = (ImageView) view.findViewById(R.id.img_reset);
        txt_title = (TextView) view.findViewById(R.id.txt_title);
        edt_xa = (EditText) view.findViewById(R.id.edt_xa);
        edt_ya = (EditText) view.findViewById(R.id.edt_ya);
        edt_xb = (EditText) view.findViewById(R.id.edt_xb);
        edt_yb = (EditText) view.findViewById(R.id.edt_yb);
        txt_calculate = (TextView) view.findViewById(R.id.txt_calculate);
        txt_phuong_vi = (TextView) view.findViewById(R.id.txt_phuong_vi);
        txt_Sab = (TextView) view.findViewById(R.id.txt_Sab);

    }

    private void initEvent() {
        img_reset.setOnClickListener(this);
        img_back.setOnClickListener(this);
        txt_calculate.setOnClickListener(this);
    }

    private void bindData() {

        txt_title.setText(getResources().getString(R.string.calcu_trac_dia_thuan));
        lay_title.setBackgroundResource(R.color.colorBlue);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.img_back:
                main.finish();
                break;

            case R.id.img_reset:

                edt_xa.setText("");
                edt_ya.setText("");
                edt_xb.setText("");
                edt_yb.setText("");
                txt_phuong_vi.setText("");
                txt_Sab.setText("");

                break;
            case R.id.txt_calculate:
                PublicMethob.hideKeyboard(main);

                if (checkOk())
                {
                    try {
                        xa = Double.parseDouble(edt_xa.getText().toString().trim());
                        ya = Double.parseDouble(edt_ya.getText().toString().trim());
                        xb = Double.parseDouble(edt_xb.getText().toString().trim());
                        yb = Double.parseDouble(edt_yb.getText().toString().trim());

                    } catch (Exception e)
                    {
                        Toast.makeText(main, "Dữ liệu nhập lỗi!", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }

                    //khoang cach AB
                    //Sab = Math.sqrt(Math.pow((xb-xa),2) +Math.pow((yb-ya),2));

                    //phuong vi canh AB
                    /*double denta_Y = yb - ya;
                    double denta_X = xb - xa;

                    double R = Math.atan(Math.abs(denta_Y/denta_X));

                    if (denta_X >= 0 && denta_Y >= 0)
                    {
                        phuong_vi_ab = R;
                    }
                    else if (denta_X <= 0 && denta_Y >=0)
                    {
                        phuong_vi_ab = Math.PI - R;
                    }
                    else if (denta_X <= 0 && denta_Y <=0)
                    {
                        phuong_vi_ab = Math.PI + R;
                    }
                    else if (denta_X >= 0 && denta_Y <=0)
                    {
                        phuong_vi_ab = 2 * Math.PI - R;
                    }

                    double phuong_vi_degree = phuong_vi_ab * 180 / Math.PI;

                    txt_Sab.setText(Math.round(Sab*1000.0)/1000.0+"");*/
                    //txt_phuong_vi.setText(PublicMethob.convertDecimalToDegree(phuong_vi_degree));

                    txt_Sab.setText(Math.round(PublicMethob.khoangCachAB(xa,ya,xb,yb)*1000.0)/1000.0+"");
                    txt_phuong_vi.setText(PublicMethob.convertDecimalToDegree(PublicMethob.phuongvi(xa,ya,xb,yb)));


                }
                    else
                {
                    Toast.makeText(main, "Vui lòng nhập đầy đủ thông tin!", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }
    private boolean checkOk()
    {
        boolean result = true;

        if (edt_xa.getText().toString().trim().equals(""))
        {
            result = false;
        }
        if (edt_ya.getText().toString().trim().equals(""))
        {
            result = false;
        }
        if (edt_xb.getText().toString().trim().equals(""))
        {
            result = false;
        }
        if (edt_yb.getText().toString().trim().equals(""))
        {
            result = false;
        }

        return result;
    }
}
