package com.geotes.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.geotes.R;
import com.geotes.activity.CalculateActivity;
import com.geotes.common.PublicMethob;

import java.util.ArrayList;

/**
 * Created by a Hoang on 6/28/2017.
 */

public class FragmentDienTichDaGiac extends Fragment implements View.OnClickListener{
    private RelativeLayout lay_title;
    private ImageView img_back, img_reset;
    private TextView txt_title;
    private CalculateActivity main;
    private int number = 3;
    ArrayList<Double> arr_x;
    ArrayList<Double> arr_y;

    private EditText edt_x1, edt_x2, edt_x3, edt_x4, edt_x5, edt_x6, edt_x7, edt_x8, edt_x9, edt_x10,edt_x11,edt_x12,edt_x13,edt_x14,edt_x15,edt_x16,edt_x17,edt_x18,edt_x19,edt_x20;
    private EditText edt_y1,edt_y2,edt_y3,edt_y4,edt_y5,edt_y6,edt_y7,edt_y8,edt_y9,edt_y10,edt_y12,edt_y13,edt_y14,edt_y15,edt_y16,edt_y17,edt_y18,edt_y19,edt_y11,edt_y20;
    private LinearLayout lay_4, lay_5,lay_6,lay_7,lay_8,lay_9,lay_10,lay_11,lay_12,lay_13,lay_14,lay_15,lay_16,lay_17,lay_18,lay_19,lay_20;
    private TextView txt_add, txt_calculate,txt_dien_tich;

    private double x1, x2, x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13,x14,x15,x16,x17,x18,x19,x20;
    private double y1, y2, y3,y4,y5,y6,y7,y8,y9,y10,y11,y12,y13,y14,y15,y16,y17,y18,y19,y20;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dien_tich_da_giac, container, false);
        main = (CalculateActivity) getActivity();

        initUI(view);
        initEvent();
        bindData();
        return view;
    }

    private void initUI(View view) {

        lay_title = (RelativeLayout) view.findViewById(R.id.lay_title);
        img_back = (ImageView) view.findViewById(R.id.img_back);
        img_reset = (ImageView) view.findViewById(R.id.img_reset);
        txt_title = (TextView) view.findViewById(R.id.txt_title);
        txt_add = (TextView) view.findViewById(R.id.txt_add);
        txt_calculate = (TextView) view.findViewById(R.id.txt_calculate);
        txt_dien_tich = (TextView) view.findViewById(R.id.txt_dien_tich);

        edt_x1 = (EditText) view.findViewById(R.id.edt_x1);
        edt_x2 = (EditText) view.findViewById(R.id.edt_x2);
        edt_x3 = (EditText) view.findViewById(R.id.edt_x3);
        edt_x4 = (EditText) view.findViewById(R.id.edt_x4);
        edt_x5 = (EditText) view.findViewById(R.id.edt_x5);
        edt_x6 = (EditText) view.findViewById(R.id.edt_x6);
        edt_x7 = (EditText) view.findViewById(R.id.edt_x7);
        edt_x8 = (EditText) view.findViewById(R.id.edt_x8);
        edt_x9 = (EditText) view.findViewById(R.id.edt_x9);
        edt_x10 = (EditText) view.findViewById(R.id.edt_x10);
        edt_x11 = (EditText) view.findViewById(R.id.edt_x11);
        edt_x12 = (EditText) view.findViewById(R.id.edt_x12);
        edt_x13 = (EditText) view.findViewById(R.id.edt_x13);
        edt_x14 = (EditText) view.findViewById(R.id.edt_x14);
        edt_x15 = (EditText) view.findViewById(R.id.edt_x15);
        edt_x16 = (EditText) view.findViewById(R.id.edt_x16);
        edt_x17 = (EditText) view.findViewById(R.id.edt_x17);
        edt_x18 = (EditText) view.findViewById(R.id.edt_x18);
        edt_x19 = (EditText) view.findViewById(R.id.edt_x19);
        edt_x20 = (EditText) view.findViewById(R.id.edt_x20);

        edt_y1 = (EditText) view.findViewById(R.id.edt_y1);
        edt_y2 = (EditText) view.findViewById(R.id.edt_y2);
        edt_y3 = (EditText) view.findViewById(R.id.edt_y3);
        edt_y4 = (EditText) view.findViewById(R.id.edt_y4);
        edt_y5 = (EditText) view.findViewById(R.id.edt_y5);
        edt_y6 = (EditText) view.findViewById(R.id.edt_y6);
        edt_y7 = (EditText) view.findViewById(R.id.edt_y7);
        edt_y8 = (EditText) view.findViewById(R.id.edt_y8);
        edt_y9 = (EditText) view.findViewById(R.id.edt_y9);
        edt_y10 = (EditText) view.findViewById(R.id.edt_y10);
        edt_y11 = (EditText) view.findViewById(R.id.edt_y11);
        edt_y12 = (EditText) view.findViewById(R.id.edt_y12);
        edt_y13 = (EditText) view.findViewById(R.id.edt_y13);
        edt_y14 = (EditText) view.findViewById(R.id.edt_y14);
        edt_y15 = (EditText) view.findViewById(R.id.edt_y15);
        edt_y16 = (EditText) view.findViewById(R.id.edt_y16);
        edt_y17 = (EditText) view.findViewById(R.id.edt_y17);
        edt_y18 = (EditText) view.findViewById(R.id.edt_y18);
        edt_y19 = (EditText) view.findViewById(R.id.edt_y19);
        edt_y20 = (EditText) view.findViewById(R.id.edt_y20);

        lay_4 = (LinearLayout) view.findViewById(R.id.lay_4);
        lay_5 = (LinearLayout) view.findViewById(R.id.lay_5);
        lay_6 = (LinearLayout) view.findViewById(R.id.lay_6);
        lay_7 = (LinearLayout) view.findViewById(R.id.lay_7);
        lay_8 = (LinearLayout) view.findViewById(R.id.lay_8);
        lay_9 = (LinearLayout) view.findViewById(R.id.lay_9);
        lay_10 = (LinearLayout) view.findViewById(R.id.lay_10);
        lay_11 = (LinearLayout) view.findViewById(R.id.lay_11);
        lay_12 = (LinearLayout) view.findViewById(R.id.lay_12);
        lay_13 = (LinearLayout) view.findViewById(R.id.lay_13);
        lay_14 = (LinearLayout) view.findViewById(R.id.lay_14);
        lay_15 = (LinearLayout) view.findViewById(R.id.lay_15);
        lay_16 = (LinearLayout) view.findViewById(R.id.lay_16);
        lay_17 = (LinearLayout) view.findViewById(R.id.lay_17);
        lay_18 = (LinearLayout) view.findViewById(R.id.lay_18);
        lay_19 = (LinearLayout) view.findViewById(R.id.lay_19);
        lay_20 = (LinearLayout) view.findViewById(R.id.lay_20);
    }

    private void initEvent() {
        img_reset.setOnClickListener(this);
        img_back.setOnClickListener(this);
        txt_add.setOnClickListener(this);
        txt_calculate.setOnClickListener(this);
    }

    private void bindData() {

        txt_title.setText(getResources().getString(R.string.dien_tich_da_giac));
        lay_title.setBackgroundResource(R.color.colorBlueDark);

        arr_x = new ArrayList<>();
        arr_y = new ArrayList<>();


    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.img_back:
                main.finish();
                break;

            case R.id.img_reset:
                reset();
                break;

            case R.id.txt_add:
                if (number < 20 )
                {
                    number = number + 1;
                    setVisibleLayout(number);
                }
                break;

            case R.id.txt_calculate:
                PublicMethob.hideKeyboard(main);
                double dien_tich = 0;
                if (checkOk())
                {
                    arr_x.add(x1);
                    arr_x.add(x2);
                    arr_x.add(x3);
                    arr_x.add(x4);
                    arr_x.add(x5);
                    arr_x.add(x6);
                    arr_x.add(x7);
                    arr_x.add(x8);
                    arr_x.add(x9);
                    arr_x.add(x10);
                    arr_x.add(x11);
                    arr_x.add(x12);
                    arr_x.add(x13);
                    arr_x.add(x14);
                    arr_x.add(x15);
                    arr_x.add(x16);
                    arr_x.add(x17);
                    arr_x.add(x18);
                    arr_x.add(x19);
                    arr_x.add(x20);

                    arr_y.add(y1);
                    arr_y.add(y2);
                    arr_y.add(y3);
                    arr_y.add(y4);
                    arr_y.add(y5);
                    arr_y.add(y6);
                    arr_y.add(y7);
                    arr_y.add(y8);
                    arr_y.add(y9);
                    arr_y.add(y10);
                    arr_y.add(y11);
                    arr_y.add(y12);
                    arr_y.add(y13);
                    arr_y.add(y14);
                    arr_y.add(y15);
                    arr_y.add(y16);
                    arr_y.add(y17);
                    arr_y.add(y18);
                    arr_y.add(y19);
                    arr_y.add(y20);

                    for (int i = 0; i< number; i++)
                    {

                        if (i == number - 1)
                        {
                            dien_tich = dien_tich + (arr_x.get(0) + arr_x.get(i)) * (arr_y.get(0) - arr_y.get(i));
                        }
                        else
                        {
                            dien_tich = dien_tich + (arr_x.get(i+1) + arr_x.get(i)) * (arr_y.get(i+1) - arr_y.get(i));
                        }


                    }
                    dien_tich = dien_tich/2;
                    txt_dien_tich.setText(PublicMethob.roundNumber(Math.abs(dien_tich))+"");
                }
                else
                {
                    Toast.makeText(main, "Vui lòng nhập đầy đủ thông tin!", Toast.LENGTH_LONG).show();
                }

                break;

        }
    }
    private boolean checkOk()
    {
        boolean result = true;

        if (edt_x1.getText().toString().trim().equals(""))
        {
            result = false;
        }
        else
        {
            try {
                x1= Double.parseDouble(edt_x1.getText().toString().trim());
            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        if (edt_x2.getText().toString().trim().equals(""))
        {
            result = false;
        }else
        {
            try {
                x2= Double.parseDouble(edt_x2.getText().toString().trim());
            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        if (edt_x3.getText().toString().trim().equals(""))
        {
            result = false;
        }else
        {
            try {
                x3= Double.parseDouble(edt_x3.getText().toString().trim());
            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        if (edt_y1.getText().toString().trim().equals(""))
        {
            result = false;
        }
        else
        {
            try {
                y1= Double.parseDouble(edt_y1.getText().toString().trim());
            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        if (edt_y2.getText().toString().trim().equals(""))
        {
            result = false;
        }
        else
        {
            try {
                y2= Double.parseDouble(edt_y2.getText().toString().trim());
            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        if (edt_y3.getText().toString().trim().equals(""))
        {
            result = false;
        }
        else
        {
            try {
                y3= Double.parseDouble(edt_y3.getText().toString().trim());
            }catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        if (lay_4.getVisibility() == View.VISIBLE)
        {
            if (edt_x4.getText().toString().trim().equals(""))
            {
                result = false;
            } else
            if (edt_y4.getText().toString().trim().equals(""))
            {
                result = false;
            }else
            {
                try {
                    x4= Double.parseDouble(edt_x4.getText().toString().trim());
                    y4= Double.parseDouble(edt_y4.getText().toString().trim());
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        if (lay_5.getVisibility() == View.VISIBLE)
        {
            if (edt_x5.getText().toString().trim().equals(""))
            {
                result = false;
            }else
            if (edt_y5.getText().toString().trim().equals(""))
            {
                result = false;
            }else
            {
                try {
                    x5= Double.parseDouble(edt_x5.getText().toString().trim());
                    y5= Double.parseDouble(edt_y5.getText().toString().trim());
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        if (lay_6.getVisibility() == View.VISIBLE)
        {
            if (edt_x6.getText().toString().trim().equals(""))
            {
                result = false;
            }else
            if (edt_y6.getText().toString().trim().equals(""))
            {
                result = false;
            }else
            {
                try {
                    x6= Double.parseDouble(edt_x6.getText().toString().trim());
                    y6= Double.parseDouble(edt_y6.getText().toString().trim());
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        if (lay_7.getVisibility() == View.VISIBLE)
        {
            if (edt_x7.getText().toString().trim().equals(""))
            {
                result = false;
            }else
            if (edt_y7.getText().toString().trim().equals(""))
            {
                result = false;
            }
            else
            {
                try {
                    x7= Double.parseDouble(edt_x7.getText().toString().trim());
                    y7= Double.parseDouble(edt_y7.getText().toString().trim());
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        if (lay_8.getVisibility() == View.VISIBLE)
        {
            if (edt_x8.getText().toString().trim().equals(""))
            {
                result = false;
            }else
            if (edt_y8.getText().toString().trim().equals(""))
            {
                result = false;
            }
            else
            {
                try {
                    x8= Double.parseDouble(edt_x8.getText().toString().trim());
                    y8= Double.parseDouble(edt_y8.getText().toString().trim());
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        if (lay_9.getVisibility() == View.VISIBLE)
        {
            if (edt_x9.getText().toString().trim().equals(""))
            {
                result = false;
            }else
            if (edt_y9.getText().toString().trim().equals(""))
            {
                result = false;
            }else
            {
                try {
                    x9= Double.parseDouble(edt_x9.getText().toString().trim());
                    y9= Double.parseDouble(edt_y9.getText().toString().trim());
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        if (lay_10.getVisibility() == View.VISIBLE)
        {
            if (edt_x10.getText().toString().trim().equals(""))
            {
                result = false;
            }else
            if (edt_y10.getText().toString().trim().equals(""))
            {
                result = false;
            }else
            {
                try {
                    x10= Double.parseDouble(edt_x10.getText().toString().trim());
                    y10= Double.parseDouble(edt_y10.getText().toString().trim());
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        if (lay_11.getVisibility() == View.VISIBLE)
        {
            if (edt_x11.getText().toString().trim().equals(""))
            {
                result = false;
            }else
            if (edt_y11.getText().toString().trim().equals(""))
            {
                result = false;
            }else
            {
                try {
                    x11= Double.parseDouble(edt_x11.getText().toString().trim());
                    y11= Double.parseDouble(edt_y11.getText().toString().trim());
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        if (lay_12.getVisibility() == View.VISIBLE)
        {
            if (edt_x12.getText().toString().trim().equals(""))
            {
                result = false;
            }else
            if (edt_y12.getText().toString().trim().equals(""))
            {
                result = false;
            }
            else
            {
                try {
                    x12= Double.parseDouble(edt_x12.getText().toString().trim());
                    y12= Double.parseDouble(edt_y12.getText().toString().trim());
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        if (lay_13.getVisibility() == View.VISIBLE)
        {
            if (edt_x13.getText().toString().trim().equals(""))
            {
                result = false;
            }else
            if (edt_y13.getText().toString().trim().equals(""))
            {
                result = false;
            }
            else
            {
                try {
                    x13= Double.parseDouble(edt_x13.getText().toString().trim());
                    y13= Double.parseDouble(edt_y13.getText().toString().trim());
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        if (lay_14.getVisibility() == View.VISIBLE)
        {
            if (edt_x14.getText().toString().trim().equals(""))
            {
                result = false;
            }else
            if (edt_y14.getText().toString().trim().equals(""))
            {
                result = false;
            }
            else
            {
                try {
                    x14= Double.parseDouble(edt_x14.getText().toString().trim());
                    y14= Double.parseDouble(edt_y14.getText().toString().trim());
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        if (lay_15.getVisibility() == View.VISIBLE)
        {
            if (edt_x15.getText().toString().trim().equals(""))
            {
                result = false;
            }else
            if (edt_y15.getText().toString().trim().equals(""))
            {
                result = false;
            }
            else
            {
                try {
                    x15= Double.parseDouble(edt_x15.getText().toString().trim());
                    y15= Double.parseDouble(edt_y15.getText().toString().trim());
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        if (lay_16.getVisibility() == View.VISIBLE)
        {
            if (edt_x16.getText().toString().trim().equals(""))
            {
                result = false;
            }else
            if (edt_y16.getText().toString().trim().equals(""))
            {
                result = false;
            }
            else
            {
                try {
                    x16= Double.parseDouble(edt_x16.getText().toString().trim());
                    y16= Double.parseDouble(edt_y16.getText().toString().trim());
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        if (lay_17.getVisibility() == View.VISIBLE)
        {
            if (edt_x17.getText().toString().trim().equals(""))
            {
                result = false;
            }else
            if (edt_y17.getText().toString().trim().equals(""))
            {
                result = false;
            }else
            {
                try {
                    x17= Double.parseDouble(edt_x17.getText().toString().trim());
                    y17= Double.parseDouble(edt_y17.getText().toString().trim());
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        if (lay_18.getVisibility() == View.VISIBLE)
        {
            if (edt_x18.getText().toString().trim().equals(""))
            {
                result = false;
            }else
            if (edt_y18.getText().toString().trim().equals(""))
            {
                result = false;
            }else
            {
                try {
                    x18= Double.parseDouble(edt_x18.getText().toString().trim());
                    y18= Double.parseDouble(edt_y18.getText().toString().trim());
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        if (lay_19.getVisibility() == View.VISIBLE)
        {
            if (edt_x19.getText().toString().trim().equals(""))
            {
                result = false;
            }else
            if (edt_y19.getText().toString().trim().equals(""))
            {
                result = false;
            }else
            {
                try {
                    x19= Double.parseDouble(edt_x19.getText().toString().trim());
                    y19= Double.parseDouble(edt_y19.getText().toString().trim());
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        if (lay_20.getVisibility() == View.VISIBLE)
        {
            if (edt_x20.getText().toString().trim().equals(""))
            {
                result = false;
            }else
            if (edt_y20.getText().toString().trim().equals(""))
            {
                result = false;
            }else
            {
                try {
                    x20= Double.parseDouble(edt_x20.getText().toString().trim());
                    y20= Double.parseDouble(edt_y20.getText().toString().trim());
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
        return result;
    }

    private  void reset()
    {
        number = 3;
        edt_x1.setText("");
        edt_x2.setText("");
        edt_x3.setText("");
        edt_x4.setText("");
        edt_x5.setText("");
        edt_x6.setText("");
        edt_x7.setText("");
        edt_x8.setText("");
        edt_x9.setText("");
        edt_x10.setText("");
        edt_x11.setText("");
        edt_x12.setText("");
        edt_x13.setText("");
        edt_x14.setText("");
        edt_x15.setText("");
        edt_x16.setText("");
        edt_x17.setText("");
        edt_x18.setText("");
        edt_x19.setText("");
        edt_x20.setText("");
        edt_y1.setText("");
        edt_y2.setText("");
        edt_y3.setText("");
        edt_y4.setText("");
        edt_y5.setText("");
        edt_y6.setText("");
        edt_y7.setText("");
        edt_y8.setText("");
        edt_y9.setText("");
        edt_y10.setText("");
        edt_y11.setText("");
        edt_y12.setText("");
        edt_y13.setText("");
        edt_y14.setText("");
        edt_y15.setText("");
        edt_y16.setText("");
        edt_y17.setText("");
        edt_y18.setText("");
        edt_y19.setText("");
        edt_y20.setText("");
        txt_dien_tich.setText("");
        lay_4.setVisibility(View.GONE);
        lay_5.setVisibility(View.GONE);
        lay_6.setVisibility(View.GONE);
        lay_7.setVisibility(View.GONE);
        lay_8.setVisibility(View.GONE);
        lay_9.setVisibility(View.GONE);
        lay_10.setVisibility(View.GONE);
        lay_11.setVisibility(View.GONE);
        lay_12.setVisibility(View.GONE);
        lay_13.setVisibility(View.GONE);
        lay_14.setVisibility(View.GONE);
        lay_15.setVisibility(View.GONE);
        lay_16.setVisibility(View.GONE);
        lay_17.setVisibility(View.GONE);
        lay_18.setVisibility(View.GONE);
        lay_19.setVisibility(View.GONE);
        lay_20.setVisibility(View.GONE);
    }
    public void setVisibleLayout(int number)
    {
        switch (number)
        {
            case 4:
                lay_4.setVisibility(View.VISIBLE);
                break;
            case 5:
                lay_5.setVisibility(View.VISIBLE);

                break;
            case 6:
                lay_6.setVisibility(View.VISIBLE);
                break;
            case 7:
                lay_7.setVisibility(View.VISIBLE);
                break;
            case 8:
                lay_8.setVisibility(View.VISIBLE);
                break;
            case 9:
                lay_9.setVisibility(View.VISIBLE);
                break;
            case 10:
                lay_10.setVisibility(View.VISIBLE);
                break;
            case 11:
                lay_11.setVisibility(View.VISIBLE);
                break;
            case 12:
                lay_12.setVisibility(View.VISIBLE);
                break;
            case 13:
                lay_13.setVisibility(View.VISIBLE);
                break;
            case 14:
                lay_14.setVisibility(View.VISIBLE);
                break;
            case 15:
                lay_15.setVisibility(View.VISIBLE);
                break;
            case 16:
                lay_16.setVisibility(View.VISIBLE);
                break;
            case 17:
                lay_17.setVisibility(View.VISIBLE);
                break;
            case 18:
                lay_18.setVisibility(View.VISIBLE);
                break;
            case 19:
                lay_19.setVisibility(View.VISIBLE);
                break;
            case 20:
                lay_20.setVisibility(View.VISIBLE);
                break;
        }
    }

}
