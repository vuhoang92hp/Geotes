package com.geotes.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.geotes.R;
import com.geotes.activity.CalculateActivity;
import com.geotes.common.PublicMethob;

/**
 * Created by a Hoang on 6/28/2017.
 */

public class FragmentTinhThongSoDuongCong extends Fragment implements View.OnClickListener{
    private RelativeLayout lay_title;
    private ImageView img_back, img_reset;
    private TextView txt_title;
    private CalculateActivity main;

    private EditText edt_r, edt_l, edt_k, edt_dd, edt_mm, edt_ss;
    private TextView txt_calculate;
    private double R,l,k;
    private int dd,mm,ss;

    private TextView txt_stt_1, txt_k_1, txt_x_1, txt_y_1;
    private TextView txt_stt_2, txt_k_2, txt_x_2, txt_y_2;
    private TextView  txt_x_3, txt_y_3;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(com.geotes.R.layout.fragment_tinh_thong_so_duong_cong, container, false);

        main = (CalculateActivity) getActivity();

        initUI(view);
        initEvent();
        bindData();
        return view;
    }

    private void initUI(View view) {

        lay_title = (RelativeLayout) view.findViewById(com.geotes.R.id.lay_title);
        img_back = (ImageView) view.findViewById(com.geotes.R.id.img_back);
        img_reset = (ImageView) view.findViewById(com.geotes.R.id.img_reset);
        txt_title = (TextView) view.findViewById(com.geotes.R.id.txt_title);

        txt_calculate = (TextView) view.findViewById(com.geotes.R.id.txt_calculate);


        edt_r = (EditText) view.findViewById(com.geotes.R.id.edt_r);
        edt_l = (EditText) view.findViewById(com.geotes.R.id.edt_l);
        edt_k = (EditText) view.findViewById(com.geotes.R.id.edt_k);
        edt_dd = (EditText) view.findViewById(com.geotes.R.id.edt_dd);
        edt_mm = (EditText) view.findViewById(com.geotes.R.id.edt_mm);
        edt_ss = (EditText) view.findViewById(com.geotes.R.id.edt_ss);

        txt_stt_1 = (TextView) view.findViewById(com.geotes.R.id.txt_stt_1);
        txt_k_1 = (TextView) view.findViewById(com.geotes.R.id.txt_k_1);
        txt_x_1 = (TextView) view.findViewById(com.geotes.R.id.txt_x_1);
        txt_y_1 = (TextView) view.findViewById(com.geotes.R.id.txt_y_1);

        txt_stt_2 = (TextView) view.findViewById(com.geotes.R.id.txt_stt_2);
        txt_k_2 = (TextView) view.findViewById(com.geotes.R.id.txt_k_2);
        txt_x_2 = (TextView) view.findViewById(com.geotes.R.id.txt_x_2);
        txt_y_2 = (TextView) view.findViewById(com.geotes.R.id.txt_y_2);


        txt_x_3 = (TextView) view.findViewById(com.geotes.R.id.txt_x_3);
        txt_y_3 = (TextView) view.findViewById(com.geotes.R.id.txt_y_3);


    }

    private void initEvent() {
        img_reset.setOnClickListener(this);
        img_back.setOnClickListener(this);
        txt_calculate.setOnClickListener(this);
    }

    private void bindData() {

        txt_title.setText(getResources().getString(com.geotes.R.string.calcu_tinh_thong_so_duong_cong));
        lay_title.setBackgroundResource(com.geotes.R.color.colorGreenCyan);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case com.geotes.R.id.img_back:
                main.finish();
                break;

            case com.geotes.R.id.img_reset:
                reset();
                break;
            case com.geotes.R.id.txt_calculate:
                if (checkOk())
                {
                        PublicMethob.hideKeyboard(main);
                    try {
                        R = Double.parseDouble(edt_r.getText().toString().trim());
                        l = Double.parseDouble(edt_l.getText().toString().trim());
                        k = Double.parseDouble(edt_k.getText().toString().trim());
                        dd = Integer.parseInt(edt_dd.getText().toString().trim());
                        mm = Integer.parseInt(edt_mm.getText().toString().trim());
                        ss = Integer.parseInt(edt_ss.getText().toString().trim());

                    }
                    catch (Exception e)
                    {
                        Toast.makeText(main, "Dữ liệu nhập lỗi!", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }

                    //goc chan cung A
                    double A = PublicMethob.convertDegreeToDecimal(dd,mm,ss);

                    //doan tiep cu T
                    double T = R* Math.tan(Math.toRadians(A/2 ) );
                    //chieu dai duong cong K
                    double K = (R * Math.PI * A)/180;
                    //chieu dai doan phan cu B
                    double B = R * (1/Math.cos(Math.toRadians(A/2)) - 1);
                    // do rut ngan duong cong D
                    double D = 2*T-K;
                    //Chieu dai day cung DC
                    double DC = 2*R*Math.sin(Math.toRadians(A/2));

                    double t = (l/2)*(1-(Math.pow(l,2))/(120*Math.pow(R,2)));
                    double p = (Math.pow(l,2))/(24*R)*(1-Math.pow(l,2)/(112*Math.pow(R,2)));

                    //Doan phan cu moi
                    double new_B = B+p;
                    //Doan tiep cu moi
                    double new_T = T+t;
                    //do giam cua goc ngoat
                    double phila = l/(2*R);
                    //Đoạn dài của nửa đường cong tròn mới
                    double R1=R-p;
                    //double K1= (R1 * Math.PI * (A - phila * 2)/180)/2;
                    double K1 = R1 * Math.PI * (A - (2*180* phila)/Math.PI) / 360;




                    //--------------------------------
                    //Tọa độ các điểm chi tiết trên đường cong chuyển tiếp theo công thức


                    //double x_result_1 = k*(1-Math.pow(k,4)/(40*Math.pow(R,2)*Math.pow(l,2)));
                    //double y_result_1 = (Math.pow(k,3)/(6*R*l)) * (1 - Math.pow(k,4)/(40*Math.pow(R,2)*Math.pow(l,2)));

                    int quantity = ((int)l/(int)k);
                    String stt_i = "", k_i = "",x_i = "",y_i = "";


                    for (int i = 0; i < quantity; i ++)
                    {
                        stt_i = stt_i + (i + 1)+"\n";
                        k_i = k_i + (k*(i+1)) + "\n";
                        x_i = x_i +  PublicMethob.roundNumber((k*(i+1))*(1-Math.pow(k*(i+1),4)/(40*Math.pow(R,2)*Math.pow(l,2)))) + "\n";
                        y_i = y_i + PublicMethob.roundNumber((Math.pow((k*(i+1)),3)/(6*R*l)) * (1 - Math.pow((k*(i+1)),4)/(40*Math.pow(R,2)*Math.pow(l,2))))+ "\n";
                    }

                    txt_stt_1.setText(stt_i);
                    txt_k_1.setText(k_i);
                    txt_x_1.setText(x_i);
                    txt_y_1.setText(y_i);
                    //--------------------------------


                    //--------------------------------
                    //Toạ độ các điểm đường cong tròn
                    int quantity_2 = ((int)K1/(int)k);
                    String stt_i_2 = "", k_i_2 = "",x_i_2 = "",y_i_2 = "";


                    for (int i = 0; i < quantity_2; i++)
                    {
                        stt_i_2 = stt_i_2 + (i+1)+"\n";
                        k_i_2 = k_i_2 + (k*(i+1)) + "\n";
                        x_i_2 = x_i_2 + PublicMethob.roundNumber(t + R1 * Math.sin((phila+ (i+1)*k/R1)))+"\n";
                        y_i_2 = y_i_2 + PublicMethob.roundNumber(R-R1*Math.cos((phila+ ((i+1)*k/R1))))+ "\n";
                    }
                    txt_stt_2.setText(stt_i_2);
                    txt_k_2.setText(k_i_2);
                    txt_x_2.setText(x_i_2);
                    txt_y_2.setText(y_i_2);

                    //----------------------------------

                    //Tọa độ điểm giữa cong tròn

                    String x_giua = PublicMethob.roundNumber((T+t) - (B+p)*Math.sin(Math.toRadians(A/2)))+"";
                    String y_giua = PublicMethob.roundNumber((B+p)*Math.cos(Math.toRadians(A/2)))+"";
                    txt_x_3.setText(x_giua);
                    txt_y_3.setText(y_giua);


                } else
                {
                    Toast.makeText(main, "Vui lòng nhập đầy đủ thông tin!", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private  void reset()
    {
        edt_r.setText("");
        edt_l.setText("");
        edt_k.setText("");
        edt_dd.setText("");
        edt_mm.setText("");
        edt_ss.setText("");
        txt_stt_1.setText("0");
        txt_k_1.setText("0.0");
        txt_x_1.setText("0.0");
        txt_y_1.setText("0.0");
        txt_stt_2.setText("0");
        txt_k_2.setText("0.0");
        txt_x_2.setText("0.0");
        txt_y_2.setText("0.0");
    }

    private boolean checkOk()
    {
        boolean result = true;

        if (edt_r.getText().toString().trim().equals(""))
        {
            result = false;
        }
        if (edt_l.getText().toString().trim().equals(""))
        {
            result = false;
        }
        if (edt_k.getText().toString().trim().equals(""))
        {
            result = false;
        }
        if (edt_dd.getText().toString().trim().equals(""))
        {
            result = false;
        }
        if (edt_mm.getText().toString().trim().equals(""))
        {
            result = false;
        }
        if (edt_ss.getText().toString().trim().equals(""))
        {
            result = false;
        }


        return result;
    }
}
