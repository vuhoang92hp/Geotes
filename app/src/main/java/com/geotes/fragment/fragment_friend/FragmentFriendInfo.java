package com.geotes.fragment.fragment_friend;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.geotes.R;
import com.geotes.activity.FriendProfileActivity;
import com.geotes.adapter.ListViewFollowAdapter;
import com.geotes.api.APICommon;
import com.geotes.api.ServiceGenerator;
import com.geotes.common.CRecyclerView;
import com.geotes.common.Config;
import com.geotes.model.FollowModel;
import com.geotes.model.FriendProfileModel;
import com.geotes.model.UserModel;
import com.geotes.response.GetFollowResponse;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by a Hoang on 6/28/2017.
 */

public class FragmentFriendInfo extends Fragment implements View.OnClickListener{


    private FriendProfileModel friendProfileModel;
    private TextView txt_friend_name, txt_friend_sdt, txt_friend_email, txt_friend_address;



    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_friend, container, false);


        initUI(view);
        initEvent();
        bindData();
        return view;
    }

    private void initUI(View view) {

        txt_friend_name = (TextView) view.findViewById(R.id.txt_friend_name);
        txt_friend_sdt = (TextView) view.findViewById(R.id.txt_friend_sdt);
        txt_friend_email = (TextView) view.findViewById(R.id.txt_friend_email);
        txt_friend_address = (TextView) view.findViewById(R.id.txt_friend_address);


    }

    private void initEvent() {

    }

    public void bindData() {


        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Config.Pref, getActivity().MODE_PRIVATE);
        String profile = sharedPreferences.getString(Config.KEY_FRIEND, "");
        Gson gson = new Gson();
        friendProfileModel = gson.fromJson(profile, FriendProfileModel.class);

        if (friendProfileModel!=null)
        {
            txt_friend_name.setText(friendProfileModel.name);
            txt_friend_sdt.setText(friendProfileModel.phone_number);
            txt_friend_email.setText(friendProfileModel.email);
            txt_friend_address.setText(friendProfileModel.address);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {

        }
    }


}
