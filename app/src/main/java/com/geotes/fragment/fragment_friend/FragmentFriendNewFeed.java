package com.geotes.fragment.fragment_friend;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.geotes.R;
import com.geotes.activity.FriendProfileActivity;
import com.geotes.adapter.ListViewFriendNewFeedAdapter;
import com.geotes.api.APICommon;
import com.geotes.api.ServiceGenerator;
import com.geotes.common.CRecyclerView;
import com.geotes.common.Config;
import com.geotes.model.FriendProfileModel;
import com.geotes.model.NewFeedModel;
import com.geotes.model.UserModel;
import com.geotes.response.GetNewFeedResponse;
import com.geotes.utils.Utils;
import com.google.gson.Gson;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by a Hoang on 6/28/2017.
 */

public class FragmentFriendNewFeed extends Fragment implements View.OnClickListener{
    private boolean isProgessingLoadMore = true;
    public int page_number = 1;
    public int page_size =5;
    private SwipeRefreshLayout swipeRefresh;
    public static ArrayList<NewFeedModel> arr_new_feed;
    private FriendProfileModel friendProfileModel;
    private CRecyclerView list_profile_newfeed;
    private boolean isCanNext = true;
    public static ListViewFriendNewFeedAdapter adapter_list_new_feed;
    public FriendProfileActivity main;
    private TextView txt_non_post;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_newfeed, container, false);
        main = (FriendProfileActivity) getActivity();

        initUI(view);
        initEvent();
        bindData();
        return view;
    }

    private void initUI(View view) {
        swipeRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefresh);
        list_profile_newfeed = (CRecyclerView) view.findViewById(R.id.list_profile_newfeed);
        txt_non_post = (TextView) view.findViewById(R.id.txt_non_post);
    }

    private void initEvent() {
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isCanNext = true;
                swipeRefresh.setRefreshing(false);

                page_number = 1;
                adapter_list_new_feed = new ListViewFriendNewFeedAdapter(main, arr_new_feed);
                arr_new_feed.clear();
                loadData();
                list_profile_newfeed.setAdapter(adapter_list_new_feed);
                //listview_new_feed.setNestedScrollingEnabled(false);
            }
        });

        /*Loadmore*/
        list_profile_newfeed.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (Utils.isReadyForPullEnd(recyclerView) && isCanNext && !isProgessingLoadMore) {
                    page_number++;
                    loadData();
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });

    }

    private void bindData() {

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(Config.Pref, getActivity().MODE_PRIVATE);
        String profile = sharedPreferences.getString(Config.KEY_FRIEND, "");
        Gson gson = new Gson();
        friendProfileModel = gson.fromJson(profile, FriendProfileModel.class);
        arr_new_feed = new ArrayList<>();

        swipeRefresh.setColorSchemeResources(R.color.colorGreen);
        Display display = main.getWindowManager().getDefaultDisplay();
        int width = display.getWidth();  // deprecated
        int height = display.getHeight();
        adapter_list_new_feed = new ListViewFriendNewFeedAdapter(main, arr_new_feed);
        list_profile_newfeed.setAdapter(adapter_list_new_feed);

        loadData();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {



        }
    }
    private void loadData(){

        if (friendProfileModel!=null)
        {
            getNewFeed(0,"");
        }

    }

    private void getNewFeed(int is_like, String text_search) {
        APICommon.Geotes service = ServiceGenerator.GetInstance();
        Call<GetNewFeedResponse> getNewFeed;

            getNewFeed = service.getNewFeed(friendProfileModel.id,is_like,text_search, page_size, page_number, 2);


        getNewFeed.enqueue(new Callback<GetNewFeedResponse>() {
            @Override
            public void onResponse(Call<GetNewFeedResponse> call, Response<GetNewFeedResponse> response) {
                try {
                    isProgessingLoadMore = false;
                    if (response.body() != null && response.body().code == 200) {
                        arr_new_feed.addAll(response.body().result);

                        //adapter_list_new_feed.addAll(arr_new_feed);
                        adapter_list_new_feed.notifyDataSetChanged();
                        if (arr_new_feed.size()==0)
                        {
                            txt_non_post.setVisibility(View.VISIBLE);
                        }
                        else
                        {
                            txt_non_post.setVisibility(View.GONE);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<GetNewFeedResponse> call, Throwable t) {
                Toast.makeText(main, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
            }


        });
    }
}
