package com.geotes.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.geotes.R;
import com.geotes.activity.CalculateActivity;
import com.geotes.common.PublicMethob;

/**
 * Created by a Hoang on 6/28/2017.
 */

public class FragmentSoCaDo extends Fragment implements View.OnClickListener{
    private RelativeLayout lay_title;
    private ImageView img_back, img_reset;
    private TextView txt_title;
    private CalculateActivity main;

    private EditText edt_so_diem_luoi, edt_so_lan_dat, edt_so_may_thu;
    private TextView txt_calculate, txt_so_ca_do;
    private int so_diem_do, so_may_giu_lai, so_may_thu;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_so_ca_do, container, false);
        main = (CalculateActivity) getActivity();

        initUI(view);
        initEvent();
        bindData();
        return view;
    }

    private void initUI(View view) {

        lay_title = (RelativeLayout) view.findViewById(R.id.lay_title);
        img_back = (ImageView) view.findViewById(R.id.img_back);
        img_reset = (ImageView) view.findViewById(R.id.img_reset);
        txt_title = (TextView) view.findViewById(R.id.txt_title);
        txt_calculate = (TextView) view.findViewById(R.id.txt_calculate);
        txt_so_ca_do = (TextView) view.findViewById(R.id.txt_so_ca_do);
        edt_so_diem_luoi = (EditText) view.findViewById(R.id.edt_so_diem_luoi);
        edt_so_lan_dat = (EditText) view.findViewById(R.id.edt_so_lan_dat);
        edt_so_may_thu = (EditText) view.findViewById(R.id.edt_so_may_thu);

    }

    private void initEvent() {
        img_reset.setOnClickListener(this);
        img_back.setOnClickListener(this);
        txt_calculate.setOnClickListener(this);

    }

    private void bindData() {

        txt_title.setText(getResources().getString(R.string.so_ca_do));
            lay_title.setBackgroundResource(R.color.colorRed);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.img_back:
                main.finish();
                break;

            case R.id.img_reset:
                reset();
                break;
            case R.id.txt_calculate:
                PublicMethob.hideKeyboard(main);
                if (checkOk())
                {

                    try {
                        so_diem_do = Integer.parseInt(edt_so_diem_luoi.getText().toString().trim());
                        so_may_giu_lai = Integer.parseInt(edt_so_lan_dat.getText().toString().trim());
                        so_may_thu = Integer.parseInt(edt_so_may_thu.getText().toString().trim());

                        if (so_may_thu - so_may_giu_lai == 0)
                        {
                            Toast.makeText(main, "Vui lòng nhập số máy thu khác số máy giữ lại!", Toast.LENGTH_LONG).show();
                            return;
                        }
                    }
                     catch (Exception e)
                     {
                         Toast.makeText(main, "Dữ liệu nhập lỗi!", Toast.LENGTH_LONG).show();
                         e.printStackTrace();
                     }

                     double so_ca_do = 1 + ((double)(so_diem_do - so_may_thu)/(so_may_thu - so_may_giu_lai));

                    if (so_ca_do - (int)so_ca_do != 0)
                    {
                        so_ca_do = (int)so_ca_do + 1;
                    }

                    txt_so_ca_do.setText(so_ca_do+"");

                }
                else
                {
                    Toast.makeText(main, "Vui lòng nhập đầy đủ thông tin!", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private boolean checkOk()
    {
        boolean result = true;

        if (edt_so_diem_luoi.getText().toString().trim().equals(""))
        {
            result = false;
        }
        if (edt_so_lan_dat.getText().toString().trim().equals(""))
        {
            result = false;
        }
        if (edt_so_may_thu.getText().toString().trim().equals(""))
        {
            result = false;
        }




        return result;
    }
    private void reset()
    {
        edt_so_diem_luoi.setText("");
        edt_so_lan_dat.setText("");
        edt_so_may_thu.setText("");
        txt_so_ca_do.setText("");
    }
}
