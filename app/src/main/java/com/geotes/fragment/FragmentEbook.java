package com.geotes.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.geotes.R;
import com.geotes.activity.IncludeActivity;
import com.geotes.activity.MainActivity;

import static com.geotes.common.CommonValue.CURRENT_FRAGMENT;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_EBOOK_1;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_EBOOK_2;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_EBOOK_3;
import static com.geotes.common.CommonValue.FRAGMENT_DOCUMENT_TYPE;
import static com.geotes.common.CommonValue.FRAGMENT_EBOOK_DETAIL;
import static com.geotes.common.CommonValue.FRAGMENT_MENU;
import static com.geotes.common.CommonValue.INCLUDE_FRAGMENT;


/**
 * Created by a Hoang on 6/28/2017.
 */

public class FragmentEbook extends Fragment implements View.OnClickListener{
    private ImageView img_back, img_menu;
    private TextView txt_title;
    private IncludeActivity main;
    private LinearLayout lay_toan_dac_dien_tu,lay_huong_dan_do_cao, lay_gps_map;



    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ebook, container, false);
        main = (IncludeActivity) getActivity();

        initUI(view);
        initEvent();
        bindData();
        return view;
    }

    private void initUI(View view) {

        img_back = (ImageView) view.findViewById(R.id.img_back);
        img_menu = (ImageView) view.findViewById(R.id.img_menu);
        txt_title = (TextView) view.findViewById(R.id.txt_title);
        lay_toan_dac_dien_tu = (LinearLayout) view.findViewById(R.id.lay_toan_dac_dien_tu);
        lay_huong_dan_do_cao = (LinearLayout) view.findViewById(R.id.lay_huong_dan_do_cao);
        lay_gps_map = (LinearLayout) view.findViewById(R.id.lay_gps_map);

    }

    private void initEvent() {
        img_menu.setOnClickListener(this);
        img_back.setOnClickListener(this);
        lay_toan_dac_dien_tu.setOnClickListener(this);
        lay_huong_dan_do_cao.setOnClickListener(this);
        lay_gps_map.setOnClickListener(this);

    }

    private void bindData() {
        img_menu.setVisibility(View.INVISIBLE);
        txt_title.setText(getResources().getString(R.string.ebook));


    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.img_back:
                main.finish();
                /*getFragmentManager().popBackStack();
                main.hideFrm(main.getFragmentEbook());
                main.showFrm(main.getFragmentTemp(),main.getFragmentMenu());*/


                CURRENT_FRAGMENT = FRAGMENT_MENU;
                break;

            case R.id.lay_toan_dac_dien_tu:

                FRAGMENT_DOCUMENT_TYPE = FDOCUMENT_TYPE_EBOOK_1;
                CURRENT_FRAGMENT = FRAGMENT_EBOOK_DETAIL;
              main.addFrm(main.getFragmentEbookDetail());

                break;
            case R.id.lay_huong_dan_do_cao:

                FRAGMENT_DOCUMENT_TYPE = FDOCUMENT_TYPE_EBOOK_2;
                CURRENT_FRAGMENT = FRAGMENT_EBOOK_DETAIL;
                main.addFrm(main.getFragmentEbookDetail());

                break;

            case R.id.lay_gps_map:

                FRAGMENT_DOCUMENT_TYPE = FDOCUMENT_TYPE_EBOOK_3;
                CURRENT_FRAGMENT = FRAGMENT_EBOOK_DETAIL;
                main.addFrm(main.getFragmentEbookDetail());

                break;


        }
    }

}
