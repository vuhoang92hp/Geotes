package com.geotes.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.geotes.R;
import com.geotes.activity.IncludeActivity;
import com.geotes.common.Config;
import com.geotes.controller.UserController;
import com.geotes.model.UserModel;
import com.geotes.request.AddFeedbackRequest;
import com.geotes.response.AddFeedbackResponse;
import com.google.gson.Gson;
import static android.content.Context.MODE_PRIVATE;

/**
 * Created by a Hoang on 6/28/2017.
 */

public class FragmentFeedback extends Fragment implements View.OnClickListener{
    private ImageView img_back, img_menu;
    private TextView txt_title,txt_course_register;
    private IncludeActivity main;
   private EditText edt_email, edt_content;
   private String email, content;
    private UserModel userModel;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_feedback, container, false);
        main = (IncludeActivity) getActivity();

        initUI(view);
        initEvent();
        bindData();
        return view;
    }

    private void initUI(View view) {

        img_back = (ImageView) view.findViewById(R.id.img_back);
        img_menu = (ImageView) view.findViewById(R.id.img_menu);
        txt_title = (TextView) view.findViewById(R.id.txt_title);
        txt_course_register = (TextView) view.findViewById(R.id.txt_course_register);
        edt_email = (EditText) view.findViewById(R.id.edt_email);
        edt_content = (EditText) view.findViewById(R.id.edt_content);
    }

    private void initEvent() {

        img_back.setOnClickListener(this);
        txt_course_register.setOnClickListener(this);

    }

    private void bindData() {
        img_menu.setVisibility(View.INVISIBLE);
        SharedPreferences sharedPreferences = main.getSharedPreferences(Config.Pref, MODE_PRIVATE);
        String profile = sharedPreferences.getString(Config.KEY_USER, "");
        Gson gson = new Gson();
        userModel = gson.fromJson(profile, UserModel.class);
        txt_title.setText(getResources().getString(R.string.feedback));

        if (userModel!= null)
        {
            edt_email.setText(userModel.email);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.img_back:
                main.finish();
               /* reset();
                main.showFrm(main.getFragmentTemp(), main.getFragmentMenu());*/
                break;

            case R.id.txt_course_register:
                email = edt_email.getText().toString().trim();
                content = edt_content.getText().toString().trim();

                if (checkOk())
                {
                    AddFeedbackAsyncTask addFeedbackAsyncTask = new AddFeedbackAsyncTask(main, new AddFeedbackRequest(email, content));
                    addFeedbackAsyncTask.execute();
                }
                else
                {
                    Toast.makeText(main, "Vui lòng nhập đầy đủ thông tin!", Toast.LENGTH_LONG).show();
                }

                break;
        }
    }
    public void reset()
    {
        edt_content.setText("");
        edt_email.setText("");
    }

    private boolean checkOk()
    {
        boolean result = true;

        if (edt_content.getText().toString().trim().equals(""))
        {
            result = false;
        }
        if (edt_email.getText().toString().trim().equals(""))
        {
            result = false;
        }
        else if (!com.geotes.utils.Utils.isEmailValid(email)) {
            Toast.makeText(getActivity(), "Email không hợp lệ!", Toast.LENGTH_SHORT).show();
            result=false;
        }



        return result;
    }

    public class AddFeedbackAsyncTask extends AsyncTask<AddFeedbackRequest, Void, AddFeedbackResponse> {
        ProgressDialog dialog;
        AddFeedbackRequest addFeedbackRequest;
        Context ct;


        public AddFeedbackAsyncTask(Context ct, AddFeedbackRequest addFeedbackRequest) {
            this.ct = ct;
            this.addFeedbackRequest = addFeedbackRequest;
        }

        @Override
        protected AddFeedbackResponse doInBackground(AddFeedbackRequest... params) {
            try {
                Thread.sleep(1000);
                UserController controller = new UserController();
                return controller.addFeedBack(addFeedbackRequest);
            } catch (Exception ex) {
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(ct);
            dialog.setMessage("Đang xử lý ...");
            dialog.show();
            dialog.setCancelable(false);
        }

        @Override
        protected void onPostExecute(AddFeedbackResponse code) {
            try {

                if (code != null) {
                    if (code.code == 200) {
                        Toast.makeText(main, code.message, Toast.LENGTH_LONG).show();
                        reset();
                        main.finish();
                    }
                    else {
                        Toast.makeText(main, code.message, Toast.LENGTH_LONG).show();
                    }
                }
            } catch (Exception e) {
            }
            dialog.dismiss();
        }
    }
}
