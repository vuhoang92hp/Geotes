package com.geotes.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.geotes.R;
import com.geotes.activity.IncludeActivity;
import com.geotes.adapter.ListViewPlaylistAdapter;
import com.geotes.common.CRecyclerView;
import com.geotes.common.CommonValue;
import com.geotes.model.PlaylistModel;
import com.github.barteksc.pdfviewer.PDFView;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import static com.geotes.common.CommonValue.CURRENT_FRAGMENT;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_0;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_1;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_2;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_3;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_4;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_5;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_6;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_7;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_8;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_EBOOK_1;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_EBOOK_2;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_EBOOK_3;
import static com.geotes.common.CommonValue.FRAGMENT_DOCUMENT;
import static com.geotes.common.CommonValue.FRAGMENT_DOCUMENT_TYPE;
import static com.geotes.common.CommonValue.FRAGMENT_PLAYLIST;

/**
 * Created by a Hoang on 6/28/2017.
 */

public class FragmentDocumentDetail extends Fragment implements View.OnClickListener{
    private ImageView img_back, img_menu;
    private TextView txt_title,txt_content,txt_description;
    private IncludeActivity main;
    private ScrollView scrollView;
    private PDFView pdfView;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_document_detail, container, false);
        main = (IncludeActivity) getActivity();

        initUI(view);
        initEvent();
        bindData();
        return view;
    }

    private void initUI(View view) {
        img_back = (ImageView) view.findViewById(R.id.img_back);
        img_menu = (ImageView) view.findViewById(R.id.img_menu);
        txt_title = (TextView) view.findViewById(R.id.txt_title);
        txt_content = (TextView) view.findViewById(R.id.txt_content);
        txt_description = (TextView) view.findViewById(R.id.txt_description);
        pdfView = (PDFView) view.findViewById(R.id.pdfView);
        scrollView = (ScrollView) view.findViewById(R.id.scrollView);
    }

    private void initEvent() {
        img_back.setOnClickListener(this);


    }

    private void bindData() {
        img_menu.setVisibility(View.INVISIBLE);
        txt_title.setText(getResources().getString(R.string.document_detai));

        if (FRAGMENT_DOCUMENT_TYPE.equals(FDOCUMENT_TYPE_0))
        {
            txt_description.setText(R.string.do_chieu_dai_may_toan_dac_dien_tu);
            txt_content.setText(R.string.do_chieu_dai_may_toan_dac_dien_tu_content);

        }
        else if (FRAGMENT_DOCUMENT_TYPE.equals(FDOCUMENT_TYPE_1))
        {
            txt_description.setText(R.string.do_goc_toan_may_toan_dac);
            txt_content.setText(R.string.do_goc_toan_may_toan_dac_content);
        }
        else if (FRAGMENT_DOCUMENT_TYPE.equals(FDOCUMENT_TYPE_2))
        {
            txt_description.setText(R.string.su_dung_may_toan_dac_nikon);
            txt_content.setText(R.string.su_dung_may_toan_dac_nikon_content);
        }
        else if (FRAGMENT_DOCUMENT_TYPE.equals(FDOCUMENT_TYPE_3))
        {
            txt_description.setText(R.string.bo_tri_diem_ra_ngoai_thuc_dia_bang_may_toan_dac_nikon);
            txt_content.setText(R.string.bo_tri_diem_ra_ngoai_thuc_dia_bang_may_toan_dac_nikon_content);
        }
        else if (FRAGMENT_DOCUMENT_TYPE.equals(FDOCUMENT_TYPE_4))
        {
            pdfView.setVisibility(View.VISIBLE);
            scrollView.setVisibility(View.GONE);
            pdfView.fromAsset("topcon_gts_105.pdf").load();
        }
        else if (FRAGMENT_DOCUMENT_TYPE.equals(FDOCUMENT_TYPE_5))
        {
            pdfView.setVisibility(View.VISIBLE);
            scrollView.setVisibility(View.GONE);
            pdfView.fromAsset("Do_cao_bang_may_thuy_binh.pdf").load();
        }
        else if (FRAGMENT_DOCUMENT_TYPE.equals(FDOCUMENT_TYPE_6))
        {
            pdfView.setVisibility(View.VISIBLE);
            scrollView.setVisibility(View.GONE);
            pdfView.fromAsset("Do_cao_luong_giac.pdf").load();
        }
        else if (FRAGMENT_DOCUMENT_TYPE.equals(FDOCUMENT_TYPE_7))
        {
            pdfView.setVisibility(View.VISIBLE);
            scrollView.setVisibility(View.GONE);
            pdfView.fromAsset("Huong_dan_do_cao_bang_may_thuy_binh.pdf").load();
        }
        else if (FRAGMENT_DOCUMENT_TYPE.equals(FDOCUMENT_TYPE_8))
        {
            pdfView.setVisibility(View.VISIBLE);
            scrollView.setVisibility(View.GONE);
            pdfView.fromAsset("Nguyen_ly_do_cao_bang_may_thuy_binh.pdf").load();
        }





    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.img_back:
                /*getFragmentManager().popBackStack();
                main.showFrm(main.getFragmentTemp(), main.getFragmentDocument());*/
                main.replaceFrm(main.getFragmentDocument());
                CURRENT_FRAGMENT = FRAGMENT_DOCUMENT;
                break;

        }
    }


}
