package com.geotes.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.geotes.R;
import com.geotes.activity.IncludeActivity;
import com.github.barteksc.pdfviewer.PDFView;

import static com.geotes.common.CommonValue.CURRENT_FRAGMENT;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_0;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_1;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_2;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_3;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_4;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_5;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_6;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_7;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_8;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_EBOOK_1;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_EBOOK_2;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_EBOOK_3;
import static com.geotes.common.CommonValue.FRAGMENT_DOCUMENT_TYPE;
import static com.geotes.common.CommonValue.FRAGMENT_EBOOK;

/**
 * Created by a Hoang on 6/28/2017.
 */

public class FragmentEbookDetail extends Fragment implements View.OnClickListener{
    private ImageView img_back, img_menu;
    private TextView txt_title,txt_content,txt_description;
    private IncludeActivity main;
    private ScrollView scrollView;
    private PDFView pdfView;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_document_detail, container, false);
        main = (IncludeActivity) getActivity();

        initUI(view);
        initEvent();
        bindData();
        return view;
    }

    private void initUI(View view) {
        img_back = (ImageView) view.findViewById(R.id.img_back);
        img_menu = (ImageView) view.findViewById(R.id.img_menu);
        txt_title = (TextView) view.findViewById(R.id.txt_title);
        txt_content = (TextView) view.findViewById(R.id.txt_content);
        txt_description = (TextView) view.findViewById(R.id.txt_description);
        pdfView = (PDFView) view.findViewById(R.id.pdfView);
        scrollView = (ScrollView) view.findViewById(R.id.scrollView);
    }

    private void initEvent() {
        img_menu.setOnClickListener(this);
        img_back.setOnClickListener(this);


    }

    private void bindData() {
        img_menu.setVisibility(View.INVISIBLE);
        txt_title.setText(getResources().getString(R.string.document_detai));

        if (FRAGMENT_DOCUMENT_TYPE.equals(FDOCUMENT_TYPE_EBOOK_1))
        {
            pdfView.setVisibility(View.VISIBLE);
            scrollView.setVisibility(View.GONE);
            pdfView.fromAsset("Ebook_toan_dac_dien_tu.pdf").load();
        }

        else if (FRAGMENT_DOCUMENT_TYPE.equals(FDOCUMENT_TYPE_EBOOK_2))
        {
            pdfView.setVisibility(View.VISIBLE);
            scrollView.setVisibility(View.GONE);
            pdfView.fromAsset("Ebook_huong_dan_do_cao.pdf").load();
        }

        else if (FRAGMENT_DOCUMENT_TYPE.equals(FDOCUMENT_TYPE_EBOOK_3))
        {
            pdfView.setVisibility(View.VISIBLE);
            scrollView.setVisibility(View.GONE);
            pdfView.fromAsset("Ebook_Gps_map.pdf").load();
        }



    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.img_back:
                main.replaceFrm(main.getFragmentEbook());
                CURRENT_FRAGMENT = FRAGMENT_EBOOK;

                break;

        }
    }


}
