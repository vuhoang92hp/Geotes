package com.geotes.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.geotes.R;
import com.geotes.activity.IncludeActivity;
import com.geotes.adapter.ListViewPlaylistAdapter;
import com.geotes.api.APICommon;
import com.geotes.api.ServiceGenerator;
import com.geotes.common.CRecyclerView;
import com.geotes.model.PlaylistModel;
import com.geotes.response.GetPlaylistResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.geotes.common.CommonValue.CURRENT_FRAGMENT;
import static com.geotes.common.CommonValue.FRAGMENT_MENU;

/**
 * Created by a Hoang on 6/28/2017.
 */

public class FragmentPlaylist extends Fragment implements View.OnClickListener{
    private ImageView img_back, img_menu;
    private TextView txt_title;
    private IncludeActivity main;
    private RelativeLayout lay_title;
    private CRecyclerView list_play_list;
    private ArrayList<PlaylistModel> arr_play_list;
    private ListViewPlaylistAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_playlist, container, false);
        main = (IncludeActivity) getActivity();

        initUI(view);
        initEvent();
        bindData();
        return view;
    }

    private void initUI(View view) {
        list_play_list = (CRecyclerView) view.findViewById(R.id.list_play_list);
        img_back = (ImageView) view.findViewById(R.id.img_back);
        img_menu = (ImageView) view.findViewById(R.id.img_menu);
        txt_title = (TextView) view.findViewById(R.id.txt_title);
        lay_title = (RelativeLayout) view.findViewById(R.id.lay_title);
    }

    private void initEvent() {

        img_back.setOnClickListener(this);


    }

    private void bindData() {
        img_menu.setVisibility(View.INVISIBLE);
        txt_title.setText(getResources().getString(R.string.playlist));
        lay_title.setBackgroundResource(R.color.colorRed);
        arr_play_list = new ArrayList<>();
        adapter = new ListViewPlaylistAdapter(getActivity(), arr_play_list);
        list_play_list.setAdapter(adapter);
        getPlaylistData();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.img_back:
                main.finish();
                /*
                getFragmentManager().popBackStack();
                main.hideFrm(main.getFragmentPlaylist());
                main.showFrm(main.getFragmentTemp(),main.getFragmentMenu());
*/
                CURRENT_FRAGMENT = FRAGMENT_MENU;
                break;

        }
    }

    private void getPlaylistData() {
        APICommon.Geotes service = ServiceGenerator.GetInstance();
        Call<GetPlaylistResponse> getPlaylist;
        getPlaylist = service.getPlaylist();
        getPlaylist.enqueue(new Callback<GetPlaylistResponse>() {
            @Override
            public void onResponse(Call<GetPlaylistResponse> call, Response<GetPlaylistResponse> response) {
                try {
                    if (response.body() != null && response.body().code == 200) {
                        if (response.body().result != null && response.body().result.size() > 0) {
                            arr_play_list.addAll(response.body().result);
                            adapter.notifyDataSetChanged();
                        } else {

                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<GetPlaylistResponse> call, Throwable t) {
                Toast.makeText(getActivity(), "Không có kết nối!", Toast.LENGTH_SHORT).show();
            }


        });
    }
}
