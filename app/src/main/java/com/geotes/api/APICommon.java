package com.geotes.api;

import android.util.Log;


import com.geotes.common.FunctionCommon;
import com.geotes.model.VideoModel;
import com.geotes.request.ActiveAccountRequest;
import com.geotes.request.AddCommentRequest;
import com.geotes.request.AddFavouriteRequest;
import com.geotes.request.AddFeedbackRequest;
import com.geotes.request.AddFollowRequest;
import com.geotes.request.AddShareRequest;
import com.geotes.request.AddStatusRequest;
import com.geotes.request.CourseRegisterRequest;
import com.geotes.request.DeletePostRequest;
import com.geotes.request.LoginFaceRequest;
import com.geotes.request.LoginRequest;
import com.geotes.request.LogoutRequest;
import com.geotes.request.ReadNotificationRequest;
import com.geotes.request.RegisterRequest;
import com.geotes.request.UpdateProfileRequest;
import com.geotes.response.ActiveAccountResponse;
import com.geotes.response.AddCommentResponse;
import com.geotes.response.AddFavouriteResponse;
import com.geotes.response.AddFeedbackResponse;
import com.geotes.response.AddFollowResponse;
import com.geotes.response.AddShareResponse;
import com.geotes.response.AddStatusResponse;
import com.geotes.response.CourseRegisterResponse;
import com.geotes.response.DeletePostResponse;
import com.geotes.response.GetAllCourseResponse;
import com.geotes.response.GetBannerResponse;
import com.geotes.response.GetCourseDetailResponse;
import com.geotes.response.GetFollowResponse;
import com.geotes.response.GetFriendProfileResponse;
import com.geotes.response.GetNewFeedResponse;
import com.geotes.response.GetNotificationResponse;
import com.geotes.response.GetPlaylistResponse;
import com.geotes.response.GetVersionCodeResponse;
import com.geotes.response.GetVideoDetailResponse;
import com.geotes.response.GetVideoResponse;
import com.geotes.response.LoginResponse;
import com.geotes.response.LogoutResponse;
import com.geotes.response.ReadNotificationResponse;
import com.geotes.response.RegisterResponse;
import com.geotes.response.StatusDetailsResponse;
import com.geotes.response.UpdateProfileResponse;
import com.geotes.response.UploadImageResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public class APICommon {
    public static final String BASE_URL = "http://103.1.209.87/geotes/public/";
    public static final int ANDROID = 1;
    public static final int IOS = 2;
    private static final String TAG = APICommon.class.getSimpleName();


    public static int getDeviceType() {
        return ANDROID;
    }



    public interface Geotes {
       /* @POST("users/login")
        Call<LoginResponse> loginRegister(@Body LoginRequest request);


        @GET("order/getProducts")
        Call<GetProductItemsResponse> getProductItems(@Query("product_type") int product_type,
                                                      @Query("page_size") int page_size,
                                                      @Query("page_number") int page_number);*/
       @GET("api/data/getAllYoutubeGuide")
       Call<GetPlaylistResponse> getPlaylist();

        @GET("api/data/getYoutubeList")
        Call<GetVideoResponse> getVideoList(@Query("youtube_guide_id") int youtube_guide_id);

        @GET("api/data/getYoutubeDetail")
        Call<GetVideoDetailResponse> getVideoDetail(@Query("youtube_list_id") int youtube_list_id);

        @GET("api/data/getAllCourse")
        Call<GetAllCourseResponse> getAllCourse();

        @GET("api/data/getCourseDetail")
        Call<GetCourseDetailResponse> getCourseDetail(@Query("course_id") int course_id);

        @GET("api/data/getBanner")
        Call<GetBannerResponse> getBanner();

        @GET("api/data/getNewfeed")
        Call<GetNewFeedResponse> getNewFeed(@Query("UserID") int UserID,
                                            @Query("is_like") int is_like,
                                            @Query("text_search") String text_search,
                                            @Query("page_size") int page_size,
                                            @Query("page_number") int page_number,
                                            @Query("newfeed_type") int newfeed_type);
        @GET("api/data/getNotification")
        Call<GetNotificationResponse> getNotification(@Query("UserID") int UserID,
                                                 @Query("ApiToken") String ApiToken,
                                                 @Query("page_size") int page_size,
                                                 @Query("page_number") int page_number);
        @GET("api/data/getNewfeedDetails")
        Call<StatusDetailsResponse> getNewFeedDetail(@Query("UserID") int UserID,
                                                     @Query("new_feed_id") int new_feed_id);

        @GET("api/users/getUserProfile")
        Call<GetFriendProfileResponse> getFriendProfile(@Query("UserID") int UserID,
                                                        @Query("user_view_id") int user_view_id
                                                        );
        @GET("api/users/getVersion")
        Call<GetVersionCodeResponse> getVersionCode();

        @GET("api/data/getUserFavorite")
        Call<GetFollowResponse> getListFollow(@Query("UserID") int UserID,
                                                 @Query("user_favorite_id") int user_favorite_id);

        @POST("api/data/addFeedback")
        Call<AddFeedbackResponse> addFeedback(@Body AddFeedbackRequest request);

        @POST("api/data/registerCourse")
        Call<CourseRegisterResponse> courseRegister(@Body CourseRegisterRequest request);

        @POST("api/users/register")
        Call<RegisterResponse> registerAccount(@Body RegisterRequest request);

        @POST("api/users/activeAccount")
        Call<ActiveAccountResponse> activeAccount(@Body ActiveAccountRequest request);

        @POST("api/users/login")
        Call<LoginResponse> login(@Body LoginRequest request);

        @POST("api/users/loginAndRegisterFacebook")
        Call<LoginResponse> loginFacebook(@Body LoginFaceRequest request);

        @POST("api/users/updateProfile")
        Call<UpdateProfileResponse> updateProfile(@Body UpdateProfileRequest request);

        @POST("api/data/addFavorite")
        Call<AddFavouriteResponse> addFavourite(@Body AddFavouriteRequest request);

        @POST("api/data/shareNewfeed")
        Call<AddShareResponse> addShare(@Body AddShareRequest request);

        @POST("api/data/addComment")
        Call<AddCommentResponse> addComment(@Body AddCommentRequest request);

        @POST("api/data/createNewfeed")
        Call<AddStatusResponse> addStatus(@Body AddStatusRequest request);

        @POST("api/data/readNotification")
        Call<ReadNotificationResponse> readNotification(@Body ReadNotificationRequest request);

        @POST("api/data/addUserFavorite")
        Call<AddFollowResponse> addFollow(@Body AddFollowRequest request);

        @POST("api/data/deleteNewfeed")
        Call<DeletePostResponse> deletePost(@Body DeletePostRequest request);

     @POST("api/users/logout")
     Call<LogoutResponse> logOut(@Body LogoutRequest request);

        @Multipart
        @POST("api/users/uploadFile")
        Call<UploadImageResponse> uploadImage(
                @Part MultipartBody.Part FileNo1,
                @Part MultipartBody.Part FileNo2,
                @Part MultipartBody.Part FileNo3,
                @Part MultipartBody.Part FileNo4,
                @Part MultipartBody.Part FileNo5,
                @Part("UserID") RequestBody UserID,
                @Part("ApiToken") RequestBody ApiToken
        );
    }

}
