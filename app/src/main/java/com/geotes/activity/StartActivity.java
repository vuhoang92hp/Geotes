package com.geotes.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.widget.FrameLayout;

import com.geotes.BuildConfig;
import com.geotes.R;
import com.geotes.common.Config;
import com.geotes.fragment.fragment_login.ForgetPassFragment;
import com.geotes.fragment.fragment_login.LoginFragment;
import com.geotes.fragment.fragment_login.RegisterFragment;
import com.geotes.fragment.fragment_login.SplashFragment;
import com.geotes.model.UserModel;
import com.google.gson.Gson;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import static com.geotes.common.CommonValue.KEY_LOGOUT;


public class StartActivity extends AppCompatActivity {
    public Fragment fragmentTemp = null;
    private SplashFragment splashFragment = new SplashFragment();
    private LoginFragment loginFragment = new LoginFragment();
    private ForgetPassFragment forgetPassFragment = new ForgetPassFragment();
    private RegisterFragment registerFragment = new RegisterFragment();
    private UserModel userModel;
    ProgressDialog dialog_facebook;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        initUI();
        initFragment();
        initEvent();
        bindData();

    }
    private void initUI() {

    }

    private void initEvent() {
    }

    private void bindData() {

        SharedPreferences sharedPreferences = getSharedPreferences(Config.Pref, MODE_PRIVATE);
        String profile = sharedPreferences.getString(Config.KEY_USER, "");
        Gson gson = new Gson();
         userModel = gson.fromJson(profile, UserModel.class);

         if (KEY_LOGOUT.equals("logout")||KEY_LOGOUT.equals("re_login"))
         {
             showFragment(fragmentTemp, loginFragment);
         }
         else
         {
             final Handler handler = new Handler();
             handler.postDelayed(new Runnable() {
                 @Override
                 public void run() {
                     // Do something after 3s = 3000ms

                     if (userModel!= null)
                     {
                         Intent i = new Intent(StartActivity.this, MainActivity.class);
                         startActivity(i);
                         finish();
                     }
                     else
                     {
                         showFragment(fragmentTemp, loginFragment);
                     }

                     if (Build.VERSION.SDK_INT < 23) {
                         //Do not need to check the permission
                     }else {
                         if (checkAndRequestPermissions()){
                             //If you have already permitted the permission
                         }
                     }

                 }
             }, 2000);
         }



    }
    private boolean checkAndRequestPermissions() {
        int permissionCAMERA = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int storagePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
        int callPhonePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE);
        int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int writePermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int groupStorePermission = ContextCompat.checkSelfPermission(this, Manifest.permission_group.STORAGE);


        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionCAMERA != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (callPhonePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CALL_PHONE);
        }
        if (writePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (storagePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (groupStorePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission_group.STORAGE);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this,
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), 1);
            return false;
        }

        return true;
    }

    private void initFragment() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.frame_start, splashFragment);
        transaction.add(R.id.frame_start, loginFragment);
        transaction.add(R.id.frame_start, forgetPassFragment);
        transaction.add(R.id.frame_start, registerFragment);

        transaction.hide(loginFragment);
        transaction.hide(forgetPassFragment);
        transaction.hide(registerFragment);

        fragmentTemp = splashFragment;
        transaction.commitAllowingStateLoss();
    }
    public void showFragment(Fragment hide, Fragment show){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.hide(hide);
        transaction.show(show);
        fragmentTemp = show;
        transaction.commit();
    }

    public void addFragment(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.frame_start, fragment);
        transaction.addToBackStack("");
        transaction.commit();
    }

    public void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.frame_start, fragment);
        transaction.addToBackStack(backStateName);
        transaction.commit();
    }

    public Fragment getFragmentTemp() {
        return fragmentTemp;
    }

    public void setFragmentTemp(Fragment fragmentTemp) {
        this.fragmentTemp = fragmentTemp;
    }

    public SplashFragment getSplashFragment() {
        return splashFragment;
    }

    public void setSplashFragment(SplashFragment splashFragment) {
        this.splashFragment = splashFragment;
    }

    public LoginFragment getLoginFragment() {
        return loginFragment;
    }

    public void setLoginFragment(LoginFragment loginFragment) {
        this.loginFragment = loginFragment;
    }

    public ForgetPassFragment getForgetPassFragment() {
        return forgetPassFragment;
    }

    public void setForgetPassFragment(ForgetPassFragment forgetPassFragment) {
        this.forgetPassFragment = forgetPassFragment;
    }

    public RegisterFragment getRegisterFragment() {
        return registerFragment;
    }

    public void setRegisterFragment(RegisterFragment registerFragment) {
        this.registerFragment = registerFragment;
    }
}
