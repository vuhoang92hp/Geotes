package com.geotes.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.geotes.R;
import com.geotes.adapter.CustomFragmentPagerAdapter;
import com.geotes.api.APICommon;
import com.geotes.api.ServiceGenerator;
import com.geotes.common.Config;
import com.geotes.common.MaterialTabs;
import com.geotes.common.PublicMethob;
import com.geotes.controller.UserController;
import com.geotes.fragment.fragment_friend.FragmentFriendFavourite;
import com.geotes.fragment.fragment_friend.FragmentFriendInfo;
import com.geotes.fragment.fragment_friend.FragmentFriendNewFeed;

import com.geotes.model.FriendProfileModel;
import com.geotes.model.UserModel;
import com.geotes.request.AddFollowRequest;
import com.geotes.response.AddFollowResponse;
import com.geotes.response.GetFriendProfileResponse;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.geotes.common.CommonValue.CURRENT_FRAGMENT;
import static com.geotes.common.CommonValue.FRAGMENT_PROFILE_INFO;
import static com.geotes.common.CommonValue.FRIEND_PROFILE_ID;
import static com.geotes.common.CommonValue.INCLUDE_FRAGMENT;
import static com.geotes.common.CommonValue.KEY_LOGOUT;

public class FriendProfileActivity extends AppCompatActivity implements View.OnClickListener{
    private ImageView ivAvatar;

    private ImageView img_back;
    private TextView tvNameUser,txt_follow;
    private LinearLayout lay_follow;

    private UserModel userModel;
    private MaterialTabs tabs;
    private ViewPager viewPager;
    private LinearLayout llNewFeed, llClass, llRate;
    private TextView tvNewFeed, tvClass, tvRate;
    private CustomFragmentPagerAdapter adapter;
    private SharedPreferences sharedPreferences;
    public FriendProfileModel friendProfileModel;
    private SharedPreferences.Editor editor;

    private Gson gson;
    private int is_follow;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend_profile);

        sharedPreferences = getSharedPreferences(Config.Pref, MODE_PRIVATE);
        String profile = sharedPreferences.getString(Config.KEY_USER, "");
        gson = new Gson();
        userModel = gson.fromJson(profile, UserModel.class);
        initUI();
        bindData();
        initEven();
    }

    private void initEven() {
        lay_follow.setOnClickListener(this);
        img_back.setOnClickListener(this);
        llNewFeed.setOnClickListener(this);
        llClass.setOnClickListener(this);
        llRate.setOnClickListener(this);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                setTabs(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void bindData() {
        sharedPreferences = getSharedPreferences(Config.Pref, MODE_PRIVATE);
        editor = sharedPreferences.edit();

        if (userModel!=null)
        {
            getFriendProfile(FRIEND_PROFILE_ID, userModel.id);
        }
        else
        {
            getFriendProfile(FRIEND_PROFILE_ID, 0);
        }



        tvRate.setText(getResources().getString(R.string.info));


    }

    private void initUI() {
        lay_follow = (LinearLayout) findViewById(R.id.lay_follow);
        txt_follow = (TextView) findViewById(R.id.txt_follow);
        ivAvatar = (ImageView) findViewById(R.id.iv_avatar);
        img_back = (ImageView) findViewById(R.id.img_back);
        tvNameUser = (TextView) findViewById(R.id.tv_name_user);
        tabs = (MaterialTabs) findViewById(R.id.tabHost);
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        llNewFeed = (LinearLayout) findViewById(R.id.ll_new_feed);
        llClass = (LinearLayout) findViewById(R.id.ll_class);
        llRate = (LinearLayout) findViewById(R.id.ll_rate);
        tvNewFeed = (TextView) findViewById(R.id.tv_new_feed);
        tvClass = (TextView) findViewById(R.id.tv_class);
        tvRate = (TextView) findViewById(R.id.tv_rate);



    }

    private void setTabs(int isTabs) {
        llNewFeed.setBackgroundResource(R.color.colorTransparent);
        llClass.setBackgroundResource(R.color.colorTransparent);
        llRate.setBackgroundResource(R.color.colorTransparent);
        tvRate.setTextColor(getResources().getColor(R.color.colorTextGrey));
        tvClass.setTextColor(getResources().getColor(R.color.colorTextGrey));
        tvNewFeed.setTextColor(getResources().getColor(R.color.colorTextGrey));

        switch (isTabs) {
            case 0:
                llNewFeed.setBackgroundResource(R.drawable.border_button_green);
                tvNewFeed.setTextColor(getResources().getColor(R.color.colorWhite));

                break;
            case 1:
                llClass.setBackgroundResource(R.drawable.border_button_green);
                tvClass.setTextColor(getResources().getColor(R.color.colorWhite));

                break;
            case 2:
                llRate.setBackgroundResource(R.drawable.border_button_green);
                tvRate.setTextColor(getResources().getColor(R.color.colorWhite));

                break;
            default:
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ll_new_feed:
                setTabs(0);
                viewPager.setCurrentItem(0);
                break;
            case R.id.ll_class:
                setTabs(1);
                viewPager.setCurrentItem(1);
                break;
            case R.id.ll_rate:
                setTabs(2);
                viewPager.setCurrentItem(2);
                break;
            case R.id.img_back:
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_OK, returnIntent);
                finish();

                break;
            case R.id.img_search:
                INCLUDE_FRAGMENT =  FRAGMENT_PROFILE_INFO;;
                CURRENT_FRAGMENT = FRAGMENT_PROFILE_INFO;
                Intent i = new Intent(FriendProfileActivity.this, IncludeActivity.class);
                startActivity(i);
                break;
            case R.id.lay_follow:
                if (userModel!=null)
                {
                    if (is_follow == 0)
                    {
                        txt_follow.setText(getResources().getString(R.string.c_un_follow));
                        is_follow = 1;
                    }
                    else
                    {
                        txt_follow.setText(getResources().getString(R.string.c_follow));
                        is_follow = 0;
                    }

                    if (userModel.id != friendProfileModel.id)
                    {
                        AddFollowAsyncTask addFollowAsyncTask = new AddFollowAsyncTask(this, new AddFollowRequest(userModel.id, userModel.ApiToken, friendProfileModel.id));
                        addFollowAsyncTask.execute();
                    }
                    else
                    {
                        Toast.makeText(FriendProfileActivity.this, "Bạn không thể tự theo dõi mình!", Toast.LENGTH_SHORT).show();
                    }

                }
                else
                {
                    KEY_LOGOUT = "re_login";
                    Intent i9 = new Intent(FriendProfileActivity.this, StartActivity.class);
                    startActivity(i9);
                    finish();
                }

               //
                break;
            default:
        }
    }

    private void getFriendProfile(int id_friend, int id_mine) {
        APICommon.Geotes service = ServiceGenerator.GetInstance();
        Call<GetFriendProfileResponse> getFriendProfileResponseCall;
        getFriendProfileResponseCall = service.getFriendProfile(id_friend, id_mine);
        getFriendProfileResponseCall.enqueue(new Callback<GetFriendProfileResponse>() {
            @Override
            public void onResponse(Call<GetFriendProfileResponse> call, Response<GetFriendProfileResponse> response) {
                try {
                    if (response.body().result != null && response.code() == 200) {
                        friendProfileModel = response.body().result;
                        Gson gson = new Gson();
                        String profile = gson.toJson(friendProfileModel);
                        editor.putString(Config.KEY_FRIEND, profile).commit();

                        setData(friendProfileModel);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<GetFriendProfileResponse> call, Throwable t) {
                Toast.makeText(FriendProfileActivity.this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
            }


        });
    }
    public void setData(FriendProfileModel model)
    {
        tvNameUser.setText(model.name);
        PublicMethob.showImageAvarta(this,model.photo,ivAvatar);

        adapter = new CustomFragmentPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new FragmentFriendNewFeed(), getResources().getString(R.string.new_feed));
        adapter.addFragment(new FragmentFriendFavourite(), getResources().getString(R.string.favourite));
        adapter.addFragment(new FragmentFriendInfo(), getResources().getString(R.string.info));
        viewPager.setAdapter(adapter);
        tabs.setViewPager(viewPager);
        viewPager.setOffscreenPageLimit(3);
        setTabs(0);

        if (model.is_like == 0)
        {
            txt_follow.setText(getResources().getString(R.string.c_follow));
            is_follow = 0;
        }
        else
        {
            txt_follow.setText(getResources().getString(R.string.c_un_follow));
            is_follow = 1;
        }
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
    public class AddFollowAsyncTask extends AsyncTask<AddFollowRequest, Void, AddFollowResponse> {

        AddFollowRequest addFollowRequest;
        Context ct;


        public AddFollowAsyncTask(Context ct, AddFollowRequest addFollowRequest) {
            this.ct = ct;
            this.addFollowRequest = addFollowRequest;
        }

        @Override
        protected AddFollowResponse doInBackground(AddFollowRequest... params) {
            try {
                Thread.sleep(0);
                UserController controller = new UserController();
                return controller.addFollow(addFollowRequest);
            } catch (Exception ex) {
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(AddFollowResponse code) {
            try {

                if (code != null) {
                    if (code.code == 200)
                    {
                        // Toast.makeText(mContext, code.message,Toast.LENGTH_SHORT).show();
                    }
                }
            } catch (Exception e) {

            }

        }
    }

}
