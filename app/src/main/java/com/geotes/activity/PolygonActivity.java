package com.geotes.activity;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Geocoder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.geotes.R;
import com.geotes.common.GPSTracker;
import com.geotes.common.PublicMethob;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.SphericalUtil;

import java.text.DecimalFormat;
import java.util.ArrayList;


public class PolygonActivity extends FragmentActivity implements OnMapReadyCallback,View.OnClickListener {

    private GoogleMap mMap;
    private Geocoder geocoder;
    private LatLng currentlocation;
    private GPSTracker gpsTracker;
    private TextView txt_distance,txt_lat,txt_log,txt_area_title,txt_area;
    private ArrayList<Marker> arr_marker = new ArrayList<>();
    private ArrayList<LatLng> arr_latlng = new ArrayList<>();
    private ArrayList<Polyline> arr_polyline = new ArrayList<>();
    private ImageView img_reset, img_undo;
    private ArrayList<Double> distance_step = new ArrayList<>();
    private double total_distance = 0;
    private PolylineOptions rectOptions;
    private Polygon polygon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        gpsTracker = new GPSTracker(PolygonActivity.this);
        setContentView(R.layout.activity_distance_calculate);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        initUI();
        initEvent();
        bindData();
    }

    private void initUI() {
        txt_distance = (TextView) findViewById(R.id.txt_distance);
        img_undo = (ImageView) findViewById(R.id.img_undo);
        img_reset = (ImageView) findViewById(R.id.img_reset);
        txt_area_title = (TextView) findViewById(R.id.txt_area_title);
        txt_area = (TextView) findViewById(R.id.txt_area);
    }

    private void initEvent() {
        img_reset.setOnClickListener(this);
        img_undo.setOnClickListener(this);
    }

    private void bindData() {
        txt_area.setVisibility(View.VISIBLE);
        txt_area_title.setVisibility(View.VISIBLE);
    }



    @Override
    public void onMapReady(GoogleMap googleMap) {

        //init map
        mMap = googleMap;
        geocoder = new Geocoder(this);
        currentlocation = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentlocation, 15));
        mMap.setMyLocationEnabled(true);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        CameraPosition cameraPosition = new CameraPosition(currentlocation, 15, 0, 0);
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        //change position button current location
        View locationButton = ((View) findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
        // change position of current location button
        rlp.setMargins(0, 200, 0, 0);

        // end init map

        //addLines();

        //action click on map
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {


                return false;
            }
        });

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener()
        {
            @Override
            public void onMapClick(LatLng arg0)
            {
                addMarker(arg0);
            }
        });
    }
    private void addLines(Marker from, Marker to) {
        LatLng position_from = from.getPosition();
        LatLng position_to = to.getPosition();

        Polyline polyline = mMap.addPolyline((new PolylineOptions())
                .add(position_from, position_to).width(5).color(Color.BLUE)
                .geodesic(true));
        arr_polyline.add(polyline);

        //calculate distance


    }

    public void addPolygon()
    {/*
        Polygon UCCpolygon = mMap.addPolygon(new PolygonOptions()
                .add(new LatLng(51.893728, -8.491865),
                        new LatLng(51.893550, -8.492479),
                        new LatLng(51.893216, -8.492224),
                        new LatLng(51.893404, -8.491598))
                .fillColor(Color.BLUE));*/
       /* CameraPosition cameraPosition = new CameraPosition(arr_latlng.get(0), 15, 0, 0);
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));*/
        if (polygon!=null)
        {
            polygon.remove();
        }
       if (arr_latlng.size()>0)
       {
           PolygonOptions polygonOptions = new PolygonOptions();

           for (int i = 0; i < arr_latlng.size();i++)
           {
               polygonOptions.add(arr_latlng.get(i));
           }
            polygon = mMap.addPolygon(polygonOptions
                   .fillColor(getResources().getColor(R.color.colorBlueTrans30)).strokeColor(Color.BLUE));
       }
        txt_area.setText(PublicMethob.roundNumber2(SphericalUtil.computeArea(arr_latlng)) + " m2");
        //Toast.makeText(PolygonActivity.this, SphericalUtil.computeArea(arr_latlng) +"", Toast.LENGTH_SHORT).show();

    }

    private void calculateDistance(Marker from, Marker to) {
        LatLng position_from = from.getPosition();
        LatLng position_to = to.getPosition();

        double distance = CalculationByDistance(position_from, position_to);
        distance_step.add(distance);
        setDistanceText();
    }
    private void setDistanceText()
    {
        total_distance = 0;
        for (int i = 0; i < distance_step.size(); i++)
        {
            total_distance = total_distance + distance_step.get(i);
        }
        txt_distance.setText(PublicMethob.roundNumber2(total_distance*1000)+" m");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.img_undo:
                if (arr_marker.size()>0)
                {
                    arr_marker.get(arr_marker.size()-1).remove();
                    arr_marker.remove(arr_marker.size()-1);
                }
                if (arr_polyline.size()>0)
                {
                    arr_polyline.get(arr_polyline.size()-1).remove();
                    arr_polyline.remove(arr_polyline.size()-1);
                }
                if (arr_latlng.size()>0)
                {
                    //arr_latlng.get(arr_latlng.size()-1).;
                    arr_latlng.remove(arr_latlng.size()-1);
                    addPolygon();
                }

                if (distance_step.size()>0)
                {
                    distance_step.remove(distance_step.size()-1);
                    setDistanceText();
                }
                break;

            case R.id.img_reset:
                if (arr_marker.size()>0)
                {
                    for (int i = 0; i < arr_marker.size();i++)
                    {
                        arr_marker.get(i).remove();

                    }

                    arr_marker.clear();


                }
                if (arr_polyline.size()>0)
                {
                    for (int i = 0; i < arr_polyline.size();i++)
                    {
                        arr_polyline.get(i).remove();

                    }
                    arr_polyline.clear();
                }

                if (arr_latlng.size()>0)
                {

                    arr_latlng.clear();
                    if (polygon!=null)
                    {
                        polygon.remove();
                    }
                }

                distance_step.clear();
                txt_area.setText("0.0 m2");
                setDistanceText();
                break;
        }
    }
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public void addMarker(LatLng latLng)
    {
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng).snippet("Lat: "+latLng.latitude+"\nLog: "+latLng.longitude).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_location_red));
        Marker marker = mMap.addMarker(markerOptions);
        arr_marker.add(marker);
        arr_latlng.add(latLng);

        if (arr_marker.size()>1)
        {
            addLines(arr_marker.get(arr_marker.size()-2), arr_marker.get(arr_marker.size()-1));
            calculateDistance(arr_marker.get(arr_marker.size()-2), arr_marker.get(arr_marker.size()-1));
        }

        addPolygon();


        //markerOptions.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_location_red));
        //Marker marker = mMap.addMarker(markerOptions);

        /*Marker marker = mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(latLng))));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 17));*/
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {

                LinearLayout info = new LinearLayout(PolygonActivity.this);
                info.setOrientation(LinearLayout.VERTICAL);

                TextView title = new TextView(PolygonActivity.this);
                title.setTextColor(Color.BLACK);
                title.setGravity(Gravity.CENTER);
                title.setTypeface(null, Typeface.BOLD);
                title.setText(marker.getTitle());

                TextView snippet = new TextView(PolygonActivity.this);
                snippet.setTextColor(Color.GRAY);
                snippet.setText(marker.getSnippet());


                info.addView(snippet);

                return info;
            }
        });
    }
    private Bitmap getMarkerBitmapFromView(LatLng latLng) {

        View customMarkerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.custom_marker, null);
        //ImageView markerImageView = (ImageView) customMarkerView.findViewById(R.id.profile_image);
        //markerImageView.setImageResource(resId);

        txt_lat = (TextView) customMarkerView.findViewById(R.id.txt_lat);
        txt_log = (TextView) customMarkerView.findViewById(R.id.txt_log);

        txt_lat.setText(latLng.latitude+"");
        txt_log.setText(latLng.longitude+"");

        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
        customMarkerView.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = customMarkerView.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        customMarkerView.draw(canvas);
        return returnedBitmap;
    }
    public double CalculationByDistance(LatLng StartP, LatLng EndP) {
        int Radius=6371;//radius of earth in Km
        double lat1 = StartP.latitude;
        double lat2 = EndP.latitude;
        double lon1 = StartP.longitude;
        double lon2 = EndP.longitude;
        double dLat = Math.toRadians(lat2-lat1);
        double dLon = Math.toRadians(lon2-lon1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLon/2) * Math.sin(dLon/2);
        double c = 2 * Math.asin(Math.sqrt(a));
        double valueResult= Radius*c;
        double km=valueResult/1;
        DecimalFormat newFormat = new DecimalFormat("####");
        int kmInDec =  Integer.valueOf(newFormat.format(km));
        double meter=valueResult%1000;
        int  meterInDec= Integer.valueOf(newFormat.format(meter));
        Log.i("Radius Value",""+valueResult+"   KM  "+kmInDec+" Meter   "+meterInDec);

        return Radius * c;
    }
}
