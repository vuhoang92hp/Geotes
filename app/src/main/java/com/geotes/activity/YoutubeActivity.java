package com.geotes.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.geotes.R;
import com.geotes.api.APICommon;
import com.geotes.api.ServiceGenerator;
import com.geotes.fragment.fragment_login.ForgetPassFragment;
import com.geotes.fragment.fragment_login.LoginFragment;
import com.geotes.fragment.fragment_login.RegisterFragment;
import com.geotes.fragment.fragment_login.SplashFragment;
import com.geotes.model.VideoDetailModel;
import com.geotes.response.GetVideoDetailResponse;
import com.geotes.response.GetVideoResponse;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.ErrorReason;
import com.google.android.youtube.player.YouTubePlayer.PlaybackEventListener;
import com.google.android.youtube.player.YouTubePlayer.PlayerStateChangeListener;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.geotes.common.CommonValue.FRAGMENT_MENU;
import static com.geotes.common.CommonValue.INCLUDE_FRAGMENT;
import static com.geotes.common.CommonValue.PLAYLIST_ID;
import static com.geotes.common.CommonValue.VIDEO_DETAIL_ID;
import static com.geotes.common.CommonValue.VIDEO_LINK;


public class YoutubeActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener, View.OnClickListener {
    private YouTubePlayerView youtube_player;
    public static final String API_KEY = "AIzaSyDSw3A4FTecUqVsnV5U_gaalVchUiZkW2o";
    String VIDEO_ID = "";
    public RelativeLayout lay_title;
    private TextView txt_title_video, txt_content,txt_title;
    private ImageView img_back, img_menu;
    private VideoDetailModel videoDetail;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_watching);
        initUI();
        initEvent();
        bindData();

    }
    private void initUI() {
        youtube_player = (YouTubePlayerView) findViewById(R.id.youtube_player);
        lay_title = (RelativeLayout) findViewById(R.id.lay_title);
        txt_title_video = (TextView) findViewById(R.id.txt_title_video);
        txt_content = (TextView) findViewById(R.id.txt_content);
        txt_title = (TextView) findViewById(R.id.txt_title);
        img_back = (ImageView) findViewById(R.id.img_back);
        img_menu = (ImageView) findViewById(R.id.img_menu);
    }

    private void initEvent() {
        img_back.setOnClickListener(this);
        img_menu.setOnClickListener(this);
    }

    private void bindData() {
        img_menu.setVisibility(View.GONE);
        txt_title.setVisibility(View.INVISIBLE);
        youtube_player.initialize(API_KEY, this);
        lay_title.setBackgroundResource(R.color.colorRed);
        //VIDEO_ID = "AZptuc7go8I";
        VIDEO_ID = VIDEO_LINK;

        getVideoList();

    }


    @Override
    public void onInitializationSuccess(Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        if(null== youTubePlayer) return;

        // Start buffering
        if (!b) {
            youTubePlayer.cueVideo(VIDEO_ID);
        }

    }

    @Override
    public void onInitializationFailure(Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Toast.makeText(this, "Failed to initialize.", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.img_back:
            finish();
            break;
            case R.id.img_menu:
            finish();
                INCLUDE_FRAGMENT = FRAGMENT_MENU;
                Intent i = new Intent(YoutubeActivity.this, IncludeActivity.class);
                startActivity(i);
        }
    }

    private void getVideoList() {
        APICommon.Geotes service = ServiceGenerator.GetInstance();
        Call<GetVideoDetailResponse> getVideoDetail;
        getVideoDetail = service.getVideoDetail(VIDEO_DETAIL_ID);
        getVideoDetail.enqueue(new Callback<GetVideoDetailResponse>() {
            @Override
            public void onResponse(Call<GetVideoDetailResponse> call, Response<GetVideoDetailResponse> response) {
                try {
                    if (response.body() != null && response.body().code == 200) {
                        if (response.body().result != null ) {
                            videoDetail = response.body().result;
                           // VIDEO_ID = videoDetail.video_link.substring(videoDetail.video_link.lastIndexOf("/"));
                            txt_content.setText(videoDetail.description);
                            txt_title_video.setText(videoDetail.title);

                        } else {

                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<GetVideoDetailResponse> call, Throwable t) {
                Toast.makeText(YoutubeActivity.this, "Không có kết nối!", Toast.LENGTH_SHORT).show();
            }


        });
    }
}
