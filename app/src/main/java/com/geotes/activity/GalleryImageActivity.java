package com.geotes.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;


import com.geotes.R;
import com.geotes.adapter.CustomPagerAdapter;

import java.util.List;


/**
 * Created by Administrator on 05/07/2016.
 */
public class GalleryImageActivity extends Activity {
    public static List<String> imagePhoto;
    public static int position;
    public View back;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.slide_show);
        back=findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        CustomPagerAdapter mCustomPagerAdapter = new CustomPagerAdapter(this, imagePhoto);
        ViewPager mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mCustomPagerAdapter);
        mViewPager.setCurrentItem(position);
    }

}