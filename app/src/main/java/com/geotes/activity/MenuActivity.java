package com.geotes.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.geotes.R;
import com.geotes.common.Config;
import com.geotes.common.PublicMethob;
import com.geotes.controller.UserController;
import com.geotes.model.UserModel;
import com.geotes.request.DeletePostRequest;
import com.geotes.request.LogoutRequest;
import com.geotes.response.DeletePostResponse;
import com.geotes.response.GetFriendProfileResponse;
import com.geotes.response.LogoutResponse;
import com.google.gson.Gson;

import retrofit2.Call;

import static com.geotes.common.CommonValue.CURRENT_FRAGMENT;
import static com.geotes.common.CommonValue.FRAGMENT_CONTACT;
import static com.geotes.common.CommonValue.FRAGMENT_DOCUMENT;
import static com.geotes.common.CommonValue.FRAGMENT_EBOOK;
import static com.geotes.common.CommonValue.FRAGMENT_FEEDBACK;
import static com.geotes.common.CommonValue.FRAGMENT_PLAYLIST;
import static com.geotes.common.CommonValue.FRAGMENT_PROFILE_INFO;
import static com.geotes.common.CommonValue.INCLUDE_FRAGMENT;
import static com.geotes.common.CommonValue.KEY_LOGOUT;

public class MenuActivity extends AppCompatActivity implements View.OnClickListener{

    private static ImageView img_avarta;
    private static TextView txt_user_name, txt_phone_number;

    private LinearLayout lay_home_page, lay_ebook, lay_document_tutorial, lay_video_tutorial, lay_introduction,lay_feed_back,lay_logout;
    private UserModel userModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        initUI();
        initEvent();
        bindData();
    }
    private void initUI() {

        img_avarta = (ImageView) findViewById(R.id.img_avarta);
        txt_user_name = (TextView) findViewById(R.id.txt_user_name);
        txt_phone_number = (TextView) findViewById(R.id.txt_phone_number);

        lay_home_page = (LinearLayout) findViewById(R.id.lay_home_page);
        lay_ebook = (LinearLayout) findViewById(R.id.lay_ebook);
        lay_document_tutorial = (LinearLayout) findViewById(R.id.lay_document_tutorial);
        lay_video_tutorial = (LinearLayout) findViewById(R.id.lay_video_tutorial);
        lay_introduction = (LinearLayout) findViewById(R.id.lay_introduction);
        lay_feed_back = (LinearLayout) findViewById(R.id.lay_feed_back);
        lay_logout = (LinearLayout) findViewById(R.id.lay_logout);


    }

    private void initEvent() {


        lay_home_page.setOnClickListener(this);
        lay_ebook.setOnClickListener(this);
        lay_document_tutorial.setOnClickListener(this);
        lay_video_tutorial.setOnClickListener(this);


        lay_introduction.setOnClickListener(this);
        lay_feed_back.setOnClickListener(this);
        lay_logout.setOnClickListener(this);



    }

    private void bindData() {

        SharedPreferences sharedPreferences = getSharedPreferences(Config.Pref, MODE_PRIVATE);
        String profile = sharedPreferences.getString(Config.KEY_USER, "");
        Gson gson = new Gson();
        userModel = gson.fromJson(profile, UserModel.class);

        if (userModel!=null)
        {
            PublicMethob.showImageAvarta(this,userModel.photo, img_avarta);
            if(userModel.name.equals(""))
            {
                txt_user_name.setText("User name");
            }
            else
            {
                txt_user_name.setText(userModel.name);
            }
            if (userModel.phone_number.equals(""))
            {
                txt_phone_number.setText("");
            } else
            {
                txt_phone_number.setText(userModel.phone_number);
            }

        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.txt_edit:

            case R.id.lay_home_page:
                finish();
                break;

            case R.id.lay_introduction:
                INCLUDE_FRAGMENT = FRAGMENT_CONTACT;
                CURRENT_FRAGMENT = FRAGMENT_CONTACT;
                Intent i = new Intent(MenuActivity.this, IncludeActivity.class);
                startActivity(i);

                break;
            case R.id.lay_feed_back:
                INCLUDE_FRAGMENT = FRAGMENT_FEEDBACK;
                CURRENT_FRAGMENT = FRAGMENT_FEEDBACK;
                Intent i1 = new Intent(MenuActivity.this, IncludeActivity.class);
                startActivity(i1);
                break;
            case R.id.lay_logout:



                if (userModel!=null)
                {
                    LogOutAsyncTask logOutAsyncTask = new LogOutAsyncTask(this, new LogoutRequest(userModel.id));
                    logOutAsyncTask.execute();
                }
                else
                {
                    SharedPreferences sharedPreferences =getSharedPreferences(Config.Pref, MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString(Config.KEY_USER, "").commit();
                    KEY_LOGOUT = "logout";

                    Intent intent = new Intent(MenuActivity.this, StartActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }



                break;
            case R.id.lay_ebook:
                INCLUDE_FRAGMENT = FRAGMENT_EBOOK;
                CURRENT_FRAGMENT = FRAGMENT_EBOOK;
                Intent i2 = new Intent(MenuActivity.this, IncludeActivity.class);
                startActivity(i2);
                break;
            case R.id.lay_document_tutorial:
                INCLUDE_FRAGMENT = FRAGMENT_DOCUMENT;
                CURRENT_FRAGMENT = FRAGMENT_DOCUMENT;;
                Intent i3 = new Intent(MenuActivity.this, IncludeActivity.class);
                startActivity(i3);

                break;
            case R.id.lay_video_tutorial:
                INCLUDE_FRAGMENT = FRAGMENT_PLAYLIST;
                CURRENT_FRAGMENT = FRAGMENT_PLAYLIST;
                Intent i4 = new Intent(MenuActivity.this, IncludeActivity.class);
                startActivity(i4);
                break;

        }
    }

    public class LogOutAsyncTask extends AsyncTask<LogoutRequest, Void, LogoutResponse> {

        LogoutRequest logoutRequest;
        Context ct;


        public LogOutAsyncTask(Context ct, LogoutRequest logoutRequest) {
            this.ct = ct;
            this.logoutRequest = logoutRequest;
        }

        @Override
        protected LogoutResponse doInBackground(LogoutRequest... params) {
            try {
                Thread.sleep(0);
                UserController controller = new UserController();
                return controller.logOut(logoutRequest);
            } catch (Exception ex) {
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(LogoutResponse code) {
            try {

                if (code != null) {
                    if (code.code == 200)
                    {
                        SharedPreferences sharedPreferences =getSharedPreferences(Config.Pref, MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString(Config.KEY_USER, "").commit();
                        KEY_LOGOUT = "logout";

                        Intent intent = new Intent(MenuActivity.this, StartActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                }
                else
                {
                    Toast.makeText(MenuActivity.this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                Toast.makeText(MenuActivity.this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
            }

        }
        public void onFailure(Call<GetFriendProfileResponse> call, Throwable t) {
            Toast.makeText(MenuActivity.this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
        }
    }

}
