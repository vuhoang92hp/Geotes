package com.geotes.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.geotes.BuildConfig;
import com.geotes.R;
import com.geotes.api.APICommon;
import com.geotes.api.ServiceGenerator;
import com.geotes.common.Config;
import com.geotes.fragment.fragment_main.FragmentCalculate;
import com.geotes.fragment.fragment_main.tab_profile.FragmentProfileFavourite;
import com.geotes.fragment.fragment_main.FragmentHome;
import com.geotes.fragment.fragment_main.FragmentNotification;
import com.geotes.fragment.fragment_main.FragmentPostStatus;
import com.geotes.fragment.fragment_main.tab_profile.FragmentProfile;
import com.geotes.model.UserModel;
import com.geotes.response.GetVersionCodeResponse;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.geotes.common.CommonValue.KEY_LOGOUT;
import static com.geotes.common.CommonValue.MAIN_SELECTED_TAB;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public int version_code;
    FrameLayout frame_main_all;
    ImageView img_home, img_calculate, img_notification, img_profile;
    LinearLayout lay_home, lay_calculation,lay_post_status, lay_profile;
    RelativeLayout lay_notification;
    public static TextView txt_number_notification;
    private Fragment fragmentTemp = null;
    private FragmentHome fragmentHome = new FragmentHome();
    private FragmentCalculate fragmentCalculate = new FragmentCalculate();
    private FragmentProfileFavourite fragmentFavourite = new FragmentProfileFavourite();
    private FragmentNotification fragmentNotification = new FragmentNotification();
    private FragmentPostStatus fragmentPostStatus = new FragmentPostStatus();
    private FragmentProfile fragmentProfile = new FragmentProfile();

    private UserModel userModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUI();
        initEvent();
        bindData();
    }

    private void initUI() {
        frame_main_all = (FrameLayout) findViewById(R.id.frame_main_all);
        img_home = (ImageView) findViewById(R.id.img_home);
        img_notification = (ImageView) findViewById(R.id.img_notification);
        img_calculate = (ImageView) findViewById(R.id.img_calculate);
        img_profile = (ImageView) findViewById(R.id.img_profile);
        lay_home = (LinearLayout) findViewById(R.id.lay_home);
        lay_calculation = (LinearLayout) findViewById(R.id.lay_calculation);
        lay_post_status = (LinearLayout) findViewById(R.id.lay_post_status);
        lay_notification = (RelativeLayout) findViewById(R.id.lay_notification);
        lay_profile = (LinearLayout) findViewById(R.id.lay_profile);
        txt_number_notification = (TextView) findViewById(R.id.txt_number_notification);
    }

    private void initEvent() {
        lay_home.setOnClickListener(this);
        lay_calculation.setOnClickListener(this);
        lay_post_status.setOnClickListener(this);
        lay_notification.setOnClickListener(this);
        lay_profile.setOnClickListener(this);
    }

    private void bindData() {

        //txt_number_notification.setText("6");
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        SharedPreferences sharedPreferences = getSharedPreferences(Config.Pref, MODE_PRIVATE);
        String profile = sharedPreferences.getString(Config.KEY_USER, "");
        Gson gson = new Gson();
        userModel = gson.fromJson(profile, UserModel.class);
        initFragment();

        if (MAIN_SELECTED_TAB.equals("Notification"))
        {
            MAIN_SELECTED_TAB = "Home";
            if (userModel!=null)
            {
                setTabSelected(4);
                showFragment(getFragmentTemp(),getFragmentNotification());
            }
            else
            {
                KEY_LOGOUT = "re_login";
                Intent i = new Intent(MainActivity.this, StartActivity.class);
                startActivity(i);
                finish();
            }
        }

        getVersionCode();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.lay_home:
                setTabSelected(1);
                showFragment(getFragmentTemp(),getFragmentHome());
                break;
            case R.id.lay_calculation:
                setTabSelected(2);
                showFragment(getFragmentTemp(),getFragmentCalculate());
                break;

            case R.id.lay_post_status:
                if (userModel!=null)
                {
                    setTabSelected(3);
                    showFragment(getFragmentTemp(),getFragmentPostStatus());
                }
                else
                {
                    KEY_LOGOUT = "re_login";
                    Intent i = new Intent(MainActivity.this, StartActivity.class);
                    startActivity(i);
                    finish();
                }
                break;
            case R.id.lay_notification:
            if (userModel!=null)
            {
                setTabSelected(4);
                showFragment(getFragmentTemp(),getFragmentNotification());
            }
            else
            {
                KEY_LOGOUT = "re_login";
                Intent i = new Intent(MainActivity.this, StartActivity.class);
                startActivity(i);
                finish();
            }
            break;
            case R.id.lay_profile:
                if (userModel!=null)
                {
                    setTabSelected(5);
                    showFragment(getFragmentTemp(),getFragmentProfile());
                }
                else
                {
                    KEY_LOGOUT = "re_login";
                    Intent i = new Intent(MainActivity.this, StartActivity.class);
                    startActivity(i);
                    finish();
                }
                break;



        }
    }

    private void initFragment() {

        fragmentTemp = fragmentHome;
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.frame_main_all, fragmentHome);
        transaction.add(R.id.frame_main_all, fragmentCalculate);
        transaction.add(R.id.frame_main_all, fragmentFavourite);
        transaction.add(R.id.frame_main_all, fragmentNotification);
        transaction.add(R.id.frame_main_all, fragmentPostStatus);
        transaction.add(R.id.frame_main_all, fragmentProfile);

        transaction.hide(fragmentCalculate);
        transaction.hide(fragmentFavourite);
        transaction.hide(fragmentNotification);
        transaction.hide(fragmentPostStatus);
        transaction.hide(fragmentProfile);

        transaction.commit();

    }
    public void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.frame_main_all, fragment);
        transaction.addToBackStack(backStateName);
        transaction.commit();

    }
    public void addFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.frame_main_all, fragment);
        transaction.addToBackStack(backStateName);
        transaction.commit();

    }
    public void showFragment(Fragment hide, Fragment show) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.hide(hide);
        transaction.show(show);
        fragmentTemp = show;
        transaction.commit();
    }

    public void setTabSelected(int tabSelect) {

        switch (tabSelect)
        {
            case 1:
                img_home.setImageResource(R.mipmap.ic_home_active);
                img_calculate.setImageResource(R.mipmap.ic_calculation);
                img_notification.setImageResource(R.mipmap.ic_noti);
                img_profile.setImageResource(R.mipmap.ic_user);
                break;
            case 2:
                img_home.setImageResource(R.mipmap.ic_home_de_active);
                img_calculate.setImageResource(R.mipmap.ic_calculation__active);
                img_notification.setImageResource(R.mipmap.ic_noti);
                img_profile.setImageResource(R.mipmap.ic_user);
                break;

            case 3:
                img_home.setImageResource(R.mipmap.ic_home_de_active);
                img_calculate.setImageResource(R.mipmap.ic_calculation);
                img_notification.setImageResource(R.mipmap.ic_noti);
                img_profile.setImageResource(R.mipmap.ic_user);
                break;

            case 4:
                img_home.setImageResource(R.mipmap.ic_home_de_active);
                img_calculate.setImageResource(R.mipmap.ic_calculation);
                img_notification.setImageResource(R.mipmap.ic_noti_active);
                img_profile.setImageResource(R.mipmap.ic_user);
                break;

            case 5:
                img_home.setImageResource(R.mipmap.ic_home_de_active);
                img_calculate.setImageResource(R.mipmap.ic_calculation);
                img_notification.setImageResource(R.mipmap.ic_noti);
                img_profile.setImageResource(R.mipmap.ic_user_active);
                break;
            default:
                img_home.setImageResource(R.mipmap.ic_home_de_active);
                img_calculate.setImageResource(R.mipmap.ic_calculation);
                img_notification.setImageResource(R.mipmap.ic_noti);
                img_profile.setImageResource(R.mipmap.ic_user);
                break;

        }
    }
    public static void setNumberNotification(int number)
    {
        if (number == 0)
        {
            txt_number_notification.setVisibility(View.GONE);
        }
        else
        {
            txt_number_notification.setVisibility(View.VISIBLE);
            txt_number_notification.setText(number+"");
        }
    }

    public Fragment getFragmentTemp() {
        return fragmentTemp;
    }

    public void setFragmentTemp(Fragment fragmentTemp) {
        this.fragmentTemp = fragmentTemp;
    }

    public FragmentHome getFragmentHome() {
        return fragmentHome;
    }

    public void setFragmentHome(FragmentHome fragmentHome) {
        this.fragmentHome = fragmentHome;
    }

    public FragmentCalculate getFragmentCalculate() {
        return fragmentCalculate;
    }

    public void setFragmentCalculate(FragmentCalculate fragmentCalculate) {
        this.fragmentCalculate = fragmentCalculate;
    }

    public FragmentNotification getFragmentNotification() {
        return fragmentNotification;
    }

    public void setFragmentNotification(FragmentNotification fragmentNotification) {
        this.fragmentNotification = fragmentNotification;
    }

    public FragmentProfileFavourite getFragmentFavourite() {
        return fragmentFavourite;
    }

    public void setFragmentFavourite(FragmentProfileFavourite fragmentFavourite) {
        this.fragmentFavourite = fragmentFavourite;
    }

    public FragmentPostStatus getFragmentPostStatus() {
        return fragmentPostStatus;
    }

    public void setFragmentPostStatus(FragmentPostStatus fragmentPostStatus) {
        this.fragmentPostStatus = fragmentPostStatus;
    }

    public FragmentProfile getFragmentProfile() {
        return fragmentProfile;
    }

    public void setFragmentProfile(FragmentProfile fragmentProfile) {
        this.fragmentProfile = fragmentProfile;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {

            for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }catch (Exception err){

        }
    }
    private void getVersionCode() {
        APICommon.Geotes service = ServiceGenerator.GetInstance();
        Call<GetVersionCodeResponse> getVersionCodeResponseCall;
        getVersionCodeResponseCall = service.getVersionCode();
        getVersionCodeResponseCall.enqueue(new Callback<GetVersionCodeResponse>() {
            @Override
            public void onResponse(Call<GetVersionCodeResponse> call, Response<GetVersionCodeResponse> response) {
                try {
                    if (response.body().result != null && response.code() == 200) {
                        version_code = response.body().result.version;

                        //new co phien ban moi, yeu cau update
                        if (version_code > BuildConfig.VERSION_CODE)
                        {
                            final Dialog dialog = new Dialog(MainActivity.this);
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setContentView(R.layout.dialog_update);
                            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                            int width = getResources().getDisplayMetrics().widthPixels * 1;
                            int height = getResources().getDisplayMetrics().heightPixels * 1;
                            dialog.getWindow().setLayout(width, height);
                            dialog.show();

                            TextView txt_cancel, txt_update;

                            txt_cancel = (TextView) dialog.findViewById(R.id.txt_cancel);
                            txt_update = (TextView) dialog.findViewById(R.id.txt_update);


                            //txt_content.setText(Html.fromHtml("Những bài đăng của "+"<span style=\"color: #00B7A6;\"><strong>"+"f"+"</strong></span> "+ "sẽ không hiển thị trên Bảng tin của bạn nữa"));

                            txt_cancel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    dialog.dismiss();
                                }
                            });
                            txt_update.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + getPackageName())));
                                    finish();
                                }
                            });

                            //delete status
                        }
                        else
                        {

                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<GetVersionCodeResponse> call, Throwable t) {

            }


        });
    }
}
