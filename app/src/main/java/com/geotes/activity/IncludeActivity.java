package com.geotes.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.widget.FrameLayout;

import com.geotes.R;
import com.geotes.fragment.FragmentDocument;
import com.geotes.fragment.FragmentDocumentDetail;
import com.geotes.fragment.FragmentEbook;
import com.geotes.fragment.FragmentEbookDetail;
import com.geotes.fragment.FragmentFeedback;
import com.geotes.fragment.FragmentIntroduction;
import com.geotes.fragment.FragmentPlaylist;

import com.geotes.fragment.FragmentProfileInfo;
import com.geotes.fragment.FragmentVideo;

import static com.geotes.common.CommonValue.CURRENT_FRAGMENT;
import static com.geotes.common.CommonValue.FRAGMENT_CONTACT;
import static com.geotes.common.CommonValue.FRAGMENT_COURSE;
import static com.geotes.common.CommonValue.FRAGMENT_CTV;
import static com.geotes.common.CommonValue.FRAGMENT_CTV_REGISTER;
import static com.geotes.common.CommonValue.FRAGMENT_DOCUMENT;
import static com.geotes.common.CommonValue.FRAGMENT_DOCUMENT_DETAIL;
import static com.geotes.common.CommonValue.FRAGMENT_EBOOK;
import static com.geotes.common.CommonValue.FRAGMENT_EBOOK_DETAIL;
import static com.geotes.common.CommonValue.FRAGMENT_FEEDBACK;
import static com.geotes.common.CommonValue.FRAGMENT_MENU;
import static com.geotes.common.CommonValue.FRAGMENT_PLAYLIST;
import static com.geotes.common.CommonValue.FRAGMENT_PLAYLIST_DETAIL;
import static com.geotes.common.CommonValue.FRAGMENT_PROFILE_INFO;
import static com.geotes.common.CommonValue.INCLUDE_FRAGMENT;

/**
 * Created by Hoang on 1/1/2018.
 */

public class IncludeActivity extends AppCompatActivity {
    private FrameLayout frame_include;
    private Fragment fragmentTemp = null;

   // private FragmentCalculate fragmentCalculate = new FragmentCalculate();
    private FragmentPlaylist fragmentPlaylist = new FragmentPlaylist();
    private FragmentVideo fragmentVideo = new FragmentVideo();
    private FragmentProfileInfo fragmentProfileInfo = new FragmentProfileInfo();
    private FragmentFeedback fragmentFeedback = new FragmentFeedback();
    private FragmentIntroduction fragmentIntroduction = new FragmentIntroduction();
    //private FragmentLibrary fragmentLibrary = new FragmentLibrary();
    private FragmentDocument fragmentDocument = new FragmentDocument();
    private FragmentDocumentDetail fragmentDocumentDetail = new FragmentDocumentDetail();
    private FragmentEbook fragmentEbook = new FragmentEbook();
    private FragmentEbookDetail fragmentEbookDetail = new FragmentEbookDetail();

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.show_left, R.anim.hide_right);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_include);
        initUI();
        initFragment();
        initEvent();
        bindData();
    }



    private void initUI() {
        frame_include = (FrameLayout) findViewById(R.id.frame_include);
    }

    private void initEvent() {
    }

    private void bindData() {
    }
    private void initFragment() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.frame_include, fragmentPlaylist);
        transaction.add(R.id.frame_include, fragmentProfileInfo);
        transaction.add(R.id.frame_include, fragmentFeedback);
        transaction.add(R.id.frame_include, fragmentEbook);
        transaction.add(R.id.frame_include, fragmentDocument);
        transaction.add(R.id.frame_include, fragmentIntroduction);

        transaction.hide(fragmentIntroduction);
        transaction.hide(fragmentPlaylist);

        transaction.hide(fragmentProfileInfo);
        transaction.hide(fragmentFeedback);
        transaction.hide(fragmentEbook);
        transaction.hide(fragmentDocument);




        if (INCLUDE_FRAGMENT.equals(FRAGMENT_MENU))
        {

        }

        if (INCLUDE_FRAGMENT.equals(FRAGMENT_PROFILE_INFO))
        {
            transaction.show(fragmentProfileInfo);
            fragmentTemp = fragmentProfileInfo;
        }

        if (INCLUDE_FRAGMENT.equals(FRAGMENT_CONTACT))
        {
            transaction.show(fragmentIntroduction);
            fragmentTemp = fragmentIntroduction;
        }

        if (INCLUDE_FRAGMENT.equals(FRAGMENT_FEEDBACK))
        {
            transaction.show(fragmentFeedback);
            fragmentTemp = fragmentFeedback;
        }

        if (INCLUDE_FRAGMENT.equals(FRAGMENT_EBOOK))
        {
            transaction.show(fragmentEbook);
            fragmentTemp = fragmentFeedback;
        }

        if (INCLUDE_FRAGMENT.equals(FRAGMENT_DOCUMENT))
        {
            transaction.show(fragmentDocument);
            fragmentTemp = fragmentFeedback;
        }

        if (INCLUDE_FRAGMENT.equals(FRAGMENT_PLAYLIST))
        {
            transaction.show(fragmentPlaylist);
            fragmentTemp = fragmentFeedback;
        }

        transaction.commit();
    }

    public void showFrm(Fragment hide, Fragment show) {

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.show(show);
        transaction.hide(hide);
        fragmentTemp = show;
        transaction.commit();
    }
    public void hideFrm(Fragment hide)
    {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.hide(hide);
        transaction.commit();
    }

    public void addFrm(Fragment fragment){
        String backStateName = fragment.getClass().getName();

        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped)//fragment not in back stack, create it.
        {
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.add(R.id.frame_include, fragment);
            transaction.addToBackStack("");
            fragmentTemp = fragment;
            transaction.commit();
        }
        else
        {
            replaceFrm(fragment);
        }

    }
    public void removeFrm(Fragment remove)
    {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.remove(remove);
        transaction.commit();
    }

    public void replaceFrm(Fragment fragment)
    {
        String backStateName = fragment.getClass().getName();

        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped) {
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.frame_include, fragment);
            ft.addToBackStack(backStateName);
            ft.commit();
        }

    }

    public Fragment getFragmentTemp() {
        return fragmentTemp;
    }

    public void setFragmentTemp(Fragment fragmentTemp) {
        this.fragmentTemp = fragmentTemp;
    }


    public FragmentPlaylist getFragmentPlaylist() {
        return fragmentPlaylist;
    }

    public void setFragmentPlaylist(FragmentPlaylist fragmentPlaylist) {
        this.fragmentPlaylist = fragmentPlaylist;
    }




    public FragmentVideo getFragmentVideo() {
        return fragmentVideo;
    }

    public void setFragmentVideo(FragmentVideo fragmentVideo) {
        this.fragmentVideo = fragmentVideo;
    }

    public FragmentProfileInfo getFragmentProfileInfo() {
        return fragmentProfileInfo;
    }

    public void setFragmentProfileInfo(FragmentProfileInfo fragmentProfileInfo) {
        this.fragmentProfileInfo = fragmentProfileInfo;
    }

    public FragmentFeedback getFragmentFeedback() {
        return fragmentFeedback;
    }

    public void setFragmentFeedback(FragmentFeedback fragmentFeedback) {
        this.fragmentFeedback = fragmentFeedback;
    }

    public FragmentIntroduction getFragmentIntroduction() {
        return fragmentIntroduction;
    }

    public void setFragmentIntroduction(FragmentIntroduction fragmentIntroduction) {
        this.fragmentIntroduction = fragmentIntroduction;
    }

    /*public FragmentLibrary getFragmentLibrary() {
        return fragmentLibrary;
    }

    public void setFragmentLibrary(FragmentLibrary fragmentLibrary) {
        this.fragmentLibrary = fragmentLibrary;
    }*/

    public FragmentDocumentDetail getFragmentDocumentDetail() {
        return fragmentDocumentDetail;
    }

    public void setFragmentDocumentDetail(FragmentDocumentDetail fragmentDocumentDetail) {
        this.fragmentDocumentDetail = fragmentDocumentDetail;
    }

    public FragmentDocument getFragmentDocument() {
        return fragmentDocument;
    }

    public void setFragmentDocument(FragmentDocument fragmentDocument) {
        this.fragmentDocument = fragmentDocument;
    }

    public FragmentEbook getFragmentEbook() {
        return fragmentEbook;
    }

    public void setFragmentEbook(FragmentEbook fragmentEbook) {
        this.fragmentEbook = fragmentEbook;
    }

    public FragmentEbookDetail getFragmentEbookDetail() {
        return fragmentEbookDetail;
    }

    public void setFragmentEbookDetail(FragmentEbookDetail fragmentEbookDetail) {
        this.fragmentEbookDetail = fragmentEbookDetail;
    }

    @Override
    public void onBackPressed() {
        if (CURRENT_FRAGMENT.equals(FRAGMENT_CONTACT))
        {
            CURRENT_FRAGMENT = FRAGMENT_MENU;
            finish();
        }
        else if (CURRENT_FRAGMENT.equals(FRAGMENT_FEEDBACK))
        {

            CURRENT_FRAGMENT = FRAGMENT_MENU;
            finish();
        }
        else if (CURRENT_FRAGMENT.equals(FRAGMENT_EBOOK))
        {
            CURRENT_FRAGMENT = FRAGMENT_MENU;
            finish();
        }
        else if (CURRENT_FRAGMENT.equals(FRAGMENT_DOCUMENT))
        {
            CURRENT_FRAGMENT = FRAGMENT_MENU;
            finish();
        }
        else if (CURRENT_FRAGMENT.equals(FRAGMENT_PLAYLIST))
        {
            CURRENT_FRAGMENT = FRAGMENT_MENU;
            finish();
        }
        else if (CURRENT_FRAGMENT.equals(FRAGMENT_EBOOK_DETAIL))
        {

            replaceFrm(getFragmentEbook());
            CURRENT_FRAGMENT = FRAGMENT_EBOOK;
        }
        else if (CURRENT_FRAGMENT.equals(FRAGMENT_DOCUMENT_DETAIL))
        {

            replaceFrm(getFragmentDocument());

            CURRENT_FRAGMENT = FRAGMENT_DOCUMENT;
        }
        else if (CURRENT_FRAGMENT.equals(FRAGMENT_PLAYLIST_DETAIL))
        {

            replaceFrm( getFragmentPlaylist());
            CURRENT_FRAGMENT = FRAGMENT_PLAYLIST;
        }
        else if (CURRENT_FRAGMENT.equals(FRAGMENT_PROFILE_INFO))
        {
            finish();
        }
        else {
            finish();
        }

    }
}
