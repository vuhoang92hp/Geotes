package com.geotes.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.widget.FrameLayout;

import com.geotes.R;
import com.geotes.fragment.FragmentDienTicTamGiacTheoSin;
import com.geotes.fragment.FragmentDienTichDaGiac;
import com.geotes.fragment.FragmentDienTichTamGiac3Canh;
import com.geotes.fragment.FragmentSoCaDo;
import com.geotes.fragment.FragmentTinhThongSoDuongCong;
import com.geotes.fragment.FragmentChuyenHeToaDoGiaDinh;
import com.geotes.fragment.FragmentTracDiaNghich;
import com.geotes.fragment.FragmentTracDiaThuan;

import static com.geotes.common.CommonValue.CALCULATE_FRAGMENT;
import static com.geotes.common.CommonValue.FRAGMENT_DIEN_TICH_DA_GIAC;
import static com.geotes.common.CommonValue.FRAGMENT_DIEN_TICH_TAM_GIAC_3_CANH;
/*import static com.geotes.common.CommonValue.FRAGMENT_DIEN_TICH_TAM_GIAC_THEO_SIN;*/
import static com.geotes.common.CommonValue.FRAGMENT_GIAO_HOI_GOC;
import static com.geotes.common.CommonValue.FRAGMENT_GIAO_HOI_NGHICH_3_DIEM;
import static com.geotes.common.CommonValue.FRAGMENT_GIAO_HOI_THUAN;
import static com.geotes.common.CommonValue.FRAGMENT_TRAC_DIA_NGHICH;
import static com.geotes.common.CommonValue.FRAGMENT_TRAC_DIA_THUAN;

/**
 * Created by Hoang on 1/1/2018.
 */

public class CalculateActivity extends AppCompatActivity {
    private FrameLayout frame_include;

    private Fragment fragmentTemp = null;
    private FragmentTracDiaThuan fragmentTracDiaThuan = new FragmentTracDiaThuan();
    private FragmentTracDiaNghich fragmentTracDiaNghich = new FragmentTracDiaNghich();
    private FragmentChuyenHeToaDoGiaDinh fragmentChuyenHeToaDoGiaDinh = new FragmentChuyenHeToaDoGiaDinh();
    private FragmentTinhThongSoDuongCong fragmentTinhThongSoDuongCong = new FragmentTinhThongSoDuongCong();
    private FragmentSoCaDo fragmentSoCaDo = new FragmentSoCaDo();
    private FragmentDienTichDaGiac fragmentDienTichDaGiac = new FragmentDienTichDaGiac();
    private FragmentDienTichTamGiac3Canh fragmentDienTichTamGiac3Canh = new FragmentDienTichTamGiac3Canh();
    private FragmentDienTicTamGiacTheoSin fragmentDienTicTamGiacTheoSin = new FragmentDienTicTamGiacTheoSin();

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.show_left, R.anim.hide_right);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_include);
        initUI();
        initFragment();
        initEvent();
        bindData();
    }

    private void initUI() {
        frame_include = (FrameLayout) findViewById(R.id.frame_include);
    }

    private void initEvent() {
    }

    private void bindData() {
    }

    private void initFragment() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.frame_include, fragmentTracDiaThuan);
        transaction.add(R.id.frame_include, fragmentTracDiaNghich);
        transaction.add(R.id.frame_include, fragmentChuyenHeToaDoGiaDinh);
        transaction.add(R.id.frame_include, fragmentTinhThongSoDuongCong);
        transaction.add(R.id.frame_include, fragmentSoCaDo);
        transaction.add(R.id.frame_include, fragmentDienTichDaGiac);
        transaction.add(R.id.frame_include, fragmentDienTichTamGiac3Canh);
        transaction.add(R.id.frame_include, fragmentDienTicTamGiacTheoSin);
        transaction.hide(fragmentTracDiaThuan);
        transaction.hide(fragmentTracDiaNghich);
        transaction.hide(fragmentChuyenHeToaDoGiaDinh);
        transaction.hide(fragmentTinhThongSoDuongCong);
        transaction.hide(fragmentSoCaDo);
        transaction.hide(fragmentDienTichDaGiac);
        transaction.hide(fragmentDienTichTamGiac3Canh);
        transaction.hide(fragmentDienTicTamGiacTheoSin);

        if (CALCULATE_FRAGMENT.equals(FRAGMENT_TRAC_DIA_THUAN))
        {
            //addFrm(fragmentCalculate);
            transaction.show(fragmentTracDiaThuan);
            fragmentTemp = fragmentTracDiaThuan;
        }
        else if (CALCULATE_FRAGMENT.equals(FRAGMENT_TRAC_DIA_NGHICH))
        {
            transaction.show(fragmentTracDiaNghich);
            fragmentTemp = fragmentTracDiaNghich;
        }
        else if (CALCULATE_FRAGMENT.equals(FRAGMENT_GIAO_HOI_THUAN))
        {
            transaction.show(fragmentChuyenHeToaDoGiaDinh);
            fragmentTemp = fragmentChuyenHeToaDoGiaDinh;
        }
        else if (CALCULATE_FRAGMENT.equals(FRAGMENT_GIAO_HOI_NGHICH_3_DIEM))
        {
            transaction.show(fragmentTinhThongSoDuongCong);
            fragmentTemp = fragmentTinhThongSoDuongCong;
        }
        else if (CALCULATE_FRAGMENT.equals(FRAGMENT_GIAO_HOI_GOC))
        {
            transaction.show(fragmentSoCaDo);
            fragmentTemp = fragmentSoCaDo;
        }
        else if (CALCULATE_FRAGMENT.equals(FRAGMENT_DIEN_TICH_DA_GIAC))
        {
            transaction.show(fragmentDienTichDaGiac);
            fragmentTemp = fragmentDienTichDaGiac;
        }
        else if (CALCULATE_FRAGMENT.equals(FRAGMENT_DIEN_TICH_TAM_GIAC_3_CANH))
        {
            transaction.show(fragmentDienTichTamGiac3Canh);
            fragmentTemp = fragmentDienTichTamGiac3Canh;
        }
        /*else if (CALCULATE_FRAGMENT.equals(FRAGMENT_DIEN_TICH_TAM_GIAC_THEO_SIN))
        {
            transaction.show(fragmentDienTicTamGiacTheoSin);
            fragmentTemp = fragmentDienTicTamGiacTheoSin;
        }*/


        transaction.commit();
    }

    public void showFrm(Fragment hide, Fragment show) {

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.show(show);
        transaction.hide(hide);
        fragmentTemp = show;
        transaction.commit();
    }

    public void addFrm(Fragment fragment){
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.add(R.id.frame_include, fragment);
        transaction.addToBackStack("");
        fragmentTemp = fragment;
        transaction.commit();
    }

    public Fragment getFragmentTemp() {
        return fragmentTemp;
    }

    public void setFragmentTemp(Fragment fragmentTemp) {
        this.fragmentTemp = fragmentTemp;
    }





    public FragmentTracDiaThuan getFragmentTracDiaThuan() {
        return fragmentTracDiaThuan;
    }

    public void setFragmentTracDiaThuan(FragmentTracDiaThuan fragmentTracDiaThuan) {
        this.fragmentTracDiaThuan = fragmentTracDiaThuan;
    }

    public FragmentTracDiaNghich getFragmentTracDiaNghich() {
        return fragmentTracDiaNghich;
    }

    public void setFragmentTracDiaNghich(FragmentTracDiaNghich fragmentTracDiaNghich) {
        this.fragmentTracDiaNghich = fragmentTracDiaNghich;
    }

    public FragmentChuyenHeToaDoGiaDinh getFragmentChuyenHeToaDoGiaDinh() {
        return fragmentChuyenHeToaDoGiaDinh;
    }

    public void setFragmentChuyenHeToaDoGiaDinh(FragmentChuyenHeToaDoGiaDinh fragmentChuyenHeToaDoGiaDinh) {
        this.fragmentChuyenHeToaDoGiaDinh = fragmentChuyenHeToaDoGiaDinh;
    }

    public FragmentTinhThongSoDuongCong getFragmentTinhThongSoDuongCong() {
        return fragmentTinhThongSoDuongCong;
    }

    public void setFragmentTinhThongSoDuongCong(FragmentTinhThongSoDuongCong fragmentTinhThongSoDuongCong) {
        this.fragmentTinhThongSoDuongCong = fragmentTinhThongSoDuongCong;
    }

    public FragmentSoCaDo getFragmentSoCaDo() {
        return fragmentSoCaDo;
    }

    public void setFragmentSoCaDo(FragmentSoCaDo fragmentSoCaDo) {
        this.fragmentSoCaDo = fragmentSoCaDo;
    }

    public FragmentDienTichDaGiac getFragmentDienTichDaGiac() {
        return fragmentDienTichDaGiac;
    }

    public void setFragmentDienTichDaGiac(FragmentDienTichDaGiac fragmentDienTichDaGiac) {
        this.fragmentDienTichDaGiac = fragmentDienTichDaGiac;
    }

    public FragmentDienTichTamGiac3Canh getFragmentDienTichTamGiac3Canh() {
        return fragmentDienTichTamGiac3Canh;
    }

    public void setFragmentDienTichTamGiac3Canh(FragmentDienTichTamGiac3Canh fragmentDienTichTamGiac3Canh) {
        this.fragmentDienTichTamGiac3Canh = fragmentDienTichTamGiac3Canh;
    }

    public FragmentDienTicTamGiacTheoSin getFragmentDienTicTamGiacTheoSin() {
        return fragmentDienTicTamGiacTheoSin;
    }

    public void setFragmentDienTicTamGiacTheoSin(FragmentDienTicTamGiacTheoSin fragmentDienTicTamGiacTheoSin) {
        this.fragmentDienTicTamGiacTheoSin = fragmentDienTicTamGiacTheoSin;
    }
}
