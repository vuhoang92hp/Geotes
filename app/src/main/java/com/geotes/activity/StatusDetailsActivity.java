package com.geotes.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.geotes.R;
import com.geotes.adapter.ListCommentsAdapter;
import com.geotes.adapter.ListViewNewFeedAdapter;
import com.geotes.api.APICommon;
import com.geotes.api.ServiceGenerator;
import com.geotes.common.CRecyclerView;
import com.geotes.common.CommonValue;
import com.geotes.common.Config;
import com.geotes.controller.UserController;
import com.geotes.model.CommentModel;
import com.geotes.model.NewFeedModel;
import com.geotes.model.UserModel;
import com.geotes.request.ActiveAccountRequest;
import com.geotes.request.AddCommentRequest;
import com.geotes.request.AddFavouriteRequest;
import com.geotes.request.AddFollowRequest;
import com.geotes.request.AddShareRequest;
import com.geotes.request.DeletePostRequest;
import com.geotes.request.UpdateProfileRequest;
import com.geotes.response.ActiveAccountResponse;
import com.geotes.response.AddCommentResponse;
import com.geotes.response.AddFavouriteResponse;
import com.geotes.response.AddFollowResponse;
import com.geotes.response.AddShareResponse;
import com.geotes.response.DeletePostResponse;
import com.geotes.response.GetNewFeedResponse;
import com.geotes.response.StatusDetailsResponse;
import com.geotes.response.UpdateProfileResponse;
import com.google.gson.Gson;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.geotes.common.CommonValue.FRIEND_PROFILE_ID;
import static com.geotes.common.CommonValue.KEY_LOGOUT;

/**
 * Created by meobu on 11/18/2017.
 */

public class StatusDetailsActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView img_detail_avarta;
    private TextView txtNamePost;
    private TextView txt_content_newfeed,txtTimePost;
    private TextView number_like,tv_number_comment,tv_number_share,txtNumberPhotoMore;
    private ImageView imgPhotoTwo,imgPhotoOne,imgPhotoThree,imgPhotoMore;
    private ImageView imgPhotoOneHoz,imgPhotoTwoHoz,imgPhotoThreeHoz,imgPhotoMoreHoz;
    private ImageView imgPhotoOneVer2,imgPhotoTwoVer2;
    private ImageView imgPhotoOneHoz2,imgPhotoTwoHoz2,img_delete;
    private EditText edtContent;
    private ImageView btnSend;
    public boolean isCheck=true;
    public LinearLayout layout4Ver,layout4Hoz,layout2Hoz,layout2Ver;
    public RelativeLayout layoutMorePhoto,layoutMoreTextHoz,layoutMoreText;
    private CRecyclerView recyclerView;
    private int newfeedId;
    private SharedPreferences sharedPreferences;
    private Gson gson;
    private UserModel userModel;
    private ListCommentsAdapter adapter;
    private ArrayList<CommentModel> arrComments = new ArrayList<>();
    private String []lstPhoto;
    private ImageView iv_is_like;
    private LinearLayout lay_review_class_care,ll_class_share;
    private NewFeedModel newFeedModel;
    private int quantity_like = 0, number_share = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.status_details_activity);
        gson = new Gson();
        sharedPreferences = getSharedPreferences(Config.Pref, Context.MODE_PRIVATE);
        String profile = sharedPreferences.getString(Config.KEY_USER, "");
        userModel = gson.fromJson(profile, UserModel.class);
        newfeedId = getIntent().getExtras().getInt(CommonValue.KEY_NEWFEED_ID);
        initUI();
        bindData();
        initEven();
    }

    private void initEven() {
        img_delete.setOnClickListener(this);
        txtNamePost.setOnClickListener(this);
        lay_review_class_care.setOnClickListener(this);
        ll_class_share.setOnClickListener(this);
        layout2Hoz.setOnClickListener(this);
        layout2Ver.setOnClickListener(this);
        layout4Hoz.setOnClickListener(this);
        layout4Ver.setOnClickListener(this);
        btnSend.setOnClickListener(this);
        edtContent.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE){
                    if (!edtContent.getText().toString().trim().matches("")){
                       // AddComment addComment = new AddComment(new AddCommentToNewFeedRequest(userModel.getId(), userModel.getApiToken(), newfeedId, edtContent.getText().toString().trim()), StatusDetailsActivity.this);
                       // addComment.execute();
                    }
                }
                return false;
            }
        });
    }

    private void bindData() {
        adapter = new ListCommentsAdapter(this, arrComments);
        adapter.clear();
        recyclerView.setAdapter(adapter);
        GetStatusDetailsAsynctask();



    }

    public void setData(final NewFeedModel object) {

        if (userModel!=null)
        {
            if (userModel.id == object.user_id)
            {
                img_delete.setVisibility(View.VISIBLE);
            }
        }

        quantity_like = object.getNumber_like();
        number_share = object.getNumber_share();

        newFeedModel = object;
        adapter.addAll(object.getComments());
        if (object.photoUser != null) {
            Picasso.with(this).load(object.photoUser).fit().centerCrop().into(img_detail_avarta, new com.squareup.picasso.Callback() {
                @Override
                public void onSuccess() {
                        /*progress.setVisibility(View.GONE);*/
                }

                @Override
                public void onError() {
                        /*progress.setVisibility(View.GONE);*/
                    Picasso.with(StatusDetailsActivity.this).load(R.mipmap.ic_no_avarta).fit().centerCrop().into(img_detail_avarta);
                }
            });
        } else {
              /*  progress.setVisibility(View.GONE);*/
            Picasso.with(this).load(R.mipmap.ic_no_avarta).fit().centerCrop().into(img_detail_avarta);
        }
        txtNamePost.setText(object.nameUser);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date dt = sdf.parse(object.created_at);

            SimpleDateFormat sdfs=new SimpleDateFormat("dd/MM/yyyy");
            String goal=sdfs.format(dt.getTime());
            txtTimePost.setText("Đăng ngày "+goal);
        } catch (ParseException e) {
            e.printStackTrace();
        }
       // txt_content_newfeed.setText(object.content);
        txt_content_newfeed.setText(Html.fromHtml(object.content));

        number_like.setText(object.number_like+" Quan tâm");
        tv_number_comment.setText(object.number_comment+"");
        tv_number_share.setText(object.number_share+"");
        lstPhoto = object.photo.split(",");
        if(lstPhoto.length>0){

            if(object.type_photo==1){
                if(lstPhoto.length>=4){
                    layout4Ver.setVisibility(View.VISIBLE);
                    layout4Hoz.setVisibility(View.GONE);
                    layout2Hoz.setVisibility(View.GONE);
                    layout2Ver.setVisibility(View.GONE);
                    Picasso.with(this).load(lstPhoto[0]).fit().centerCrop().into(imgPhotoOne);
                    Picasso.with(this).load(lstPhoto[1]).fit().centerCrop().into(imgPhotoTwo);
                    Picasso.with(this).load(lstPhoto[2]).fit().centerCrop().into(imgPhotoThree);
                    Picasso.with(this).load(lstPhoto[3]).fit().centerCrop().into(imgPhotoMore);
                    if(lstPhoto.length==4){
                        layoutMoreText.setVisibility(View.GONE);
                    }else {
                        layoutMoreText.setVisibility(View.VISIBLE);
                        txtNumberPhotoMore.setText("+"+(lstPhoto.length-4));
                    }
                }else if(lstPhoto.length==3){
                    layout4Ver.setVisibility(View.VISIBLE);
                    layout4Hoz.setVisibility(View.GONE);
                    layout2Hoz.setVisibility(View.GONE);
                    layout2Ver.setVisibility(View.GONE);
                    Picasso.with(this).load(lstPhoto[0]).fit().centerCrop().into(imgPhotoOne);
                    Picasso.with(this).load(lstPhoto[1]).fit().centerCrop().into(imgPhotoTwo);
                    Picasso.with(this).load(lstPhoto[2]).fit().centerCrop().into(imgPhotoThree);
                    layoutMorePhoto.setVisibility(View.GONE);
                }else if(lstPhoto.length==2){
                    layout4Ver.setVisibility(View.GONE);
                    layout4Hoz.setVisibility(View.GONE);
                    layout2Hoz.setVisibility(View.GONE);
                    layout2Ver.setVisibility(View.VISIBLE);

                    Picasso.with(this).load(lstPhoto[0]).fit().centerCrop().into(imgPhotoOneVer2);
                    Picasso.with(this).load(lstPhoto[1]).fit().centerCrop().into(imgPhotoTwoVer2);
                }else if(lstPhoto.length==1){
                    layout4Ver.setVisibility(View.GONE);
                    layout4Hoz.setVisibility(View.GONE);
                    layout2Hoz.setVisibility(View.GONE);
                    layout2Ver.setVisibility(View.VISIBLE);
                    Picasso.with(this).load(lstPhoto[0]).fit().centerCrop().into(imgPhotoOneVer2);
                    imgPhotoTwoVer2.setVisibility(View.GONE);
                }
            }else {
                if(lstPhoto.length>=4){
                    layout4Ver.setVisibility(View.GONE);
                    layout4Hoz.setVisibility(View.VISIBLE);
                    layout2Hoz.setVisibility(View.GONE);
                    layout2Ver.setVisibility(View.GONE);

                    Picasso.with(this).load(lstPhoto[0]).fit().centerCrop().into(imgPhotoOneHoz);
                    Picasso.with(this).load(lstPhoto[1]).fit().centerCrop().into(imgPhotoTwoHoz);
                    Picasso.with(this).load(lstPhoto[2]).fit().centerCrop().into(imgPhotoThreeHoz);
                    Picasso.with(this).load(lstPhoto[3]).fit().centerCrop().into(imgPhotoMoreHoz);

                    if(lstPhoto.length==4){
                        layoutMoreTextHoz.setVisibility(View.GONE);
                    }else {
                        layoutMoreTextHoz.setVisibility(View.VISIBLE);
                        txtNumberPhotoMore.setText("+"+(lstPhoto.length-4));
                    }
                }else if(lstPhoto.length==3){
                    layout4Ver.setVisibility(View.GONE);
                    layout4Hoz.setVisibility(View.VISIBLE);
                    layout2Hoz.setVisibility(View.GONE);
                    layout2Ver.setVisibility(View.GONE);

                    Picasso.with(this).load(lstPhoto[0]).fit().centerCrop().into(imgPhotoOneHoz);
                    Picasso.with(this).load(lstPhoto[1]).fit().centerCrop().into(imgPhotoTwoHoz);
                    Picasso.with(this).load(lstPhoto[2]).fit().centerCrop().into(imgPhotoThreeHoz);
                    layoutMorePhoto.setVisibility(View.GONE);
                }else if(lstPhoto.length==2){
                    layout4Ver.setVisibility(View.GONE);
                    layout4Hoz.setVisibility(View.GONE);
                    layout2Hoz.setVisibility(View.VISIBLE);
                    layout2Ver.setVisibility(View.GONE);

                    Picasso.with(this).load(lstPhoto[0]).fit().centerCrop().into(imgPhotoOneHoz2);
                    Picasso.with(this).load(lstPhoto[1]).fit().centerCrop().into(imgPhotoTwoHoz2);
                }else if(lstPhoto.length==1){
                    layout4Ver.setVisibility(View.GONE);
                    layout4Hoz.setVisibility(View.GONE);
                    layout2Hoz.setVisibility(View.VISIBLE);
                    layout2Ver.setVisibility(View.GONE);

                    Picasso.with(this).load(lstPhoto[0]).fit().centerCrop().into(imgPhotoOneHoz2);
                    imgPhotoTwoHoz2.setVisibility(View.GONE);
                }
            }

        }
        if (object.is_liked == 0)
        {
            iv_is_like.setImageResource(R.mipmap.ic_favorite_grey);
        }
        else
        {
            iv_is_like.setImageResource(R.mipmap.ic_heart_active);
        }
    }

    private void initUI() {
        img_delete = (ImageView) findViewById(R.id.img_delete);
        ll_class_share = (LinearLayout) findViewById(R.id.ll_class_share);
        lay_review_class_care = (LinearLayout) findViewById(R.id.lay_review_class_care);
        iv_is_like = (ImageView) findViewById(R.id.iv_is_like);
        img_detail_avarta =(ImageView) findViewById(R.id.img_detail_avarta);
        imgPhotoTwo =(ImageView) findViewById(R.id.imgPhotoTwo);
        imgPhotoOne =(ImageView) findViewById(R.id.imgPhotoOne);
        imgPhotoThree =(ImageView) findViewById(R.id.imgPhotoThree);
        imgPhotoMore =(ImageView) findViewById(R.id.imgPhotoMore);
        txtNamePost = (TextView) findViewById(R.id.txtNamePost);
        txtTimePost = (TextView) findViewById(R.id.txtTimePost);
        txt_content_newfeed = (TextView) findViewById(R.id.txt_content_newfeed);
        number_like = (TextView) findViewById(R.id.number_like);
        tv_number_comment = (TextView) findViewById(R.id.tv_number_comment);
        tv_number_share = (TextView) findViewById(R.id.tv_number_share);
        txtNumberPhotoMore = (TextView) findViewById(R.id.txtNumberPhotoMore);
        layout4Ver = (LinearLayout) findViewById(R.id.layout4Ver);
        layout4Hoz = (LinearLayout) findViewById(R.id.layout4Hoz);
        layout2Hoz = (LinearLayout) findViewById(R.id.layout2Hoz);
        layout2Ver = (LinearLayout) findViewById(R.id.layout2Ver);
        layoutMorePhoto = (RelativeLayout) findViewById(R.id.layoutMorePhoto);
        layoutMoreTextHoz = (RelativeLayout) findViewById(R.id.layoutMoreTextHoz);
        layoutMoreText = (RelativeLayout) findViewById(R.id.layoutMoreText);
        imgPhotoOneHoz =(ImageView) findViewById(R.id.imgPhotoOneHoz);
        imgPhotoTwoHoz =(ImageView) findViewById(R.id.imgPhotoTwoHoz);
        imgPhotoThreeHoz =(ImageView) findViewById(R.id.imgPhotoThreeHoz);
        imgPhotoMoreHoz =(ImageView) findViewById(R.id.imgPhotoMoreHoz);
        imgPhotoOneVer2 =(ImageView) findViewById(R.id.imgPhotoOneVer2);
        imgPhotoTwoVer2 =(ImageView) findViewById(R.id.imgPhotoTwoVer2);
        imgPhotoOneHoz2 =(ImageView) findViewById(R.id.imgPhotoOneHoz2);
        imgPhotoTwoHoz2 =(ImageView) findViewById(R.id.imgPhotoTwoHoz2);
        recyclerView = (CRecyclerView) findViewById(R.id.rc_list_comments);
        edtContent = (EditText) findViewById(R.id.edt_comment_content);
        btnSend = (ImageView) findViewById(R.id.btn_send);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, GalleryImageActivity.class);
        List<String> list = Arrays.asList(lstPhoto);
        switch (v.getId()){
            case R.id.layout2Hoz:
                GalleryImageActivity.imagePhoto=list;
                startActivity(intent);
                break;
            case R.id.layout2Ver:
                GalleryImageActivity.imagePhoto=list;
                startActivity(intent);
                break;
            case R.id.layout4Hoz:
                GalleryImageActivity.imagePhoto=list;
                startActivity(intent);
                break;
            case R.id.txtNamePost:
                FRIEND_PROFILE_ID = newFeedModel.user_id;
                Intent i = new Intent(StatusDetailsActivity.this, FriendProfileActivity.class);
                startActivity(i);
                break;
            case R.id.layout4Ver:
                GalleryImageActivity.imagePhoto=list;
                startActivity(intent);
                break;
            case R.id.btn_send:
                if (!edtContent.getText().toString().trim().matches("")){
                    AddCommentAsyncTask addComment = new AddCommentAsyncTask(this, new AddCommentRequest(userModel.id, newfeedId, edtContent.getText().toString().trim()));
                    addComment.execute();
                }
                break;
            case R.id.lay_review_class_care:
                if (userModel!=null)
                {
                    if (newFeedModel.is_liked == 0)
                    {
                        iv_is_like.setImageResource(R.mipmap.ic_heart_active);
                        newFeedModel.setIs_liked(1);
                        quantity_like = quantity_like + 1;

                    }
                    else
                    {
                        iv_is_like.setImageResource(R.mipmap.ic_favorite_grey);
                        newFeedModel.setIs_liked(0);
                        quantity_like = quantity_like - 1;
                    }

                    number_like.setText(quantity_like+" Quan tâm");




                    AddFavouriteAsyncTask addFavouriteAsyncTask = new AddFavouriteAsyncTask(StatusDetailsActivity.this, new AddFavouriteRequest(userModel.id, newFeedModel.id));
                    addFavouriteAsyncTask.execute();
                }
                else {
                    KEY_LOGOUT = "re_login";
                    Intent i1 = new Intent(StatusDetailsActivity.this, StartActivity.class);
                    startActivity(i1);
                    finish();
                }

                break;
            case R.id.ll_class_share:
                number_share = number_share+1;
                tv_number_share.setText(number_share+"");

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT,"https://play.google.com/store/apps/details?id=com.geotes&hl=vi");
                sendIntent.setType("text/plain");
                startActivity(sendIntent);

                AddShareAsyncTask addShareAsyncTask = new AddShareAsyncTask(StatusDetailsActivity.this, new AddShareRequest(userModel.id,newFeedModel.id));
                addShareAsyncTask.execute();
                break;

            case R.id.img_delete:
                final Dialog dialog = new Dialog(StatusDetailsActivity.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.dialog_delete);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                int width = getResources().getDisplayMetrics().widthPixels * 1;
                int height = getResources().getDisplayMetrics().heightPixels * 1;
                dialog.getWindow().setLayout(width, height);
                dialog.show();

               TextView txt_follow, txt_unfollow,txt_content;

               txt_follow = (TextView) dialog.findViewById(R.id.txt_follow);
                txt_unfollow = (TextView) dialog.findViewById(R.id.txt_unfollow);
                txt_content = (TextView) dialog.findViewById(R.id.txt_content);

                //txt_content.setText(Html.fromHtml("Những bài đăng của "+"<span style=\"color: #00B7A6;\"><strong>"+"f"+"</strong></span> "+ "sẽ không hiển thị trên Bảng tin của bạn nữa"));

                txt_follow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                txt_unfollow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DeletePostAsyncTask deletePostAsyncTask = new DeletePostAsyncTask(StatusDetailsActivity.this, new DeletePostRequest(userModel.id, userModel.ApiToken, newFeedModel.id));
                        deletePostAsyncTask.execute();
                    }
                });

                    //delete status
                break;
            default:
        }
    }
    private void GetStatusDetailsAsynctask() {
        APICommon.Geotes service = ServiceGenerator.GetInstance();
        Call<StatusDetailsResponse> getStatusDetail;
        getStatusDetail = service.getNewFeedDetail(userModel.id, newfeedId);
        getStatusDetail.enqueue(new Callback<StatusDetailsResponse>() {
            @Override
            public void onResponse(Call<StatusDetailsResponse> call, Response<StatusDetailsResponse> response) {
                try {
                    if (response.body().result != null && response.code() == 200) {
                        setData(response.body().result);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<StatusDetailsResponse> call, Throwable t) {
                Toast.makeText(StatusDetailsActivity.this, "Không có kết nối mạng!", Toast.LENGTH_SHORT).show();
            }


        });
    }


    public class AddCommentAsyncTask extends AsyncTask<AddCommentRequest, Void, AddCommentResponse> {
        ProgressDialog dialog;
        AddCommentRequest addCommentRequest;
        Context ct;


        public AddCommentAsyncTask(Context ct, AddCommentRequest addCommentRequest) {
            this.ct = ct;
            this.addCommentRequest = addCommentRequest;
        }

        @Override
        protected AddCommentResponse doInBackground(AddCommentRequest... params) {
            try {
                Thread.sleep(1000);
                UserController controller = new UserController();
                return controller.addComment(addCommentRequest);
            } catch (Exception ex) {
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(ct);
            dialog.setMessage("Đang xử lý ...");
            dialog.show();
            dialog.setCancelable(false);
        }

        @Override
        protected void onPostExecute(AddCommentResponse code) {
            try {

                if (code != null) {
                    if (code.code == 200) {
                        adapter.add(0, new CommentModel(0, newfeedId, userModel.id, edtContent.getText().toString().trim(), "", "", userModel.name, userModel.photo, ""));
                        adapter.notifyDataSetChanged();
                        edtContent.setText("");
                        InputMethodManager imm = (InputMethodManager) StatusDetailsActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(edtContent.getWindowToken(), 0);
                        recyclerView.getLayoutManager().scrollToPosition(0);
                    }
                    else
                    {
                        Toast.makeText(StatusDetailsActivity.this, code.message, Toast.LENGTH_LONG).show();
                    }

                }
            } catch (Exception e) {
                Toast.makeText(StatusDetailsActivity.this, "Không có kết nối mạng!", Toast.LENGTH_LONG).show();
            }
            dialog.dismiss();
        }
    }
    public class AddFavouriteAsyncTask extends AsyncTask<AddFavouriteRequest, Void, AddFavouriteResponse> {
        AddFavouriteRequest addFavouriteRequest;
        Context ct;


        public AddFavouriteAsyncTask(Context ct, AddFavouriteRequest addFavouriteRequest) {
            this.ct = ct;
            this.addFavouriteRequest = addFavouriteRequest;
        }

        @Override
        protected AddFavouriteResponse doInBackground(AddFavouriteRequest... params) {
            try {
                Thread.sleep(1000);
                UserController controller = new UserController();
                return controller.addFavourite(addFavouriteRequest);
            } catch (Exception ex) {
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(AddFavouriteResponse code) {
            try {

                if (code != null) {
                    if (code.code == 200) {

                    }
                }
            } catch (Exception e) {
            }

        }
    }

    public class AddShareAsyncTask extends AsyncTask<AddShareRequest, Void, AddShareResponse> {

        AddShareRequest addShareRequest;
        Context ct;


        public AddShareAsyncTask(Context ct, AddShareRequest addShareRequest) {
            this.ct = ct;
            this.addShareRequest = addShareRequest;
        }

        @Override
        protected AddShareResponse doInBackground(AddShareRequest... params) {
            try {
                Thread.sleep(1000);
                UserController controller = new UserController();
                return controller.addShare(addShareRequest);
            } catch (Exception ex) {
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(AddShareResponse code) {
            try {

                if (code != null) {
                    if (code.code == 200) {

                    }
                }
            } catch (Exception e) {
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
    public class DeletePostAsyncTask extends AsyncTask<DeletePostRequest, Void, DeletePostResponse> {

        DeletePostRequest deletePostRequest;
        Context ct;


        public DeletePostAsyncTask(Context ct, DeletePostRequest deletePostRequest) {
            this.ct = ct;
            this.deletePostRequest = deletePostRequest;
        }

        @Override
        protected DeletePostResponse doInBackground(DeletePostRequest... params) {
            try {
                Thread.sleep(0);
                UserController controller = new UserController();
                return controller.deletePost(deletePostRequest);
            } catch (Exception ex) {
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(DeletePostResponse code) {
            try {

                if (code != null) {
                    if (code.code == 200)
                    {
                         Toast.makeText(StatusDetailsActivity.this, "Bài đăng đã bị xóa!",Toast.LENGTH_SHORT).show();
                         finish();
                    }
                }
            } catch (Exception e) {

            }

        }
    }
}
