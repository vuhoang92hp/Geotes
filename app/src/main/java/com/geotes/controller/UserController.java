package com.geotes.controller;

import android.os.StrictMode;

import com.geotes.api.APICommon;
import com.geotes.api.ServiceGenerator;
import com.geotes.request.ActiveAccountRequest;
import com.geotes.request.AddCommentRequest;
import com.geotes.request.AddFavouriteRequest;
import com.geotes.request.AddFeedbackRequest;
import com.geotes.request.AddFollowRequest;
import com.geotes.request.AddShareRequest;
import com.geotes.request.AddStatusRequest;
import com.geotes.request.CourseRegisterRequest;
import com.geotes.request.DeletePostRequest;
import com.geotes.request.LoginFaceRequest;
import com.geotes.request.LoginRequest;
import com.geotes.request.LogoutRequest;
import com.geotes.request.ReadNotificationRequest;
import com.geotes.request.RegisterRequest;
import com.geotes.request.UpdateProfileRequest;
import com.geotes.response.ActiveAccountResponse;
import com.geotes.response.AddCommentResponse;
import com.geotes.response.AddFavouriteResponse;
import com.geotes.response.AddFeedbackResponse;
import com.geotes.response.AddFollowResponse;
import com.geotes.response.AddShareResponse;
import com.geotes.response.AddStatusResponse;
import com.geotes.response.CourseRegisterResponse;
import com.geotes.response.DeletePostResponse;
import com.geotes.response.LoginResponse;
import com.geotes.response.LogoutResponse;
import com.geotes.response.ReadNotificationResponse;
import com.geotes.response.RegisterResponse;
import com.geotes.response.UpdateProfileResponse;

import retrofit2.Call;


/**
 * Created by Administrator on 11/02/2017.
 */

public class UserController {
    APICommon.Geotes service;

    public UserController() {
        service = ServiceGenerator.GetInstance();
    }


    public AddFeedbackResponse addFeedBack(AddFeedbackRequest request) {
        try {
            Call<AddFeedbackResponse> addFeedbackResponseCall = service.addFeedback(request);
            if (android.os.Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy =
                        new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
            AddFeedbackResponse result = addFeedbackResponseCall.execute().body();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public CourseRegisterResponse courseRegister(CourseRegisterRequest request) {
        try {
            Call<CourseRegisterResponse> courseRegisterResponseCall = service.courseRegister(request);
            if (android.os.Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy =
                        new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
            CourseRegisterResponse result = courseRegisterResponseCall.execute().body();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public RegisterResponse registerAccount(RegisterRequest request) {
        try {
            Call<RegisterResponse> registerResponseCall = service.registerAccount(request);
            if (android.os.Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy =
                        new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
            RegisterResponse result = registerResponseCall.execute().body();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public ActiveAccountResponse activeAccount(ActiveAccountRequest request) {
        try {
            Call<ActiveAccountResponse> activeAccountResponseCall = service.activeAccount(request);
            if (android.os.Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy =
                        new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
            ActiveAccountResponse result = activeAccountResponseCall.execute().body();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public LoginResponse login(LoginRequest request) {
        try {
            Call<LoginResponse> loginResponseCall = service.login(request);
            if (android.os.Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy =
                        new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
            LoginResponse result = loginResponseCall.execute().body();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public LoginResponse loginFacebook(LoginFaceRequest request) {
        try {
            Call<LoginResponse> loginResponseCall = service.loginFacebook(request);
            if (android.os.Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy =
                        new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
            LoginResponse result = loginResponseCall.execute().body();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public UpdateProfileResponse updateProfile(UpdateProfileRequest request) {
        try {
            Call<UpdateProfileResponse> updateProfileResponseCall = service.updateProfile(request);
            if (android.os.Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy =
                        new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
            UpdateProfileResponse result = updateProfileResponseCall.execute().body();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public AddCommentResponse addComment(AddCommentRequest request) {
        try {
            Call<AddCommentResponse> addCommentResponseCall = service.addComment(request);
            if (android.os.Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy =
                        new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
            AddCommentResponse result = addCommentResponseCall.execute().body();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public AddStatusResponse addStatus(AddStatusRequest request) {
        try {
            Call<AddStatusResponse> addStatusResponseCall = service.addStatus(request);
            if (android.os.Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy =
                        new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
            AddStatusResponse result = addStatusResponseCall.execute().body();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public ReadNotificationResponse readNotification(ReadNotificationRequest request) {
        try {
            Call<ReadNotificationResponse> readNotificationResponseCall = service.readNotification(request);
            if (android.os.Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy =
                        new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
            ReadNotificationResponse result = readNotificationResponseCall.execute().body();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public AddFollowResponse addFollow(AddFollowRequest request) {
        try {
            Call<AddFollowResponse> addFollowResponseCall = service.addFollow(request);
            if (android.os.Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy =
                        new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
            AddFollowResponse result = addFollowResponseCall.execute().body();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public DeletePostResponse deletePost(DeletePostRequest request) {
        try {
            Call<DeletePostResponse> deletePostResponseCall = service.deletePost(request);
            if (android.os.Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy =
                        new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
            DeletePostResponse result = deletePostResponseCall.execute().body();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public LogoutResponse logOut(LogoutRequest request) {
        try {
            Call<LogoutResponse> logoutResponseCall = service.logOut(request);
            if (android.os.Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy =
                        new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
            LogoutResponse result = logoutResponseCall.execute().body();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public AddFavouriteResponse addFavourite(AddFavouriteRequest request) {
        try {
            Call<AddFavouriteResponse> addFavouriteResponseCall = service.addFavourite(request);
            if (android.os.Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy =
                        new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
            AddFavouriteResponse result = addFavouriteResponseCall.execute().body();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public AddShareResponse addShare(AddShareRequest request) {
        try {
            Call<AddShareResponse> addShareResponseCall = service.addShare(request);
            if (android.os.Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy =
                        new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
            AddShareResponse result = addShareResponseCall.execute().body();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
