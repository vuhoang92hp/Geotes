package com.geotes.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.geotes.R;
import com.geotes.activity.IncludeActivity;
import com.geotes.activity.YoutubeActivity;
import com.geotes.model.DocumentModel;
import com.geotes.model.VideoModel;

import java.util.List;

import static com.geotes.common.CommonValue.CURRENT_FRAGMENT;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_0;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_1;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_2;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_3;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_4;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_5;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_6;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_7;
import static com.geotes.common.CommonValue.FDOCUMENT_TYPE_8;
import static com.geotes.common.CommonValue.FRAGMENT_DOCUMENT_DETAIL;
import static com.geotes.common.CommonValue.FRAGMENT_DOCUMENT_TYPE;

/**
 * Created by a Hoang on 6/29/2017.
 */

public class ListViewDocumentAdapter extends BaseRecyclerAdapter<DocumentModel, ListViewDocumentAdapter.ViewHolder_Document> {


    public ListViewDocumentAdapter(Context context, List<DocumentModel> list) {
        super(context, list);
    }

    @Override
    public void onBindViewHolder(ViewHolder_Document holder, int position) {
        holder.bindData(list.get(position));
        holder.initEvent(list.get(position));
    }

    @Override
    public ViewHolder_Document onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_document, parent, false);
        return new ViewHolder_Document(view);
    }

    public class ViewHolder_Document extends RecyclerView.ViewHolder {

       private TextView txt_title;
       private LinearLayout lay_item;
       private IncludeActivity main;


        public ViewHolder_Document(View itemView) {
            super(itemView);
            txt_title = (TextView) itemView.findViewById(R.id.txt_title);
            lay_item = (LinearLayout) itemView.findViewById(R.id.lay_item);
            main = (IncludeActivity) mContext;
        }

        public void bindData(final DocumentModel object) {

            txt_title.setText(object.name);
        }

        public void initEvent(final DocumentModel object){
            lay_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    CURRENT_FRAGMENT = FRAGMENT_DOCUMENT_DETAIL;
                    if (object.id == 0)
                    {
                        FRAGMENT_DOCUMENT_TYPE = FDOCUMENT_TYPE_0;
                    }
                    else if (object.id == 1)
                    {
                        FRAGMENT_DOCUMENT_TYPE = FDOCUMENT_TYPE_1;
                    }
                    else if (object.id == 2)
                    {
                        FRAGMENT_DOCUMENT_TYPE = FDOCUMENT_TYPE_2;
                    }
                    else if (object.id == 3)
                    {
                        FRAGMENT_DOCUMENT_TYPE = FDOCUMENT_TYPE_3;
                    }
                    else if (object.id == 4)
                    {
                        FRAGMENT_DOCUMENT_TYPE = FDOCUMENT_TYPE_4;
                    }
                    else if (object.id == 5)
                    {
                        FRAGMENT_DOCUMENT_TYPE = FDOCUMENT_TYPE_5;
                    }
                    else if (object.id == 6)
                    {
                        FRAGMENT_DOCUMENT_TYPE = FDOCUMENT_TYPE_6;
                    }
                    else if (object.id == 7)
                    {
                        FRAGMENT_DOCUMENT_TYPE = FDOCUMENT_TYPE_7;
                    }
                    else if (object.id == 8)
                    {
                        FRAGMENT_DOCUMENT_TYPE = FDOCUMENT_TYPE_8;
                    }

                   /* main.addFrm(main.getFragmentDocumentDetail());
                    main.showFrm(main.getFragmentDocument(), main.getFragmentDocumentDetail());*/

                    main.addFrm(main.getFragmentDocumentDetail());


                }
            });



        }



    }
}
