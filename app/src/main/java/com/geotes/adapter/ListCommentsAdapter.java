package com.geotes.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.geotes.R;
import com.geotes.activity.FriendProfileActivity;
import com.geotes.model.CommentModel;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.geotes.common.CommonValue.FRIEND_PROFILE_ID;

/**
 * Created by meobu on 11/18/2017.
 */

public class ListCommentsAdapter extends BaseRecyclerAdapter<CommentModel, ListCommentsAdapter.ViewHolder>{
    public ListCommentsAdapter(Context context, List<CommentModel> list) {
        super(context, list);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bindData(list.get(position));
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.items_comments, parent, false);
        return new ViewHolder(view);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView avataUserComment;
        private TextView txtContentUserComment;
        private TextView nameUserComment;


        public ViewHolder(View itemView) {
            super(itemView);
            avataUserComment =(ImageView) itemView.findViewById(R.id.avataUserComment);
            nameUserComment = (TextView) itemView.findViewById(R.id.nameUserComment);
            txtContentUserComment = (TextView) itemView.findViewById(R.id.txtContentUserComment);
        }

        public void bindData(final CommentModel comments){
            if (comments.user_photo != null) {
                Picasso.with(mContext).load(comments.user_photo).fit().centerCrop().into(avataUserComment, new Callback() {
                    @Override
                    public void onSuccess() {
                        /*progress.setVisibility(View.GONE);*/
                    }

                    @Override
                    public void onError() {
                        /*progress.setVisibility(View.GONE);*/
                        Picasso.with(mContext).load(R.mipmap.ic_no_avarta).fit().centerCrop().into(avataUserComment);
                    }
                });
            }
            nameUserComment.setText(comments.getUser_name());
            txtContentUserComment.setText(comments.getContent());

            nameUserComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FRIEND_PROFILE_ID = comments.user_id;
                    Intent i = new Intent(mContext, FriendProfileActivity.class);
                    mContext.startActivity(i);
                }
            });
        }
    }
}
