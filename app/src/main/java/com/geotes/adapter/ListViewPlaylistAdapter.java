package com.geotes.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.geotes.R;
import com.geotes.activity.IncludeActivity;
import com.geotes.common.PublicMethob;
import com.geotes.model.PlaylistModel;


import java.security.PublicKey;
import java.util.List;

import static com.geotes.common.CommonValue.CURRENT_FRAGMENT;
import static com.geotes.common.CommonValue.FRAGMENT_PLAYLIST_DETAIL;
import static com.geotes.common.CommonValue.PLAYLIST_ID;

/**
 * Created by a Hoang on 6/29/2017.
 */

public class ListViewPlaylistAdapter extends BaseRecyclerAdapter<PlaylistModel, ListViewPlaylistAdapter.ViewHolder_Playlist> {


    public ListViewPlaylistAdapter(Context context, List<PlaylistModel> list) {
        super(context, list);
    }

    @Override
    public void onBindViewHolder(ViewHolder_Playlist holder, int position) {
        holder.bindData(list.get(position));
        holder.initEvent(list.get(position));
    }

    @Override
    public ViewHolder_Playlist onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_playlist, parent, false);
        return new ViewHolder_Playlist(view);
    }

    public class ViewHolder_Playlist extends RecyclerView.ViewHolder {
       private ImageView img_photo_playlist;
       private TextView txt_description, txt_quantity;
       private LinearLayout lay_item;
       private IncludeActivity main;

        public ViewHolder_Playlist(View itemView) {
            super(itemView);
            img_photo_playlist = (ImageView) itemView.findViewById(R.id.img_photo_playlist);
            txt_description = (TextView) itemView.findViewById(R.id.txt_description);
            txt_quantity = (TextView) itemView.findViewById(R.id.txt_quantity);
            lay_item = (LinearLayout) itemView.findViewById(R.id.lay_item);
            main = (IncludeActivity) mContext;
        }

        public void bindData(final PlaylistModel object) {
            PublicMethob.showImageNomal(mContext, object.photo, img_photo_playlist);
            //img_photo_playlist.setImageResource(R.mipmap.ic_youtube_cover);
            txt_quantity.setText(object.video_number+"");
            txt_description.setText(object.title);
        }

        public void initEvent(final PlaylistModel object){
            lay_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PLAYLIST_ID = object.id;
                    CURRENT_FRAGMENT = FRAGMENT_PLAYLIST_DETAIL;
                    main.addFrm(main.getFragmentVideo());

                    //main.showFrm(main.getFragmentPlaylist(), main.getFragmentVideo());
                }
            });



        }



    }
}
