package com.geotes.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.geotes.R;
import com.geotes.activity.MainActivity;
import com.geotes.activity.YoutubeActivity;
import com.geotes.common.PublicMethob;
import com.geotes.model.PlaylistModel;
import com.geotes.model.VideoModel;

import java.util.List;

import static com.geotes.common.CommonValue.VIDEO_DETAIL_ID;
import static com.geotes.common.CommonValue.VIDEO_LINK;

/**
 * Created by a Hoang on 6/29/2017.
 */

public class ListViewVideoAdapter extends BaseRecyclerAdapter<VideoModel, ListViewVideoAdapter.ViewHolder_Video> {


    public ListViewVideoAdapter(Context context, List<VideoModel> list) {
        super(context, list);
    }

    @Override
    public void onBindViewHolder(ViewHolder_Video holder, int position) {
        holder.bindData(list.get(position));
        holder.initEvent(list.get(position));
    }

    @Override
    public ViewHolder_Video onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_youtube, parent, false);
        return new ViewHolder_Video(view);
    }

    public class ViewHolder_Video extends RecyclerView.ViewHolder {
       private ImageView img_photo;
       private TextView txt_duration, txt_title,txt_chanel;
       private LinearLayout lay_item;

        public ViewHolder_Video(View itemView) {
            super(itemView);
            img_photo = (ImageView) itemView.findViewById(R.id.img_photo);
            txt_duration = (TextView) itemView.findViewById(R.id.txt_duration);
            txt_title = (TextView) itemView.findViewById(R.id.txt_title);
            txt_chanel = (TextView) itemView.findViewById(R.id.txt_chanel);
            lay_item = (LinearLayout) itemView.findViewById(R.id.lay_item);
        }

        public void bindData(final VideoModel object) {
            PublicMethob.showImageNomal(mContext, object.photo, img_photo);
            //img_photo.setImageResource(R.mipmap.ic_youtube_cover);
            txt_duration.setText(object.video_time);
            txt_title.setText(object.title);
            txt_chanel.setText(object.video_channel);
        }

        public void initEvent(final VideoModel object){
            lay_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    VIDEO_DETAIL_ID = object.id;
                    VIDEO_LINK = object.video_link.substring(object.video_link.lastIndexOf("/")+1);
                    Intent i5 = new Intent(mContext, YoutubeActivity.class);
                    mContext.startActivity(i5);
                }
            });



        }



    }
}
