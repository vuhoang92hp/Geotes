package com.geotes.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;


import com.geotes.R;
import com.geotes.common.PublicMethob;
import com.rey.material.widget.ProgressView;

import java.util.ArrayList;

public class CustomImageViewPager extends PagerAdapter {
    Context context;
    ArrayList<String> images;
    LayoutInflater layoutInflater;


    public CustomImageViewPager(Context context, ArrayList<String> images) {
        this.context = context;
        this.images = images;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
 
    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
 
    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        final View itemView = layoutInflater.inflate(R.layout.frm_image_advertisement, container, false);
        ProgressView progressView = (ProgressView) itemView.findViewById(R.id.progress_bar);
        final ImageView imageView = (ImageView) itemView.findViewById(R.id.iv_photo_advertisement);
        PublicMethob.showImageNomal(context, images.get(position), imageView);
        //imageView.setImageResource(R.mipmap.banner_2);
        progressView.setVisibility(View.INVISIBLE);
        container.addView(itemView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return itemView;
    }
 
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}