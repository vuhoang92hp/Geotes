package com.geotes.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.geotes.R;
import com.geotes.activity.FriendProfileActivity;
import com.geotes.activity.MainActivity;
import com.geotes.activity.StatusDetailsActivity;
import com.geotes.common.CommonValue;
import com.geotes.common.Config;
import com.geotes.controller.UserController;
import com.geotes.model.NotificationModel;
import com.geotes.model.UserModel;
import com.geotes.request.AddStatusRequest;
import com.geotes.request.ReadNotificationRequest;
import com.geotes.response.AddStatusResponse;
import com.geotes.response.ReadNotificationResponse;
import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static com.geotes.common.CommonValue.FRIEND_PROFILE_ID;
import static com.geotes.common.CommonValue.KEY_RELOAD;

public class ListViewNotificationAdapter extends BaseRecyclerAdapter<NotificationModel, ListViewNotificationAdapter.ViewHolder_Notification> {
    private UserModel userModel;
    private MainActivity main;

    public ListViewNotificationAdapter(Context context, List<NotificationModel> list) {
        super(context, list);

    }

    @Override
    public void onBindViewHolder(ViewHolder_Notification holder, int position) {
        main = (MainActivity) mContext;
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(Config.Pref, mContext.MODE_PRIVATE);
        String profile = sharedPreferences.getString(Config.KEY_USER, "");
        Gson gson = new Gson();
        userModel = gson.fromJson(profile, UserModel.class);

        holder.bindData(list.get(position),position);
        holder.initEvent(list.get(position),position);
    }

    @Override
    public ViewHolder_Notification onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_notification, parent, false);
        return new ViewHolder_Notification(view);
    }

    public class ViewHolder_Notification extends RecyclerView.ViewHolder {
        private ImageView img_avarta;
        private TextView txt_title,txt_time;
        private LinearLayout lay_item;


        public ViewHolder_Notification(View itemView) {
            super(itemView);
            img_avarta = (ImageView) itemView.findViewById(R.id.img_avarta);
            txt_title =(TextView) itemView.findViewById(R.id.txt_title);
            txt_time =(TextView) itemView.findViewById(R.id.txt_time);
            lay_item = (LinearLayout) itemView.findViewById(R.id.lay_item);
        }

        public void bindData(final NotificationModel object, final int position) {


            //== avarta
            if (object.user_created_photo != null && !object.user_created_photo.equals("")) {
                Picasso.with(mContext).load(object.user_created_photo).fit().centerCrop().into(img_avarta, new Callback() {
                    @Override
                    public void onSuccess() {
                        /*progress.setVisibility(View.GONE);*/
                    }

                    @Override
                    public void onError() {
                        /*progress.setVisibility(View.GONE);*/
                        Picasso.with(mContext).load(R.mipmap.ic_no_image).fit().centerCrop().into(img_avarta);
                    }
                });
            } else {
              /*  progress.setVisibility(View.GONE);*/
                Picasso.with(mContext).load(R.mipmap.ic_no_image).fit().centerCrop().into(img_avarta);
            }

            //== content
            txt_title.setText(object.content);

            //== date
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date dt = sdf.parse(object.created_at);

                SimpleDateFormat sdfs=new SimpleDateFormat("dd/MM/yyyy");
                String goal=sdfs.format(dt.getTime());
                txt_time.setText(goal);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            //== is read
            if (object.is_read == 0)
            {
                lay_item.setBackgroundResource(R.color.colorGreen_20);
            }
            else
            {
                lay_item.setBackgroundResource(R.color.colorTransparent);
            }



        }

        public void initEvent(final NotificationModel object, final int position)
        {
            lay_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (object.is_read==0)
                    {
                        lay_item.setBackgroundResource(R.color.colorTransparent);
                        ReadNotificationAsyncTask readNotificationAsyncTask = new ReadNotificationAsyncTask(mContext, new ReadNotificationRequest(userModel.id, userModel.ApiToken, object.id));
                        readNotificationAsyncTask.execute();

                    }

                    if (object.new_feed_id !=0)
                    {
                        Intent intent = new Intent(mContext, StatusDetailsActivity.class);
                        intent.putExtra(CommonValue.KEY_NEWFEED_ID, object.new_feed_id);
                        main.startActivityForResult(intent, KEY_RELOAD);
                    }
                    else if (object.user_created_id != 0)
                    {
                        FRIEND_PROFILE_ID = object.user_created_id;
                        Intent i = new Intent(mContext, FriendProfileActivity.class);
                        main.startActivityForResult(i,KEY_RELOAD );
                    }




                    //do something
                }
            });

        }
    }
    public class ReadNotificationAsyncTask extends AsyncTask<ReadNotificationRequest, Void, ReadNotificationResponse> {

        ReadNotificationRequest readNotificationRequest;
        Context ct;


        public ReadNotificationAsyncTask(Context ct, ReadNotificationRequest readNotificationRequest) {
            this.ct = ct;
            this.readNotificationRequest = readNotificationRequest;
        }

        @Override
        protected ReadNotificationResponse doInBackground(ReadNotificationRequest... params) {
            try {
                Thread.sleep(0);
                UserController controller = new UserController();
                return controller.readNotification(readNotificationRequest);
            } catch (Exception ex) {
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(ReadNotificationResponse code) {
            try {

                if (code != null) {
                    if (code.code == 200)
                    {
                       // Toast.makeText(mContext, code.message,Toast.LENGTH_SHORT).show();
                    }
                }
            } catch (Exception e) {

            }

        }
    }
}
