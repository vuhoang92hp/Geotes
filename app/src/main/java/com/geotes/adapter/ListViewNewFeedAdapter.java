package com.geotes.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.geotes.R;
import com.geotes.activity.FriendProfileActivity;
import com.geotes.activity.GalleryImageActivity;
import com.geotes.activity.MainActivity;
import com.geotes.activity.StartActivity;
import com.geotes.activity.StatusDetailsActivity;
import com.geotes.common.CommonValue;
import com.geotes.common.Config;
import com.geotes.common.PublicMethob;
import com.geotes.controller.UserController;
import com.geotes.fragment.fragment_main.tab_profile.FragmentProfileFavourite;
import com.geotes.fragment.fragment_main.FragmentHome;
import com.geotes.fragment.fragment_main.tab_profile.FragmentProfileNewFeed;
import com.geotes.model.NewFeedModel;
import com.geotes.model.UserModel;
import com.geotes.request.AddFavouriteRequest;
import com.geotes.request.AddShareRequest;
import com.geotes.response.AddFavouriteResponse;
import com.geotes.response.AddShareResponse;
import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static com.geotes.common.CommonValue.FRIEND_PROFILE_ID;
import static com.geotes.common.CommonValue.KEY_LOGOUT;
import static com.geotes.common.CommonValue.KEY_RELOAD;

public class ListViewNewFeedAdapter extends BaseRecyclerAdapter<NewFeedModel, ListViewNewFeedAdapter.ViewHolder_NewFeed> {
    private MainActivity main;
    private UserModel userModel;

    public ListViewNewFeedAdapter(Context context, List<NewFeedModel> list) {
        super(context, list);

    }

    @Override
    public void onBindViewHolder(ViewHolder_NewFeed holder, int position) {
        main = (MainActivity) mContext;
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(Config.Pref, mContext.MODE_PRIVATE);
        String profile = sharedPreferences.getString(Config.KEY_USER, "");
        Gson gson = new Gson();
        userModel = gson.fromJson(profile, UserModel.class);

        holder.bindData(list.get(position),position);
        holder.initEvent(list.get(position),position);
    }

    @Override
    public ViewHolder_NewFeed onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_newfeed, parent, false);
        return new ViewHolder_NewFeed(view);
    }

    public class ViewHolder_NewFeed extends RecyclerView.ViewHolder {
        private ImageView img_detail_avarta,avataUserComment;
        private TextView txtNamePost,nameUserComment;
        private TextView txt_content_newfeed,txtContentUserComment,txtTimePost,txtMore;
        private TextView number_like,tv_number_comment,tv_number_share,txtNumberPhotoMore,txtNumberPhotoMoreHoz;
        private ImageView imgPhotoTwo,imgPhotoOne,imgPhotoThree,imgPhotoMore;
        private ImageView imgPhotoOneHoz,imgPhotoTwoHoz,imgPhotoThreeHoz,imgPhotoMoreHoz;
        private ImageView imgPhotoOneVer2,imgPhotoTwoVer2;
        private ImageView imgPhotoOneHoz2,imgPhotoTwoHoz2,iv_is_like;
        public boolean isCheck=true;
        public LinearLayout layout4Ver,layout4Hoz,layout2Hoz,layout2Ver, layoutReviewComments,lay_review_class_care,lay_include_comment,ll_class_share;
        public RelativeLayout layoutMorePhoto,layoutMoreTextHoz,layoutMoreText;
        public int number_share = 0, quantity_like = 0;

        public ViewHolder_NewFeed(View itemView) {
            super(itemView);
            iv_is_like = (ImageView) itemView.findViewById(R.id.iv_is_like);
            img_detail_avarta =(ImageView) itemView.findViewById(R.id.img_detail_avarta);
            avataUserComment =(ImageView) itemView.findViewById(R.id.avataUserComment);
            imgPhotoTwo =(ImageView) itemView.findViewById(R.id.imgPhotoTwo);
            imgPhotoOne =(ImageView) itemView.findViewById(R.id.imgPhotoOne);
            imgPhotoThree =(ImageView) itemView.findViewById(R.id.imgPhotoThree);
            imgPhotoMore =(ImageView) itemView.findViewById(R.id.imgPhotoMore);
            txtNamePost = (TextView) itemView.findViewById(R.id.txtNamePost);
            txtTimePost = (TextView) itemView.findViewById(R.id.txtTimePost);
            nameUserComment = (TextView) itemView.findViewById(R.id.nameUserComment);
            txt_content_newfeed = (TextView) itemView.findViewById(R.id.txt_content_newfeed);
            txtContentUserComment = (TextView) itemView.findViewById(R.id.txtContentUserComment);
            number_like = (TextView) itemView.findViewById(R.id.number_like);
            txtMore = (TextView) itemView.findViewById(R.id.txtMore);
            Paint paint = new Paint();
            paint.setColor(mContext.getResources().getColor(R.color.colorBlueFacebook));
            txtMore.setPaintFlags(paint.UNDERLINE_TEXT_FLAG);
            tv_number_comment = (TextView) itemView.findViewById(R.id.tv_number_comment);
            tv_number_share = (TextView) itemView.findViewById(R.id.tv_number_share);
            txtNumberPhotoMore = (TextView) itemView.findViewById(R.id.txtNumberPhotoMore);
            txtNumberPhotoMoreHoz = (TextView) itemView.findViewById(R.id.txtNumberPhotoMoreHoz);
            lay_review_class_care = (LinearLayout) itemView.findViewById(R.id.lay_review_class_care);
            layout4Ver = (LinearLayout) itemView.findViewById(R.id.layout4Ver);
            layout4Hoz = (LinearLayout) itemView.findViewById(R.id.layout4Hoz);
            layout2Hoz = (LinearLayout) itemView.findViewById(R.id.layout2Hoz);
            layout2Ver = (LinearLayout) itemView.findViewById(R.id.layout2Ver);
            layoutMorePhoto = (RelativeLayout) itemView.findViewById(R.id.layoutMorePhoto);
            layoutMoreTextHoz = (RelativeLayout) itemView.findViewById(R.id.layoutMoreTextHoz);
            layoutMoreText = (RelativeLayout) itemView.findViewById(R.id.layoutMoreText);
            layoutReviewComments = (LinearLayout) itemView.findViewById(R.id.lay_review_class_comment);
            lay_include_comment = (LinearLayout) itemView.findViewById(R.id.lay_include_comment);
            ll_class_share = (LinearLayout) itemView.findViewById(R.id.ll_class_share);
            imgPhotoOneHoz =(ImageView) itemView.findViewById(R.id.imgPhotoOneHoz);
            imgPhotoTwoHoz =(ImageView) itemView.findViewById(R.id.imgPhotoTwoHoz);
            imgPhotoThreeHoz =(ImageView) itemView.findViewById(R.id.imgPhotoThreeHoz);
            imgPhotoMoreHoz =(ImageView) itemView.findViewById(R.id.imgPhotoMoreHoz);

            imgPhotoOneVer2 =(ImageView) itemView.findViewById(R.id.imgPhotoOneVer2);
            imgPhotoTwoVer2 =(ImageView) itemView.findViewById(R.id.imgPhotoTwoVer2);

            imgPhotoOneHoz2 =(ImageView) itemView.findViewById(R.id.imgPhotoOneHoz2);
            imgPhotoTwoHoz2 =(ImageView) itemView.findViewById(R.id.imgPhotoTwoHoz2);

        }

        public void bindData(final NewFeedModel object, final int position) {

            quantity_like = object.number_like;
            number_share = object.number_share;

            if (object.photoUser != null && !object.photoUser.equals("")) {
                Picasso.with(mContext).load(object.photoUser).fit().centerCrop().into(img_detail_avarta, new Callback() {
                    @Override
                    public void onSuccess() {
                        /*progress.setVisibility(View.GONE);*/
                    }

                    @Override
                    public void onError() {
                        /*progress.setVisibility(View.GONE);*/
                        Picasso.with(mContext).load(R.mipmap.ic_no_avarta).fit().centerCrop().into(img_detail_avarta);
                    }
                });
            } else {
              /*  progress.setVisibility(View.GONE);*/
                Picasso.with(mContext).load(R.mipmap.ic_no_avarta).fit().centerCrop().into(img_detail_avarta);
            }
            txtNamePost.setText(object.nameUser);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date dt = sdf.parse(object.created_at);

                SimpleDateFormat sdfs=new SimpleDateFormat("dd/MM/yyyy");
                String goal=sdfs.format(dt.getTime());
                txtTimePost.setText("Đăng ngày "+goal);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            //txt_content_newfeed.setText(object.content);
            txt_content_newfeed.setText(Html.fromHtml(object.content));

            txtMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isCheck) {
                        txt_content_newfeed.setMaxLines(100);
                        isCheck = false;
                        txtMore.setText("Thu gọn");
                    } else {
                        txt_content_newfeed.setMaxLines(3);
                        isCheck = true;
                        txtMore.setText("Xem thêm");
                    }


                }
            });




            txt_content_newfeed.post(new Runnable() {
                @Override
                public void run() {
                    if(txt_content_newfeed.getLineCount()<=3){
                        txtMore.setVisibility(View.GONE);

                    }else {
                        txtMore.setVisibility(View.VISIBLE);
                        txt_content_newfeed.setMaxLines(3);
                    }

                }
            });

            number_like.setText(object.number_like+" Quan tâm");
            tv_number_comment.setText(object.number_comment+"");
            tv_number_share.setText(object.number_share+"");
            try {
                if (object.comments.get(0).user_photo != null) {
                    Picasso.with(mContext).load(object.comments.get(0).user_photo).fit().centerCrop().into(avataUserComment, new Callback() {
                        @Override
                        public void onSuccess() {
                        /*progress.setVisibility(View.GONE);*/
                        }

                        @Override
                        public void onError() {
                        /*progress.setVisibility(View.GONE);*/
                            Picasso.with(mContext).load(R.mipmap.ic_no_avarta).fit().centerCrop().into(avataUserComment);
                        }
                    });
                } else {
              /*  progress.setVisibility(View.GONE);*/
                    Picasso.with(mContext).load(R.mipmap.ic_no_avarta).fit().centerCrop().into(avataUserComment);
                }
                nameUserComment.setText(object.comments.get(0).user_name);
                txtContentUserComment.setText(object.comments.get(0).content);

            }catch (Exception err){
                Picasso.with(mContext).load(R.mipmap.ic_no_avarta).fit().centerCrop().into(avataUserComment);
            }

            final String []lstPhoto = object.photo.split(",");


            if(lstPhoto.length>0){
                layout4Ver.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        List<String> list = Arrays.asList(lstPhoto);
                        Intent intent = new Intent(mContext, GalleryImageActivity.class);
                        GalleryImageActivity.imagePhoto=list;
                        mContext.startActivity(intent);
                    }
                });
                layout4Hoz.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        List<String> list = Arrays.asList(lstPhoto);
                        Intent intent = new Intent(mContext, GalleryImageActivity.class);
                        GalleryImageActivity.imagePhoto=list;
                        mContext.startActivity(intent);
                    }
                });
                layout2Ver.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        List<String> list = Arrays.asList(lstPhoto);
                        Intent intent = new Intent(mContext, GalleryImageActivity.class);
                        GalleryImageActivity.imagePhoto=list;
                        mContext.startActivity(intent);
                    }
                });
                layout2Hoz.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        List<String> list = Arrays.asList(lstPhoto);
                        Intent intent = new Intent(mContext, GalleryImageActivity.class);
                        GalleryImageActivity.imagePhoto=list;
                        mContext.startActivity(intent);
                    }
                });

                if(object.type_photo==1){/*Horizontal*/
                    if(lstPhoto.length>=4){
                        layout4Ver.setVisibility(View.VISIBLE);
                        layout4Hoz.setVisibility(View.GONE);
                        layout2Hoz.setVisibility(View.GONE);
                        layout2Ver.setVisibility(View.GONE);
                        Picasso.with(mContext).load(lstPhoto[0]).fit().centerCrop().into(imgPhotoOne);
                        Picasso.with(mContext).load(lstPhoto[1]).fit().centerCrop().into(imgPhotoTwo);
                        Picasso.with(mContext).load(lstPhoto[2]).fit().centerCrop().into(imgPhotoThree);
                        Picasso.with(mContext).load(lstPhoto[3]).fit().centerCrop().into(imgPhotoMore);
                        if(lstPhoto.length==4){
                            layoutMoreText.setVisibility(View.GONE);
                        }else {
                            layoutMoreText.setVisibility(View.VISIBLE);
                            txtNumberPhotoMore.setText("+"+(lstPhoto.length-4));
                        }
                    }else if(lstPhoto.length==3){
                        layout4Ver.setVisibility(View.VISIBLE);
                        layout4Hoz.setVisibility(View.GONE);
                        layout2Hoz.setVisibility(View.GONE);
                        layout2Ver.setVisibility(View.GONE);
                        Picasso.with(mContext).load(lstPhoto[0]).fit().centerCrop().into(imgPhotoOne);
                        Picasso.with(mContext).load(lstPhoto[1]).fit().centerCrop().into(imgPhotoTwo);
                        Picasso.with(mContext).load(lstPhoto[2]).fit().centerCrop().into(imgPhotoThree);
                        layoutMorePhoto.setVisibility(View.GONE);
                    }else if(lstPhoto.length==2){
                        layout4Ver.setVisibility(View.GONE);
                        layout4Hoz.setVisibility(View.GONE);
                        layout2Hoz.setVisibility(View.GONE);
                        layout2Ver.setVisibility(View.VISIBLE);

                        Picasso.with(mContext).load(lstPhoto[0]).fit().centerCrop().into(imgPhotoOneVer2);
                        Picasso.with(mContext).load(lstPhoto[1]).fit().centerCrop().into(imgPhotoTwoVer2);
                    }else if(lstPhoto.length==1){
                        layout4Ver.setVisibility(View.GONE);
                        layout4Hoz.setVisibility(View.GONE);
                        layout2Hoz.setVisibility(View.GONE);
                        layout2Ver.setVisibility(View.VISIBLE);

                        Picasso.with(mContext).load(lstPhoto[0]).fit().centerCrop().into(imgPhotoOneVer2);
                        imgPhotoTwoVer2.setVisibility(View.GONE);
                    }
                }else {/*Vertical*/
                    if(lstPhoto.length>=4){
                        layout4Ver.setVisibility(View.GONE);
                        layout4Hoz.setVisibility(View.VISIBLE);
                        layout2Hoz.setVisibility(View.GONE);
                        layout2Ver.setVisibility(View.GONE);

                        Picasso.with(mContext).load(lstPhoto[0]).fit().centerCrop().into(imgPhotoOneHoz);
                        Picasso.with(mContext).load(lstPhoto[1]).fit().centerCrop().into(imgPhotoTwoHoz);
                        Picasso.with(mContext).load(lstPhoto[2]).fit().centerCrop().into(imgPhotoThreeHoz);
                        Picasso.with(mContext).load(lstPhoto[3]).fit().centerCrop().into(imgPhotoMoreHoz);

                        if(lstPhoto.length==4){
                            layoutMoreTextHoz.setVisibility(View.GONE);
                        }else {
                            layoutMoreTextHoz.setVisibility(View.VISIBLE);
                            txtNumberPhotoMore.setText("+"+(lstPhoto.length-4));
                        }
                    }else if(lstPhoto.length==3){
                        layout4Ver.setVisibility(View.GONE);
                        layout4Hoz.setVisibility(View.VISIBLE);
                        layout2Hoz.setVisibility(View.GONE);
                        layout2Ver.setVisibility(View.GONE);

                        Picasso.with(mContext).load(lstPhoto[0]).fit().centerCrop().into(imgPhotoOneHoz);
                        Picasso.with(mContext).load(lstPhoto[1]).fit().centerCrop().into(imgPhotoTwoHoz);
                        Picasso.with(mContext).load(lstPhoto[2]).fit().centerCrop().into(imgPhotoThreeHoz);
                        layoutMorePhoto.setVisibility(View.GONE);
                    }else if(lstPhoto.length==2){
                        layout4Ver.setVisibility(View.GONE);
                        layout4Hoz.setVisibility(View.GONE);
                        layout2Hoz.setVisibility(View.VISIBLE);
                        layout2Ver.setVisibility(View.GONE);

                        Picasso.with(mContext).load(lstPhoto[0]).fit().centerCrop().into(imgPhotoOneHoz2);
                        Picasso.with(mContext).load(lstPhoto[1]).fit().centerCrop().into(imgPhotoTwoHoz2);
                    }else if(lstPhoto.length==1){
                        layout4Ver.setVisibility(View.GONE);
                        layout4Hoz.setVisibility(View.GONE);
                        layout2Hoz.setVisibility(View.VISIBLE);
                        layout2Ver.setVisibility(View.GONE);

                        Picasso.with(mContext).load(lstPhoto[0]).fit().centerCrop().into(imgPhotoOneHoz2);
                        imgPhotoTwoHoz2.setVisibility(View.GONE);
                    }
                }

                layoutReviewComments.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (PublicMethob.isOnline(mContext))
                        {
                            if (userModel!=null)
                            {
                                Intent intent = new Intent(main, StatusDetailsActivity.class);
                                intent.putExtra(CommonValue.KEY_NEWFEED_ID, object.id);
                                main.startActivityForResult(intent,KEY_RELOAD);
                            }
                            else
                            {
                                KEY_LOGOUT = "re_login";
                                Intent i = new Intent(mContext, StartActivity.class);
                                mContext.startActivity(i);
                                main.finish();
                            }
                        }
                    }
                });

                txt_content_newfeed.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (PublicMethob.isOnline(mContext))
                        {
                            if (userModel!=null)
                            {
                                Intent intent = new Intent(main, StatusDetailsActivity.class);
                                intent.putExtra(CommonValue.KEY_NEWFEED_ID, object.id);
                                main.startActivityForResult(intent,KEY_RELOAD);
                            }
                            else
                            {
                                KEY_LOGOUT = "re_login";
                                Intent i = new Intent(mContext, StartActivity.class);
                                mContext.startActivity(i);
                                main.finish();
                            }
                        }
                    }
                });
            }

            if (object.comments.size()==0)
            {
                lay_include_comment.setVisibility(View.GONE);
            } else
            {
                lay_include_comment.setVisibility(View.VISIBLE);
            }

            if (object.is_liked == 0)
            {
                iv_is_like.setImageResource(R.mipmap.ic_favorite_grey);
            }
            else
            {
                iv_is_like.setImageResource(R.mipmap.ic_heart_active);
            }
        }

        public void initEvent(final NewFeedModel object, final int position)
        {

            txtNamePost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FRIEND_PROFILE_ID = object.user_id;
                    if (userModel!=null)
                    {
                        if (userModel.id == FRIEND_PROFILE_ID)
                        {
                            main.setTabSelected(5);
                            main.showFragment(main.getFragmentTemp(),main.getFragmentProfile());
                        }
                        else
                        {
                            Intent i = new Intent(mContext, FriendProfileActivity.class);
                            mContext.startActivity(i);
                        }
                    }
                    else
                    {
                        Intent i = new Intent(mContext, FriendProfileActivity.class);
                        mContext.startActivity(i);
                    }


                }
            });
            nameUserComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FRIEND_PROFILE_ID = object.comments.get(0).user_id;

                    if (userModel!=null)
                    {
                        if (userModel.id == FRIEND_PROFILE_ID)
                        {
                            main.setTabSelected(5);
                            main.showFragment(main.getFragmentTemp(),main.getFragmentProfile());
                        }
                        else
                        {
                            Intent i = new Intent(mContext, FriendProfileActivity.class);
                            mContext.startActivity(i);
                        }
                    }
                    else
                    {
                        Intent i = new Intent(mContext, FriendProfileActivity.class);
                        mContext.startActivity(i);
                    }

                }
            });
            lay_review_class_care.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (userModel!=null)
                    {
                        if (object.is_liked == 0)
                        {
                            iv_is_like.setImageResource(R.mipmap.ic_heart_active);
                            object.setIs_liked(1);
                            quantity_like = quantity_like+1;
                            object.setNumber_like(quantity_like);
                            FragmentProfileFavourite.arr_favourite.add(object);
                            FragmentProfileFavourite.adapter_list_new_feed.notifyDataSetChanged();
                            FragmentHome.adapter_list_new_feed.notifyDataSetChanged();

                        }
                        else
                        {
                            iv_is_like.setImageResource(R.mipmap.ic_favorite_grey);
                            object.setIs_liked(0);
                            quantity_like = quantity_like-1;
                            object.setNumber_like(quantity_like);
                            FragmentProfileFavourite.arr_favourite.remove(object);
                            FragmentProfileFavourite.adapter_list_new_feed.notifyDataSetChanged();
                            FragmentHome.adapter_list_new_feed.notifyDataSetChanged();
                        }
                        AddFavouriteAsyncTask addFavouriteAsyncTask = new AddFavouriteAsyncTask(mContext, new AddFavouriteRequest(userModel.id, object.id));
                        addFavouriteAsyncTask.execute();
                    }
                    else {
                        KEY_LOGOUT = "re_login";
                        Intent i = new Intent(mContext, StartActivity.class);
                        mContext.startActivity(i);
                        main.finish();
                    }

                }
            });

            ll_class_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (userModel!=null)
                    {
                        number_share = number_share+1;
                        object.setNumber_share(number_share);
                        tv_number_share.setText(number_share+"");

                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT,"https://play.google.com/store/apps/details?id=com.geotes&hl=vi" );
                        sendIntent.setType("text/plain");
                        mContext.startActivity(sendIntent);

                        AddShareAsyncTask addShareAsyncTask = new AddShareAsyncTask(mContext, new AddShareRequest(userModel.id,object.id));
                        addShareAsyncTask.execute();
                    }
                    else
                    {
                        KEY_LOGOUT = "re_login";
                        Intent i = new Intent(mContext, StartActivity.class);
                        mContext.startActivity(i);
                        main.finish();
                    }
                }
            });
        }
    }

    public class AddFavouriteAsyncTask extends AsyncTask<AddFavouriteRequest, Void, AddFavouriteResponse> {
        AddFavouriteRequest addFavouriteRequest;
        Context ct;


        public AddFavouriteAsyncTask(Context ct, AddFavouriteRequest addFavouriteRequest) {
            this.ct = ct;
            this.addFavouriteRequest = addFavouriteRequest;
        }

        @Override
        protected AddFavouriteResponse doInBackground(AddFavouriteRequest... params) {
            try {
                Thread.sleep(0);
                UserController controller = new UserController();
                return controller.addFavourite(addFavouriteRequest);
            } catch (Exception ex) {
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(AddFavouriteResponse code) {
            try {

                if (code != null) {
                    if (code.code == 200) {

                    }
                }
            } catch (Exception e) {
            }

        }
    }

    public class AddShareAsyncTask extends AsyncTask<AddShareRequest, Void, AddShareResponse> {

        AddShareRequest addShareRequest;
        Context ct;


        public AddShareAsyncTask(Context ct, AddShareRequest addShareRequest) {
            this.ct = ct;
            this.addShareRequest = addShareRequest;
        }

        @Override
        protected AddShareResponse doInBackground(AddShareRequest... params) {
            try {
                Thread.sleep(1000);
                UserController controller = new UserController();
                return controller.addShare(addShareRequest);
            } catch (Exception ex) {
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(AddShareResponse code) {
            try {

                if (code != null) {
                    if (code.code == 200) {

                    }
                }
            } catch (Exception e) {
            }
        }
    }
}
