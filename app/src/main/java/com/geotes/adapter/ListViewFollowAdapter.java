package com.geotes.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.geotes.R;
import com.geotes.activity.FriendProfileActivity;
import com.geotes.common.Config;
import com.geotes.controller.UserController;
import com.geotes.model.FollowModel;
import com.geotes.model.NotificationModel;
import com.geotes.model.UserModel;
import com.geotes.request.AddFollowRequest;
import com.geotes.request.ReadNotificationRequest;
import com.geotes.response.AddFollowResponse;
import com.geotes.response.ReadNotificationResponse;
import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ListViewFollowAdapter extends BaseRecyclerAdapter<FollowModel, ListViewFollowAdapter.ViewHolder_Follow> {
    private UserModel userModel;

    public ListViewFollowAdapter(Context context, List<FollowModel> list) {
        super(context, list);

    }

    @Override
    public void onBindViewHolder(ViewHolder_Follow holder, int position) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(Config.Pref, mContext.MODE_PRIVATE);
        String profile = sharedPreferences.getString(Config.KEY_USER, "");
        Gson gson = new Gson();
        userModel = gson.fromJson(profile, UserModel.class);

        holder.bindData(list.get(position),position);
        holder.initEvent(list.get(position),position);
    }

    @Override
    public ViewHolder_Follow onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_follow, parent, false);
        return new ViewHolder_Follow(view);
    }

    public class ViewHolder_Follow extends RecyclerView.ViewHolder {
        private ImageView img_avarta;
        private TextView txt_name;
        private LinearLayout lay_unfollow, lay_profile;


        public ViewHolder_Follow(View itemView) {
            super(itemView);
            img_avarta = (ImageView) itemView.findViewById(R.id.img_avarta);
            txt_name =(TextView) itemView.findViewById(R.id.txt_name);
            lay_unfollow = (LinearLayout) itemView.findViewById(R.id.lay_unfollow);
            lay_profile = (LinearLayout)itemView.findViewById(R.id.lay_profile);
        }

        public void bindData(final FollowModel object, final int position) {


            //== avarta
            if (object.photo != null && !object.photo.equals("")) {
                Picasso.with(mContext).load(object.photo).fit().centerCrop().into(img_avarta, new Callback() {
                    @Override
                    public void onSuccess() {
                        /*progress.setVisibility(View.GONE);*/
                    }

                    @Override
                    public void onError() {
                        /*progress.setVisibility(View.GONE);*/
                        Picasso.with(mContext).load(R.mipmap.ic_no_avarta).fit().centerCrop().into(img_avarta);
                    }
                });
            } else {
              /*  progress.setVisibility(View.GONE);*/
                Picasso.with(mContext).load(R.mipmap.ic_no_avarta).fit().centerCrop().into(img_avarta);
            }

            //== name
            txt_name.setText(object.name);


        }

        public void initEvent(final FollowModel object, final int position)
        {
            lay_unfollow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    lay_unfollow.setVisibility(View.INVISIBLE);
                    AddFollowAsyncTask addFollowAsyncTask = new AddFollowAsyncTask(mContext, new AddFollowRequest(userModel.id, userModel.ApiToken, object.id));
                    addFollowAsyncTask.execute();
                }
            });

            lay_profile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(mContext, FriendProfileActivity.class);
                    mContext.startActivity(i);
                }
            });
        }
    }

    public class AddFollowAsyncTask extends AsyncTask<AddFollowRequest, Void, AddFollowResponse> {

        AddFollowRequest addFollowRequest;
        Context ct;


        public AddFollowAsyncTask(Context ct, AddFollowRequest addFollowRequest) {
            this.ct = ct;
            this.addFollowRequest = addFollowRequest;
        }

        @Override
        protected AddFollowResponse doInBackground(AddFollowRequest... params) {
            try {
                Thread.sleep(0);
                UserController controller = new UserController();
                return controller.addFollow(addFollowRequest);
            } catch (Exception ex) {
                return null;
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(AddFollowResponse code) {
            try {

                if (code != null) {
                    if (code.code == 200)
                    {
                        // Toast.makeText(mContext, code.message,Toast.LENGTH_SHORT).show();
                    }
                }
            } catch (Exception e) {

            }

        }
    }

}
