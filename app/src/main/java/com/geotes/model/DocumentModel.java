package com.geotes.model;

import java.io.Serializable;

/**
 * Created by Hoang on 1/2/2018.
 */

public class DocumentModel implements Serializable {
    public int id;
    public String name;

    public DocumentModel(int id, String name) {
        this.id = id;
        this.name = name;
    }
}
