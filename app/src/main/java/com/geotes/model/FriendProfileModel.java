package com.geotes.model;

import java.io.Serializable;

/**
 * Created by Hoang on 1/2/2018.
 */

public class FriendProfileModel implements Serializable {
    public int id;
    public String name,phone_number,photo,address,email;
    public int is_like;

    public FriendProfileModel(int id, String name, String phone_number, String photo, String address, String email, int is_like) {
        this.id = id;
        this.name = name;
        this.phone_number = phone_number;
        this.photo = photo;
        this.address = address;
        this.email = email;
        this.is_like = is_like;
    }
}
