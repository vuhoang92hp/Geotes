package com.geotes.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Hoang on 1/2/2018.
 */

public class StatusDetailModel implements Serializable {
    public int id;
    public String content;
    public String photo;
    public String tag, created_at, updated_at;
    public int user_id,is_active,type_photo,number_like,number_comment,number_share;
    public ArrayList<CommentModel> comments;

    public StatusDetailModel(int id, String content, String photo, String tag, String created_at, String updated_at, int user_id, int is_active, int type_photo, int number_like, int number_comment, int number_share, ArrayList<CommentModel> comments) {
        this.id = id;
        this.content = content;
        this.photo = photo;
        this.tag = tag;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.user_id = user_id;
        this.is_active = is_active;
        this.type_photo = type_photo;
        this.number_like = number_like;
        this.number_comment = number_comment;
        this.number_share = number_share;
        this.comments = comments;
    }
}
