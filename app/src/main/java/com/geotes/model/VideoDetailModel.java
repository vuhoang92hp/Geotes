package com.geotes.model;

import java.io.Serializable;

/**
 * Created by Hoang on 1/2/2018.
 */

public class VideoDetailModel implements Serializable {
    public int id;
    public String title;
    public String photo;
    public String description, video_link;
    public int youtube_list_id;
    public String created_at, updated_at;

    public VideoDetailModel(int id, String title, String photo, String description, String video_link, int youtube_list_id, String created_at, String updated_at) {
        this.id = id;
        this.title = title;
        this.photo = photo;
        this.description = description;
        this.video_link = video_link;
        this.youtube_list_id = youtube_list_id;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }
}
