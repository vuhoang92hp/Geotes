package com.geotes.model;

import java.io.Serializable;

/**
 * Created by Hoang on 1/2/2018.
 */

public class VideoModel implements Serializable {
    public int id;
    public String title;
    public String photo;
    public String video_time, video_channel, video_link;
    public int youtube_guide_id;
    public String created_at, updated_at, youtube_guide_title;

    public VideoModel(int id, String title, String photo, String video_time, String video_channel, String video_link, int youtube_guide_id, String created_at, String updated_at, String youtube_guide_title) {
        this.id = id;
        this.title = title;
        this.photo = photo;
        this.video_time = video_time;
        this.video_channel = video_channel;
        this.video_link = video_link;
        this.youtube_guide_id = youtube_guide_id;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.youtube_guide_title = youtube_guide_title;
    }
}
