package com.geotes.model;

import java.util.ArrayList;

/**
 * Created by Admin on 9/15/2017.
 */

public class NewFeedModel {
    public int id;
    public String content;
    public String photo;
    public String tag;
    public String created_at;
    public String updated_at;
    public int user_id;
    public int is_active;
    public int type_photo;

    public String nameUser;
    public String photoUser;
    public int number_like;
    public int number_comment;
    public int number_share;
    public int is_liked;
    public ArrayList<CommentModel> comments;

    public NewFeedModel(int id, String content, String photo, String tag, String created_at, String updated_at, int user_id, int is_active, int type_photo, String nameUser, String photoUser, int number_like, int number_comment, int number_share, int is_liked, ArrayList<CommentModel> comments) {
        this.id = id;
        this.content = content;
        this.photo = photo;
        this.tag = tag;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.user_id = user_id;
        this.is_active = is_active;
        this.type_photo = type_photo;
        this.nameUser = nameUser;
        this.photoUser = photoUser;
        this.number_like = number_like;
        this.number_comment = number_comment;
        this.number_share = number_share;
        this.is_liked = is_liked;
        this.comments = comments;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getIs_active() {
        return is_active;
    }

    public void setIs_active(int is_active) {
        this.is_active = is_active;
    }

    public int getType_photo() {
        return type_photo;
    }

    public void setType_photo(int type_photo) {
        this.type_photo = type_photo;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public String getPhotoUser() {
        return photoUser;
    }

    public void setPhotoUser(String photoUser) {
        this.photoUser = photoUser;
    }

    public int getNumber_like() {
        return number_like;
    }

    public void setNumber_like(int number_like) {
        this.number_like = number_like;
    }

    public int getNumber_comment() {
        return number_comment;
    }

    public void setNumber_comment(int number_comment) {
        this.number_comment = number_comment;
    }

    public int getNumber_share() {
        return number_share;
    }

    public void setNumber_share(int number_share) {
        this.number_share = number_share;
    }

    public ArrayList<CommentModel> getComments() {
        return comments;
    }

    public void setComments(ArrayList<CommentModel> comments) {
        this.comments = comments;
    }

    public int getIs_liked() {
        return is_liked;
    }

    public void setIs_liked(int is_liked) {
        this.is_liked = is_liked;
    }
}
