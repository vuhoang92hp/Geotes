package com.geotes.model;

import java.io.Serializable;

/**
 * Created by Hoang on 1/2/2018.
 */

public class VersionCodeModel implements Serializable {
    public int id,version;
    public String created_at, updated_at;


    public VersionCodeModel(int id, int version, String created_at, String updated_at) {
        this.id = id;
        this.version = version;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }
}
