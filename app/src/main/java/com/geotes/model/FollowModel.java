package com.geotes.model;

import java.util.ArrayList;

/**
 * Created by Admin on 9/15/2017.
 */

public class FollowModel {
    public int id;
    public String name, photo;
    public int is_liked;

    public FollowModel(int id, String name, String photo, int is_liked) {
        this.id = id;
        this.name = name;
        this.photo = photo;
        this.is_liked = is_liked;
    }
}
