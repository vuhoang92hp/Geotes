package com.geotes.model;

import java.io.Serializable;

/**
 * Created by Hoang on 1/2/2018.
 */

public class CourseModel implements Serializable {
    public int id;
    public String title;
    public String price;
    public String content, address;
    public int is_valid;
    public String photo,created_at, updated_at;

    public CourseModel(int id, String title, String price, String content, String address, int is_valid, String photo, String created_at, String updated_at) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.content = content;
        this.address = address;
        this.is_valid = is_valid;
        this.photo = photo;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }
}
