package com.geotes.model;

/**
 * Created by Hoang on 1/11/2018.
 */

public class UserModel {
    public String email,facebook_id, token_firebase, photo, name, phone_number, address;
    public int is_active;
    public String updated_at, created_at;
    public int id;
    public String ApiToken;

    public UserModel(String email, String facebook_id, String token_firebase, String photo, String name, String phone_number, String address, int is_active, String updated_at, String created_at, int id, String apiToken) {
        this.email = email;
        this.facebook_id = facebook_id;
        this.token_firebase = token_firebase;
        this.photo = photo;
        this.name = name;
        this.phone_number = phone_number;
        this.address = address;
        this.is_active = is_active;
        this.updated_at = updated_at;
        this.created_at = created_at;
        this.id = id;
        ApiToken = apiToken;
    }
}
