package com.geotes.model;

import java.io.Serializable;

/**
 * Created by Hoang on 1/2/2018.
 */

public class PlaylistModel implements Serializable {
    public int id;
    public String photo;
    public int video_number;
    public String title, created_at, updated_at;

    public PlaylistModel(int id, String photo, int video_number, String title, String created_at, String updated_at) {
        this.id = id;
        this.photo = photo;
        this.video_number = video_number;
        this.title = title;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }
}
