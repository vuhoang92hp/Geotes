package com.geotes.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Hoang on 1/2/2018.
 */

public class CourseDetailModel implements Serializable {
    public int id;
    public String introduce,difference,benifit,address,phone_number;
    public int course_id;
    public String created_at, updated_at;
    public ArrayList<PhotoCourseModel> image;

    public CourseDetailModel(int id, String introduce, String difference, String benifit, String address, String phone_number, int course_id, String created_at, String updated_at, ArrayList<PhotoCourseModel> image) {
        this.id = id;
        this.introduce = introduce;
        this.difference = difference;
        this.benifit = benifit;
        this.address = address;
        this.phone_number = phone_number;
        this.course_id = course_id;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.image = image;
    }
}
