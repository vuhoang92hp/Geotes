package com.geotes.model;

/**
 * Created by Admin on 9/15/2017.
 */

public class CommentModel {
    public int id;
    public int new_feed_id;
    public int user_id;
    public String content;
    public String created_at;
    public String updated_at;
    public String user_name;
    public String user_photo;
    public String comment_time;

    public CommentModel(int id, int new_feed_id, int user_id, String content, String created_at, String updated_at, String user_name, String user_photo, String comment_time) {
        this.id = id;
        this.new_feed_id = new_feed_id;
        this.user_id = user_id;
        this.content = content;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.user_name = user_name;
        this.user_photo = user_photo;
        this.comment_time = comment_time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNew_feed_id() {
        return new_feed_id;
    }

    public void setNew_feed_id(int new_feed_id) {
        this.new_feed_id = new_feed_id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_photo() {
        return user_photo;
    }

    public void setUser_photo(String user_photo) {
        this.user_photo = user_photo;
    }

    public String getComment_time() {
        return comment_time;
    }

    public void setComment_time(String comment_time) {
        this.comment_time = comment_time;
    }
}
