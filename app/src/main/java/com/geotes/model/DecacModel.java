package com.geotes.model;

import java.io.Serializable;

/**
 * Created by Hoang on 1/2/2018.
 */

public class DecacModel implements Serializable {
    public double x;
    public double y;

    public DecacModel(double x, double y) {
        this.x = x;
        this.y = y;
    }
}
