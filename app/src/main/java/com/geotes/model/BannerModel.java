package com.geotes.model;

import java.io.Serializable;

/**
 * Created by Hoang on 1/2/2018.
 */

public class BannerModel implements Serializable {
    public int id;
    public String photo;
    public int course_detail_id;
    public String created_at, updated_at;

    public BannerModel(int id, String photo, int course_detail_id, String created_at, String updated_at) {
        this.id = id;
        this.photo = photo;
        this.course_detail_id = course_detail_id;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }
}
