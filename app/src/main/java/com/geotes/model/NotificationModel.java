package com.geotes.model;

import java.io.Serializable;

/**
 * Created by Hoang on 1/2/2018.
 */

public class NotificationModel implements Serializable {
    public int id;
    public String content;
    public int user_create, new_feed_id, user_created_id;
    public String user_created_photo;
    public int is_read;
    public String user_name, user_photo;
    public int  user_id;
    public int time_ago;
    public String created_at;

    public NotificationModel(int id, String content, int user_create, int new_feed_id, int user_created_id, String user_created_photo, int is_read, String user_name, String user_photo, int user_id, int time_ago, String created_at) {
        this.id = id;
        this.content = content;
        this.user_create = user_create;
        this.new_feed_id = new_feed_id;
        this.user_created_id = user_created_id;
        this.user_created_photo = user_created_photo;
        this.is_read = is_read;
        this.user_name = user_name;
        this.user_photo = user_photo;
        this.user_id = user_id;
        this.time_ago = time_ago;
        this.created_at = created_at;
    }
}
