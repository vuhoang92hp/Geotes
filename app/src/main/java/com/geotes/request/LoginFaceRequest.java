package com.geotes.request;

import java.io.Serializable;

public class LoginFaceRequest extends BaseRequest implements Serializable {
    public String facebook_id;
    public String name;
    public String email;
    public String photo;
    public String token_firebase;
    public String phone_number;


    public LoginFaceRequest(String facebook_id, String name, String email, String photo, String token_firebase, String phone_number) {
        this.facebook_id = facebook_id;
        this.name = name;
        this.email = email;
        this.photo = photo;
        this.token_firebase = token_firebase;
        this.phone_number = phone_number;
    }
}
