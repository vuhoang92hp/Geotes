package com.geotes.request;

/**
 * Created by Admin on 9/8/2017.
 */

public class AddCommentRequest extends BaseRequest{
    public int UserID,new_feed_id;
    public String content;

    public AddCommentRequest(int userID, int new_feed_id, String content) {
        UserID = userID;
        this.new_feed_id = new_feed_id;
        this.content = content;
    }
}
