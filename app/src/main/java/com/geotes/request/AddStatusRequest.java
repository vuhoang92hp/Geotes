package com.geotes.request;

/**
 * Created by Admin on 9/8/2017.
 */

public class AddStatusRequest extends BaseRequest{
    public int UserID;
    public String ApiToken,content,photo;


    public AddStatusRequest(int userID, String apiToken, String content, String photo) {
        UserID = userID;
        ApiToken = apiToken;
        this.content = content;
        this.photo = photo;
    }
}
