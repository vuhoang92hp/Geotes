package com.geotes.request;

/**
 * Created by Admin on 8/18/2017.
 */

public class UpdateProfileRequest extends BaseRequest {
    public int UserID;
   public String name, photo, email,address;

    public UpdateProfileRequest(int userID, String name, String photo, String email, String address) {
        UserID = userID;
        this.name = name;
        this.photo = photo;
        this.email = email;
        this.address = address;
    }
}
