package com.geotes.request;

/**
 * Created by Admin on 9/8/2017.
 */

public class DeletePostRequest extends BaseRequest{
    public int UserID;
    public String ApiToken;
    public int new_feed_id;

    public DeletePostRequest(int userID, String apiToken, int new_feed_id) {
        UserID = userID;
        ApiToken = apiToken;
        this.new_feed_id = new_feed_id;
    }
}
