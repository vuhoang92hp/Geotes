package com.geotes.request;

/**
 * Created by Admin on 9/8/2017.
 */

public class AddFollowRequest extends BaseRequest{
    public int UserID;
    public String ApiToken;
    public int user_favorite_id;

    public AddFollowRequest(int userID, String apiToken, int user_favorite_id) {
        UserID = userID;
        ApiToken = apiToken;
        this.user_favorite_id = user_favorite_id;
    }
}
