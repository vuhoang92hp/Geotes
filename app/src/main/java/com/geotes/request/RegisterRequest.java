package com.geotes.request;

/**
 * Created by Admin on 9/14/2017.
 */

public class RegisterRequest extends BaseRequest {
    public String phone_number;
    public String name;
    public String token_firebase;
    public String password;

    public RegisterRequest(String phone_number, String name, String token_firebase, String password) {
        this.phone_number = phone_number;
        this.name = name;
        this.token_firebase = token_firebase;
        this.password = password;
    }
}
