package com.geotes.request;

/**
 * Created by Admin on 9/8/2017.
 */

public class CourseRegisterRequest extends BaseRequest{

    public String name,phone_number,job,note;
    public int course_id;

    public CourseRegisterRequest(String name, String phone_number, String job, String note, int course_id) {
        this.name = name;
        this.phone_number = phone_number;
        this.job = job;
        this.note = note;
        this.course_id = course_id;
    }
}
