package com.geotes.request;


import com.geotes.api.APICommon;

/**
 * Created by Hoang on 3/16/2017.
 */

public class BaseRequest {
    public String app_id;
    public int device_type;
    public String sign;

    public BaseRequest(String path_url) {
       // app_id = APICommon.getAppId();
        device_type = APICommon.getDeviceType();
        //sign = APICommon.getSign(APICommon.getApiKey(), path_url);
    }

    public BaseRequest() {

    }
}
