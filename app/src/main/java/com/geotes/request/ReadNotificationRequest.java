package com.geotes.request;

/**
 * Created by Admin on 9/8/2017.
 */

public class ReadNotificationRequest extends BaseRequest{
    public int UserID;
    public String ApiToken;
    public int notification_id;

    public ReadNotificationRequest(int userID, String apiToken, int notification_id) {
        UserID = userID;
        ApiToken = apiToken;
        this.notification_id = notification_id;
    }
}
