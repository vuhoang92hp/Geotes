package com.geotes.request;

/**
 * Created by Admin on 8/18/2017.
 */

public class LoginRequest extends BaseRequest {
   public String phone_number;
    public String password;
    public String token_firebase;

    public LoginRequest(String phone_number, String password, String token_firebase) {
        this.phone_number = phone_number;
        this.password = password;
        this.token_firebase = token_firebase;
    }
}
