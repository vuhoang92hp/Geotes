package com.geotes.request;

/**
 * Created by Admin on 9/14/2017.
 */

public class ActiveAccountRequest extends BaseRequest {
    public String phone_number;
    public String password,token_firebase;


    public ActiveAccountRequest(String phone_number, String password, String token_firebase) {
        this.phone_number = phone_number;
        this.password = password;
        this.token_firebase = token_firebase;
    }
}
