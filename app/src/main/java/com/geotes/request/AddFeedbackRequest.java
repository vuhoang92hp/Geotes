package com.geotes.request;

/**
 * Created by Admin on 9/8/2017.
 */

public class AddFeedbackRequest extends BaseRequest{

    public String email,content;

    public AddFeedbackRequest(String email, String content) {
        this.email = email;
        this.content = content;
    }
}
